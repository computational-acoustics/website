---
title: "Intro to FEniCS - Part 1"
date: 2021-11-13T13:52:51Z
draft: false
featured_image: "https://fenicsproject.org/pub/graphics/fenics_logo.png"
description: "Setting up a FEniCS Environment"
categories: ["Software"]
tags: ["Introductory", "Software", "Tutorial", "FEniCS"]
---

In the previous episodes we used Elmer to develop a few different acoustics models. We were able to verify the accuracy of Elmer with few benchmark problems. Additionally, we studied the effect of mesh order and size on the accuracy. Finally, we started probing vibro-acoustic problems. We only scratched the surface of Elmer capabilities in this field. Still, it is worth to take a step back and reconsider these problems under a different light. The [`FEniCS`](https://fenicsproject.org/) project is perfect for this. We will be introducing `FEniCS` in this episode.

<!--more-->

# Why `FEniCS`?

## Elmer Pros and Cons

Elmer is a very good FEM package with highly sophisticated acoustics solvers. It also allows to build complex multi-physics simulations through weak and strong coupling. However, it has a few downsides. 

If we need to write a custom solver for a PDE we will need to make use of Fortran. If coding with Fortran is your cup of tea then this is quite painless. Elmer is also a very good FEM toolkit and it provides its own compiler wrapper. Compilation of custom solvers as well as custom functions is then very straightforward. 

Maybe a custom solver is not what we need most of the time, but we might need custom functions. For example, we might want to have a non uniform flux boundary condition on a certain boundary for the Helmholtz equation. These more "advanced" features are often supported within Elmer through the use of `lua` and `MATC` scripting. This can come at a cost of reduced computational efficiency, especially with `MATC`. 

Finally, we might want to _embed_ our FEM solver step in something bigger. This could be an optimization problem, for example. Often we do not use Fortran to implement this bigger problem. To code the entire stack we would then need some significant _glue code_ development (unless some useful tool already exists, such as [pyelmer](https://pypi.org/project/pyelmer/)).

So, to sum up, Elmer:

* Provides highly sophisticated solvers;
* Supports multi-physics.

But:

* Custom solvers have to be implemented in Fortran;
* Scripting might reduce computationally efficiency;
* Building a highly sophisticated study around Elmer might be cumbersome.

## `FEniCS` Pros and Cons

> `FEniCS` is a popular open-source (LGPLv3) computing platform for solving partial differential equations (PDEs). `FEniCS` enables users to quickly translate scientific models into efficient finite element code. With the high-level Python and C++ interfaces to `FEniCS`, it is easy to get started, but `FEniCS` offers also powerful capabilities for more experienced programmers. `FEniCS` runs on a multitude of platforms ranging from laptops to high-performance clusters.

The excerpt above is from the [`FEniCS` home page](https://fenicsproject.org/). It sums up all the pros of `FEniCS`. First of all, we can code `FEniCS` solvers with `python`.  Through a wrapper, with can also use [julia as well](https://github.com/SciML/FEniCS.jl). These are two high level languages particularly well suited for scientific computing. Both come with a huge repository of packages. Hence, we can use them to build any sort of extended application around our FEM solving node.

`FEniCS` allows the programmer to define the problem by providing the _weak form_ of the PDE. In the [FEM in a Nutshell]({{< ref "/posts/3-fem-in-a-nutshell" >}}) episode we seen how this is the very first step behind any FEM scheme. `FEniCS` will take care of the problem after that, based on how we configure it. With respect Elmer, which normally we use "as is", `FEniCS` can support any PDE. We can code any complex boundary conditions, such as Perfectly Matched Layers (PML). These, for example, are not available in Elmer acoustics solvers.

The emphasis here is on _coding_: when using `FEniCS` we will write the code to solve the problem. However, `FEniCS` handles the most complex steps quite for us. This is both a pro and a con. As a pro, this will lead us to a more intimate understanding of how to turn a PDE into a FEM numerical model. As a con, the learning curve is quite steep. This is especially true when it comes to acoustics, being the governing PDEs quite complex.

As an additional con, `FEniCS` is in the process of becoming `FEniCS`. Hence, currently there are few different ways to deal with problems depending on what we use (`FEniCS` or the development version of `FEniCS`). This is particularly relevant for acoustics, as we often make use of complex fields. Complex fields are dealt with in different ways between `FEniCS` and `FEniCS`. At the time of writing, navigating the existing documentation is a bit hard. This is also why this series exists.

## `FEniCS` is not Better/Worse than Elmer

`FEniCS` and Elmer are very good tools. Elmer is very efficient and highly accurate, but harder to customise and fit into a bigger project. Elmer provides many advanced solvers for acoustics.

`FEniCS` is more of an SDK for FEM. It allows to code a huge array of models. But needs a deeper understanding of PDEs and FEM to do so with effectiveness.

Learning both is definitely beneficial, as we will be able to apply to every problem the best tool.

In this introductory series about `FEniCS` we will first see how to install it. Then, we will go through the steps of replicating the Elmer Helmholtz model with `FEniCS`. From there, we will extend the `FEniCS` model beyond what Elmer can currently handle.

# `FEniCS` with `Python`?

In this `FEniCS` based series we will use `python` as the programming language of choice. This is at odds with the language used throughout the series, which is `julia`. We can use `FEniCS` with `julia`, either through [PyCall.jl](https://github.com/JuliaPy/PyCall.jl) or [`FEniCS`.jl](https://github.com/SciML/FEniCS.jl). This tends to come with some rougher edges, so for this introductory series we will keep it to `python`. `python` is a very good language to have a grasp of, and it is not too different from `julia`. Having an understanding of both is beneficial. In fact, we already integrated `python` packages in these tutorials. For example, see the [Intro to meshio]({{< ref "/posts/20-intro-to-meshio" >}}) episode.

# Getting Started with `FEniCS`

One good starting point is the book [Solving PDEs in Python](https://link.springer.com/content/pdf/10.1007%2F978-3-319-52462-7.pdf). The first two chapters are easy to read. It is good to read them and try the code before we jump into acoustics.

The installation instructions presented in the book are very similar to those presented [in the documentation](https://fenics.readthedocs.io/en/latest/installation.html). First, we should make sure we have `Python` and few other useful packages:

```bash
sudo apt update
sudo apt install -y python3 python3-pip build-essential libssl-dev libffi-dev python-dev python3-venv
```

On my Ubuntu 18.04 machine installing `FEniCS` was as simple as installing from the PPA:

```bash
sudo apt-get install --no-install-recommends software-properties-common
sudo add-apt-repository ppa:fenics-packages/fenics
sudo apt-get update
sudo apt-get install --no-install-recommends fenics
```

In addition to `FEniCS` we can install a `python` IDE to make coding and debugging easier. [`PyCharm`](https://www.jetbrains.com/pycharm/) is a good one, and we will be using that in this series (Community Edition). In Ubuntu, `PyCharm` is also available as a snap. You can install it from the `Software` application, or simply issue the following command:

```bash
snap install pycharm-community
```

This should be enough to have our `FEniCS` modelling environment ready.

# Conclusion

In this series we will start discovering `FEniCS` and we will apply it to acoustics problems. We will start off the Helmholtz equation, and replicate the Elmer solver. Then, we will extend the solver and make it more advanced. In this episode we covered the pros and cons of both Elmer and `FEniCS`. We developed a feeling of what they are best suited for. Additionally, we covered the necessary steps to setup a `FEniCS` modelling environment on Ubuntu.

In the next episode we will study the Helmholtz PDE in high details, and derive its weak form.


{{< featuredpic source="FEniCS" url="https://fenicsproject.org/" topbr="True" >}}

{{< cc >}}

