---
title: "Project Refactor"
date: 2021-01-30T15:03:30Z
draft: false
featured_image: "https://julialang.github.io/Pkg.jl/v1/assets/logo.png"
description: "Refactoring the Site for Easier Maintenance and Readability"
categories: ["Maintenance"]
tags: ["Maintenance", "Updates"]
---

This project has now quite a few articles and simulations going. Unfortunately, the way the Julia code repositories were set up is not very functional and is not scaling well to many repositories. For this reason, this site will undergo a period of refactoring.

<!--more-->

# The Status Quo

At the moment there are few additional Julia code repositories that contain helper code, mainly for postprocessing and to create the plots in this website. Examples are:

* [Acoustic Models](https://gitlab.com/computational-acoustics/acoustic-models).
* [FEA Tools](https://gitlab.com/computational-acoustics/fea-tools).
* [Web Plots](https://gitlab.com/computational-acoustics/web-plots).

These are typically included as submodules in the various [simulation repositories](https://gitlab.com/computational-acoustics). However, submodules need to be updated manually and the usage of this code is also a bit impervious for other users. A better idea is then to setup each of those projects as a Julia Package.

# Julia Packages

The [Web Plots](https://gitlab.com/computational-acoustics/web-plots) repository has been already converted to a new Julia package [WebPlots.jl](https://gitlab.com/computational-acoustics/webplots.jl). The old [Web Plots](https://gitlab.com/computational-acoustics/web-plots) repository will be perhaps soon deleted (as all other old repositories above, so expect the links to die).

However, the code in all repositories that uses the old submodules will need to be adapted to make use of the new incoming packages. This activity will most likely require most of the time in the next few weeks.

At the end of this process new simulations will be easier to setup and maintain.

# Other Activities

The site has also many other minor issues that need addressing, such as:

- [X] Change _ElmerFEM_ to _Elmer_ in all articles.
- [X] Use LaTeX for all numbers and units in all articles.
- [X] Uniform mathematical notation on all articles.
- [X] Uniform terms like "open-source" instead of mixed use of "opensource", "open source", "open-source"...
- [X] Refactor tags by series.
- [X] Clarify the source of each featured picture when not produced by this project authors (maybe with a shortcode).

# Conclusion

These activities will happen in the next few weeks. After that ordinary simulation activity will be resumed.

{{< featuredpic source="the Julia Pkg Documentation" url="https://julialang.github.io/Pkg.jl/v1/" topbr="True" >}}

{{< cc >}}

