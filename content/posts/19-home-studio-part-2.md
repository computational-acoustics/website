---
title: "Home Studio - Part 2"
date: 2020-10-31T10:21:45Z
draft: false
featured_image: "/posts_res/19-home-studio-part-2/featured.png"
description: "Eigenproblem for the Realistically Shaped Room"
categories: ["Modelling"]
tags: ["Physics", "Elmer", "FEM", "Linear System", "Single Physics", "Steady State", "Eigensystem", "Series: Home Studio"]
---

In the [Home Studio - Part 1]({{< ref "/posts/12-home-studio-part-1.md" >}}) episode we computed the steady state field of a realistic, but still rigid walled, room. We put a source in the room, modelled as a flux boundary condition, and run the study at few different frequencies. The solutions that we found were reminiscent of modal patterns, which is expected as the low frequency response of a room is dominated by its resonances. However, that kind of study does not inform us on the actual resonance frequencies of the room, which are properties of great interest, as well as how the associated modal shapes (eigenmodes, or eigenfunctions) look like.

Recently we treated the acoustic eigenproblem of the rigid walled rectangular room. In the [Rigid Walled Room Revisited - Part 1]({{< ref "/posts/15-rigid-walled-room-revisited-part-1.md" >}}), [2]({{< ref "/posts/16-rigid-walled-room-revisited-part-2.md" >}}), [3]({{< ref "/posts/17-rigid-walled-room-revisited-part-3.md" >}}) episodes we studied how to compute highly accurate solutions, with resonance frequency accuracy within $0.003$ $\text{cent}$ and eigenmode accuracy within $0.001$ $\text{dB}$. In the context of a realistically shaped room, the rectangular room acts as a benchmark system: we can carry what we learned in terms of effective solver parameters for the rectangular room over to the realistic room, knowing that those parameters provide good accuracy, much like we discussed in the [The FEM Pipeline]({{< ref "/posts/4-the-fem-pipeline.md" >}}) episode.

In this episode we will do just that: we will solve the eigenproblem for the realistically shaped room by using what we learned from the rectangular room eigenproblem.

# Project Files

All the files used for this project are available at the repositories below:

* [Home Studio 4](https://gitlab.com/computational-acoustics/home-studio-4).

# The Meshing

As we discussed in many other episodes, typically we choose our mesh size $s$ according to this rule of thumb:

$$ s < \frac{\lambda}{10} = \frac{c}{10f}$$

Where $c$ is the phase speed of sound in the medium and $f$ is the frequency of the simulation. Let's say that we want to search for the $10$ lowest eigenfrequencies and eigenfunctions. Then we need a mesh size $s$ that is appropriate for the highest eigenfrequency. But wait, we are trying to solve for the eigenfrequencies! So we do not know them yet. How can we set an appropriate mesh size to solve for eigenfrequencies we do not know?

This is where the rectangular room comes into rescue again. Our realistic room is very close to a rectangular room in shape, so we should expect it to behave somewhat similar. The rough dimensions of our room are roughly $4.36$ $\text{m}$ by $2.80$ $\text{m}$ by $2.25$ $\text{m}$. We can use these sizes, and assume a speed of sound of $343$ $\frac{\text{m}}{\text{s}}$ (appropriate for air at $20$ $\text{°C}$) to compute the resonance frequencies just as in the [Acoustic Modes of a Rectangular Room]({{< ref "/posts/5-acoustic-modes-of-a-rectangular-room.md" >}}) episode. The results are as follows:

{{< load-table-dfl-style >}}
| Mode Number | Mode Frequency $\left\[\text{Hz}\right\]$ |
|-------------|-------------------------------------------|
| $1$         | $0$                                       |
| $2$         | $39.33$                                   |
| $3$         | $61.25$                                   |
| $4$         | $72.79$                                   |
| $5$         | $76.22$                                   |
| $6$         | $78.67$                                   |
| $7$         | $85.77$                                   |
| $8$         | $97.78$                                   |
| $9$         | $99.70$                                   |
| $10$        | $105.40$                                  |

Based on the results above, we expect a maximum frequency around $105$ $\text{Hz}$. We can calibrate our mesh size $s$ around this value.

But before going ahead and do just that, what we would do if our system was nothing like a rectangular room?

In that case, we could proceed as follows. Put a bounding box on our system and calculate the eigenfrequencies of the bounding box you are interested in searching for (in this case, the first $10$). Then, apply a good safety margin by calibrating $s$ for a higher frequency with respect the highest eigenfrequency you found. It is hard to say what a good margin is, but the mesh size should not be higher than that associated with the highest bounding box eigenfrequency. If you are not sure your mesh size is appropriate, run the study with few sizes. If, from going to a smaller size, the results are essentially the same as those with a bigger size, than you are good. If the result changes, keep on testing smaller sizes until the results settle.

For this problem, being the room pretty much rectangular, I chosen a small safety margin. I first calculated the size associated to $110$ $\text{Hz}$, which is $312$ $\text{mm}$, and then I rounded it down to $300$ $\text{mm}$. This is equivalent to choosing a target frequency of $\sim114$ $\text{Hz}$, or a safety margin of $9$ $\text{Hz}$. As a meshing algorithm I chosen our trustworthy _NETGEN 1D-2D-3D_. The mesh parameters used in Salome are shown below (for more guidance on how to mesh your geometry refer to the previous episodes). Note that the mesh is first order as, just like in the [Rigid Walled Room Revisited - Part 3]({{< ref "/posts/17-rigid-walled-room-revisited-part-3.md" >}}) episode, we will use second order p-elements within Elmer.

{{< figure src="/website/posts_res/19-home-studio-part-2/mesh-settings-1.png" title="Figure 1" caption="Mesh Settings" class="mw6" >}}

When I first computed the mesh with the parameters above I wasn't fully satisfied with the amount of elements on the little space around the door that this provided, as well as the number of elements on both doors. The picture below should clarify the areas I am mentioning.

{{< figure src="/website/posts_res/19-home-studio-part-2/local-refinement-1.png" title="Figure 2" caption="Areas of Special Interest" class="mw10" >}}

The figure above, taken from Salome, shows the areas of special interest, evidenced by a light colour outline. In the [Rigid Walled Room Revisited - Part 3]({{< ref "/posts/17-rigid-walled-room-revisited-part-3.md" >}}) episode we noticed how the accuracy is lower where there are fewer elements. In this system we essentially have a small rectangular appendix attached by a bigger rectangular room. The small space is evident in the left of the room in the picture above. Covering the smaller subsystem with few elements might produce errors that can propagate to the bigger subsystem as well, thus lowering to global accuracy.

The doors themselves are less of a concern for our problem, as we will not treat them differently and still assign a rigid boundary condition to them. However, if we had to treat them differently, such as model them as a source or outlet of sound, or just as patches with different acoustic properties, we will need to ensure that they are meshed with sufficient density to properly model the local wave phenomena taking place in their proximity.

Whenever there are areas of special interest like the ones above it is best to mesh them with higher density. To do so we can use the _Local sizes_ tab in the _Hypotheses Construction_ window, as shown below.

{{< figure src="/website/posts_res/19-home-studio-part-2/local-refinement-2.png" title="Figure 3" caption="Areas of Special Interest" class="mw6" >}}

To add a local size, simply select the geometric entity you want to refine under _Geometry_ in the _Object Browser_ and then press the most appropriate button in the _Local sizes_ tab. The picture below shows how to add _Face 4_ to the local sizes:

{{< figure src="/website/posts_res/19-home-studio-part-2/local-refinement-3.png" title="Figure 4" caption="Adding an Entity to the Local Sizes" class="mw8" >}}

You will be able to edit the value field directly. For this problem, I chosen a fairly low value of $50$ $\text{mm}$. The final resulting mesh is shown below.

{{< figure src="/website/posts_res/19-home-studio-part-2/mesh.png" title="Figure 5" caption="Mesh" class="mw10" >}}

It is possible to see that the local sizes are indeed much higher in the areas of interest. The mesh was exported with `salomeToElmer` as in the [Rigid Walled Room Revisited - Part 1]({{< ref "/posts/15-rigid-walled-room-revisited-part-1.md" >}}) episode.

**_A Note for Salome 9.5.0 users:_** _It seems like Salome 9.5.0 will already create the required face groups if a volume entity is selected, so you might just need to select the volume to have all your groups setup. Ensure the face groups are not getting duplicated._

# Elmer Study Setup

The study setup is exactly the same as that of the [Rigid Walled Room Revisited - Part 3]({{< ref "/posts/17-rigid-walled-room-revisited-part-3.md" >}}) episode, so there is no need to go into detail. In essence, we are dealing with the same study, but on a different mesh.

# The Solution

The eigenfrequencies of the room are reported below:

| Mode Number | Mode Frequency $\left\[\text{Hz}\right\]$ |
|-------------|-------------------------------------------|
| $1$         | $0$                                       |
| $2$         | $38.87$                                   |
| $3$         | $59.94$                                   |
| $4$         | $71.12$                                   |
| $5$         | $76.22$                                   |
| $6$         | $78.75$                                   |
| $7$         | $85.56$                                   |
| $8$         | $96.97$                                   |
| $9$         | $99.27$                                   |
| $10$        | $104.25$                                  |

These frequencies are pretty similar to that for the rectangular room we used to calibrate the mesh size. Most frequencies have a slightly lower value. This is expected as the realistic room has slightly bigger volume. However mode $5$ has pretty much the same value, while mode $6$ is instead slightly higher.

The associated eigenfunctions are shown in the video below:

{{< youtube 2yEGKl96lbs >}}

From the inspection of the video we can see why mode $5$, at $76.22$ $\text{Hz}$, has frequency very similar to that of the same mode for a rectangular room. The mode develops only along the vertical axis. Perhaps we did not mention this yet, but an eigenfunction can be though of as the superposition of two waves travelling in opposite directions. In this case, vertically travelling waves reflecting at the ceiling and the floor. Given that the ceiling and floor are horizontal, and that the distance between them is the same for both the rectangular room and the realistic room, the frequency also happen to be the same.

Figuring out why mode $6$ has slightly higher frequency is instead less straightforward. We can see that the mode develops mainly along the $x$ axis. Along this axis the system appears as the chaining of two rooms of slightly different cross section. Where the cross section change partial reflection of waves happen, and in fact we can see distortion of the isosurfaces that are not vertical planes (as they would be for a rectangular room). This combination of shorter, weaker, reflection patterns with the longer patterns involving the whole room length might be the reason for the raised resonance frequency.

All the other modes appear similar to that of the rectangular room, but with noticeable distortion and asymmetry of the isosurfaces brought about by the little additional volume around the door.

Clearly, being this room not rectangular, the differences with the rectangular room are expected. However, it is good to critically compare the results to check that the differences make physical sense. In this case we can explain easily while most frequencies are lower, while we can clearly see how we should expect mode $5$ to have a very similar frequency to that of a rectangular room of same height. This equivalency in particular suggests us that the results are physically correct, even though we are not fully sure yet why mode $6$ ended up with a higher resonance frequency.

# Conclusion

In this episode we applied what we learned from the rectangular room eigenproblem to the eigenproblem for a more realistic room.

We learned how to apply local mesh refinement and when it makes sense to do so.

We inspected the solution and observed that physically it makes sense. This, united with the high accuracy assessed for the rectangular room system, which acts as a benchmark, is enough to conclude that we can trust this solution.

We also observed that a small variation in the shape of a room, like an added small volume, produce appreciable deviation in the resonance frequencies and resonance patterns with respect a similar rectangular room, although results are overall very similar, at least for the low frequencies we studied.

{{< cc >}}

