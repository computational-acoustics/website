---
title: "Tuning Fork - Part 2"
date: 2021-05-15T14:48:25+01:00
draft: false
featured_image: "/posts_res/26-tuning-fork-part-2/featured.png"
description: "Eigenmodes of a Tuning Fork - Results"
categories: ["Modelling"]
tags: ["Physics", "Elmer", "FEM", "Linear System", "Single Physics", "Eigensystem", "Series: Tuning Fork"]
---

In the [Tuning Fork - Part 1]({{< ref "/posts/25-tuning-fork-part-1" >}}) episode we setup a new study for the determination of the elastic modes of vibration of a tuning fork. Our study made use a few meshing and solver setting ideas we learned along the way, including the use of $p$-elements. In this episode we will examine the results from the Elmer simulation.

<!--more--> 

# Project Files

All the files used for this project are available at the repositories below:

* [Tuning Fork 1](https://gitlab.com/computational-acoustics/tuning-fork-1).

# A Note on Terminology

In this episode, quite inappropriately, the handle of the tuning fork will be referred as _prong_ while its prongs will be referred as _tines_. This due to the study having been setup with this terminology originally. Sorry for the confusion...

# Results

As you remember from the [Tuning Fork - Part 1]({{< ref "/posts/25-tuning-fork-part-1" >}}) episode we setup three different studies with different boundary conditions: free, prong bottom simply supported and prong simply supported. The results of all the studies are discussed in the sections below.

## Free Boundary Condition

In this case the tuning fork is completely free. The eigenvalues are saved by Elmer in the file `eigenvalues_free.dat`. As a reminder, eigenvalues have the following relationship with resonance frequencies:

$$\lambda\_{n} = \omega\_{n}^{2} = \left(2 \pi f\_{n} \right)^{2}$$

Where $\lambda\_{n}$, $\omega\_{n}$ and $f\_{n}$ are the $n$-th eigenvalue, resonance angular frequency and resonance frequency respectively. Hence:

$$\Rightarrow f\_{n} = \frac{\sqrt[+]{\lambda\_{n}}}{2 \pi}$$

You can use the [FEATools](https://gitlab.com/computational-acoustics/featools.jl) package to read the `dat` files and convert them in frequency automatically:

```julia
using FEATools
f = read_fem_eigenfrequencies("elmerfem/eigenvalues_free.dat")
```

In the example above the Julia session was started from the root of the [Tuning Fork 1](https://gitlab.com/computational-acoustics/tuning-fork-1) project.

For a free system we expect the first $6$ resonance frequencies to be $0$ $\text{Hz}$, associated to bulk motion of the whole system along its $6$ rigid body degrees of freedom ($3$ translational and $3$ rotational). This is indeed the case, however Elmer will output small (eventually negative) eigenvalues for these degrees of freedom. This is due to numerical accuracy. We can discard these values and start counting the mode number from the first nonzero eigenmode. The table below reports the first $10$ nonzero resonance frequencies:

{{< load-table-dfl-style >}}

| Mode Number | Resonance Frequency $\left\[ \text{Hz} \right\]$ |
|-------------|--------------------------------------------------|
| $1$         | $528.91$                                         |
| $2$         | $898.88$                                         |
| $3$         | $1547.97$                                        |
| $4$         | $1984.45$                                        |
| $5$         | $3232.84$                                        |
| $6$         | $3616.79$                                        |
| $7$         | $4134.71$                                        |
| $8$         | $4521.75$                                        |
| $9$         | $5369.73$                                        |
| $10$        | $7616.73$                                        |

It is evident that the fundamental of the tuning fork as computed by Elmer, $528.91$ $\text{Hz}$, is different from $512$ $\text{Hz}$, the nominal frequency of the tuning fork we are modelling. The error in [cent](https://en.wikipedia.org/wiki/Cent_(music)) amounts to $56.25$ $\text{cent}$, around a quarter step in musical language. A difference that will be definitely perceivable to humans. The results are overall close to those provided by [Justin Black](https://justinablack.com/tuning_fork_freqs/), from which we took the fork geometry. Since we use second order $p$-element (Justin's analysis makes use of first order elments) we can expect our results to be more accurate.

Another evident property of these frequencies is that the higher tones are not harmonically related to the fundamental: they are _inharmonic overtones_. Tuning forks are especially designed to be like that, so that activation of the overtones results to be much lower than that of the fundamental when the system is excited, a property which has clear advantages when attempting to use the system as a pitch reference.

The videos below show a highly exaggerated animation of the tuning fork modes. The first video shows $10$ periods of the first $2$ modes, while the second video shows the remaining $8$ modes. To be noted that the vibration happens at the frequency of the eigenmode. In the animations below, to ease visualisation, each of the eigenmodes vibration as been slowed down and synchronised to that of the fundamental.


### First Two Modes
{{< youtube id="fP7CYQZKQUA" title="First Two Modes" >}}

### Modes $3$ to $10$
{{< youtube id="A0ljIN1ogFk" title="Modes 3 to 10" >}}

It is possible to see that the first mode, associated to the $528.91$ $\text{Hz}$ resonance frequency, involves lateral vibration of the tines. Modes become progressively more complex at higher frequencies, producing multiple displacement peaks and twisting. Overall, the displacement fields appear smooth up to order $10$. This suggests no obvious mistakes have been done with the meshing.

## Prong Bottom Simply Supported Boundary Condition

We can report the results similarly to how we reported them with the free boundary condition. This time the eigenvalues are in the `eigenvalues_bottom_prong.dat` file and the associated modal frequencies are reported below:

| Mode Number | Resonance Frequency $\left\[ \text{Hz} \right\]$ |
|-------------|--------------------------------------------------|
| $1$         | $131.93$                                         |
| $2$         | $133.46$                                         |
| $3$         | $528.90$                                         |
| $4$         | $588.82$                                         |
| $5$         | $1159.16$                                        |
| $6$         | $1346.35$                                        |
| $7$         | $1650.39$                                        |
| $8$         | $3115.81$                                        |
| $9$         | $3231.20$                                        |
| $10$        | $3897.50$                                        |

We can see that now two low frequency modes have appeared. These modes are associated with the fork oscillating as a whole around its blocked prong bottom, as shown in the animations below. All the frequencies of the other modes are affected by the new boundary condition aside for the first mode involving vibration of the tines (mode $3$), which has essentially the same frequency of the first mode of the free fork and essentially the same vibration pattern. In other words, the first mode involving the tines has remained unaltered by this boundary condition applied to the prong bottom.

### First Two Modes
{{< youtube id="ag4Wt6hWSW0" title="First Two Modes" >}}

### Modes $3$ to $10$
{{< youtube id="S1K0-bI8l5g" title="Modes 3 to 10" >}}

## Prong Simply Supported Boundary Condition

We can report the results similarly to how we reported them with the free boundary condition. This time the eigenvalues are in the `eigenvalues_whole_prong.dat` file and the associated modal frequencies are reported below:

| Mode Number | Resonance Frequency $\left\[ \text{Hz} \right\]$ |
|-------------|--------------------------------------------------|
| $1$         | $444.04$                                         |
| $2$         | $473.42$                                         |
| $3$         | $528.93$                                         |
| $4$         | $640.27$                                         |
| $5$         | $2408.22$                                        |
| $6$         | $2899.11$                                        |
| $7$         | $3231.85$                                        |
| $8$         | $3638.52$                                        |
| $9$         | $5642.91$                                        |
| $10$        | $5989.38$                                        |

Very similar considerations to the case above apply: from the table above and the animations below we can see that the first mode involving the tines (mode $3$) has not significantly changed in frequency or shape, while the change in boundary conditions mainly affects the other overtones and undertones. Similarly to the previous case, the first two modes are simply bulk oscillation of the tuning fork around its constrained part.

### First Two Modes
{{< youtube id="PgFDjmBOi3Y" title="First Two Modes" >}}

### Modes $3$ to $10$
{{< youtube id="iQQRaXZZYJ0" title="Modes 3 to 10" >}}

## Comparison of Principal Modes

The first mode which involves the tines on their own is called the _principal_ mode of the tuning fork and it is responsible for radiating the reference frequency of the system. We can compare the solutions for this mode as in the plot below:

{{< figure src="/website/posts_res/26-tuning-fork-part-2/comparison.png" title="Figure 1" caption="Comparison of the Principal Modes." class="mw10" >}}

The plot above shows that the principal mode is really not affected by boundary conditions applied to the prong. This is to be expected as the principal mode, even in the free case, only very marginally involves the prong. Or, in other words, only the modes that involve the prong in appreciable ways are affected by boundary conditions applied to it.

# Conclusion

In this episode we studied the solution obtained from the study we setup in the [Tuning Fork - Part 1]({{< ref "/posts/25-tuning-fork-part-1" >}}) episode. We found that a tuning fork has a plethora of inharmonic modes. However, the principal mode, the first mode involving the tines, is not impacted by boundary conditions applied to the prong. The principal mode is the one associated with the nominal frequency of the fork and the one responsible for radiating it. This shows why tuning forks can be handled at the prong without impacting the fundamental of the acoustic wave they emit.

We also seen that the principal mode frequency from our simulation is not the expected one. This is most likely due to our material properties not exactly matching those of the real fork, which are unknown.

In the next episodes of this series we will aim to build coupling with air, so to study the acoustic wave radiated by the system. 

{{< cc >}}

