---
title: "Rigid Walled Room Revisited - Part 3"
date: 2020-09-26T14:37:36+01:00
draft: false
featured_image: "/posts_res/17-rigid-walled-room-revisited-part-3/featured.png"
description: "Mesh Order and Accuracy"
categories: ["Modelling"]
tags: ["Physics", "Elmer", "FEM", "Linear System", "Single Physics", "Steady State", "Eigensystem", "Series: Rigid Walled Room"]
---

In the [Rigid Walled Room Revisited - Part 2]({{< ref "/posts/16-rigid-walled-room-revisited-part-2.md" >}}) episode we reviewed the solution provided by the Elmer Wave Equation solver. In that study we solved for the eigenfrequencies and eigenmodes of a rectangular room by using a first order mesh. The results we found were very accurate already, with eigenfrequencies within $1$ $\text{Hz}$ from the exact value and eigenmodes accurate within $1.4\text{%}$. However, we seen in the [Mesh Order and Accuracy]({{< ref "/posts/14-mesh-order-and-accuracy.md" >}}) episode that we can significantly increase the accuracy of results by acting on the mesh fineness and, more effectively, the order. In this episode we will solve the rigid walled room eigenproblem with a second order mesh to quantify what effects this has on the accuracy of the solution.

# Project Files

All the files used for this project are available at the repositories below:

* [Rigid Walled Room 4](https://gitlab.com/computational-acoustics/rigid-walled-room-4).

# The Improved Study

Luckily for me Peter Råback, one of the main developers of Elmer, provided a lot of possible improvements for this study back on an [Elmer forums thread that I opened](http://www.elmerfem.org/forum/viewtopic.php?f=3&t=7153&sid=16958e759ceb346623f63a27afa70e94). In essence, this article is the result of implementing the improvements suggested by Peter in [this post](http://www.elmerfem.org/forum/viewtopic.php?p=23255#p23255).

## Solver Output Data Configuration 

In the [Rigid Walled Room Revisited - Part 2]({{< ref "/posts/16-rigid-walled-room-revisited-part-2.md" >}}) episode we noted how the the `vtu` file which, by default, Elmer outputs contains as many scalar fields as the number of eigenvalues (and eigenfunctions) we are searching for. This makes processing with ParaView a bit cumbersome, so the various fields were separated in `vtu` timestamped files programmatically during postprocessing. Turns out this needs not to be done. In fact, the `Simulation` section of the Solver Input File supports the `vtu: Eigen Analysis` keyword that, when set to `True`, will instruct the VTU writer to output timestamped files just like the ones we produced through postprocessing, i.e. a collection of `vtu` files numbered by eigenmode.

The improved Solver Input File `Simulation` section is reported below:

```text
Simulation
  Max Output Level = 5
  Coordinate System = Cartesian
  Coordinate Mapping(3) = 1 2 3
  Simulation Type = Steady state
  Steady State Max Iterations = 10
  Output Intervals = 1
  Timestepping Method = BDF
  BDF Order = 1
  Coordinate Scaling = 0.001
  Solver Input File = case.sif
  Post File = case.vtu
  vtu: Eigen Analysis = Logical True
End
```

Where the `vtu: Eigen Analysis` keyword has been set at the bottom of the section.

## Second Order Mesh

In the [Mesh Order and Accuracy]({{< ref "/posts/14-mesh-order-and-accuracy.md" >}}) episode we set the mesh order at "the root", that is, by using the meshing software itself. This is a totally legit way to set the order of the mesh, however there are is also a more convenient way within Elmer.

Certain solvers, and the Wave Equation solver is one of those, can make use of the [hierarchical p-element base](http://www.nic.funet.fi/pub/sci/physics/elmer/doc/ElmerSolverManual.pdf#appendix.E), which means that the element order can be configured up to order $10$ within Elmer. Not only that, but Elmer will make use of a smart implementation of basis functions that offer many advantages, both in terms of accuracy and computational efficiency, with respect ordinary polynomial basis functions (refer to the Elmer Solver Manual linked above for more information, some information is also available [in this Elmer forum thread](http://www.elmerfem.org/forum/viewtopic.php?f=3&t=7174)). We will use the p-elements for this study.

Enabling of the p-order elements is simply done by configuring the `Element` keyword for the `Solver` section defining the properties of our Wave Equation solver:

```text
Solver 1

  ! Equation
  Equation = "Wave Equation"
  Procedure = "WaveSolver" "WaveSolver"
  Variable = "Pressure"
  Variable DOFs = 1
  
  ! Solver
  
  ! Steady State
  Steady State Convergence Tolerance = 1e-09
  
  ! Nonlinear
  ! Nonlinear System Convergence Tolerance = 1.0e-7
  ! Nonlinear System Max Iterations = 1
  ! Nonlinear System Newton After Iterations = 3
  ! Nonlinear System Newton After Tolerance = 1.0e-3
  ! Nonlinear System Relaxation Factor = 1
  
  ! Linear
  ! Linear System Scaling = Logical True
  ! Linear System Symmetric = Logical True
  Linear System Solver = string "Iterative"
  Linear System Direct Method = Umfpack
  Linear System Iterative Method = BiCGStabl
  Linear System Max Iterations = integer 10000
  Linear System Convergence Tolerance = real 1e-8
  BiCGstabl polynomial degree = 4
  Linear System Preconditioning = ILU2
  Linear System ILUT Tolerance = 1.0e-3
  Linear System Abort Not Converged = True
  Linear System Residual Output = 50
  Linear System Precondition Recompute = 1
  
  ! Special
  Eigen Analysis = True
  Eigen System Values = 10
  Eigen System Select = smallest magnitude
  Eigen System Convergence Tolerance = Real 1.0e-9
  Eigen System Max Iterations = 10000
  ! Eigen System Shift = Real 1000
  Eigen System Normalize To Unity = Logical True
  Element = "p:2"
  
End
```

You can see that the `Element` keyword is configured for second order p-elements (`p:2`) at the bottom of the `Solver 1` section.

The clear advantage of p-elements (beside those mentioned above which come from their implementation within Elmer) is that we do not have to recompute the whole mesh, but just set the value of the keyword. This facilitates the study of the effect of order on accuracy, which could be carried on even programmatically.

# Results

The table below shows a comparison of the FEM resonance frequencies with the exact ones (rounded to two decimal places):

{{< load-table-dfl-style >}}

| Mode Number | FEM Resonance Frequency $\text{Hz}$ | Exact Resonance Frequency $\text{Hz}$ |
|-------------|-------------------------------------|---------------------------------------|
| $1$         | $3.8 \cdot 10^{-6}$                 | $0$                                   |
| $2$         | $34.30$                             | $34.30$                               |
| $3$         | $42.88$                             | $42.88$                               |
| $4$         | $54.91$                             | $54.91$                               |
| $5$         | $57.17$                             | $57.17$                               |
| $6$         | $66.67$                             | $66.67$                               |
| $7$         | $68.60$                             | $68.60$                               |
| $8$         | $71.46$                             | $71.46$                               |
| $9$         | $79.26$                             | $79.26$                               |
| $10$        | $80.90$                             | $80.90$                               |

Clearly, the results are much more accurate with respect those found in the [Rigid Walled Room Revisited - Part 2]({{< ref "/posts/16-rigid-walled-room-revisited-part-2.md" >}}) episode. In fact, the numbers are all the same (to two decimal places).

We can plot the error metric $E\_{n}$ defined in the [Rigid Walled Room Revisited - Part 2]({{< ref "/posts/16-rigid-walled-room-revisited-part-2.md" >}}) episode to check for any patterns in the error (as a reminder, $E\_{n}$ is just the distance, in cents, between the FEM eigenfrequency and the exact one).

{{< plotly fname="/posts_res/17-rigid-walled-room-revisited-part-3/eigen-error-cents.html" height_px=512 >}}

When it comes to the error in cents we can see that the trend of increasing error with mode number (and hence frequency) is preserved (as expected). However, note how the error is now much smaller. In the [Rigid Walled Room Revisited - Part 2]({{< ref "/posts/16-rigid-walled-room-revisited-part-2.md" >}}) episode the error reached almost $4$ $\text{cent}$ at worst. In this case, it barely reaches $0.003$ $\text{cent}$. In essence, switching to second order has reduced the eigenfrequency error, in cents, by a factor of $\sim1300$, which is a very remarkable improvement.

Again, whether errors of this magnitude are acceptable or not depends on the application. With reference to musical acoustics applications, as those we considered as an example in the [Rigid Walled Room Revisited - Part 2]({{< ref "/posts/16-rigid-walled-room-revisited-part-2.md" >}}) episode, these errors are completely and safely negligible.

# Eigenmodes

The video below shows a comparison of the eigenmodes with the exact solution.

{{< youtube _Y4Eb0Qi6Y8 >}}

The FEM solution is presented on the top left, while the bottom left represents the FEM solution with sign correction (for details about the sign correction refer to the [Rigid Walled Room Revisited - Part 2]({{< ref "/posts/16-rigid-walled-room-revisited-part-2.md" >}}) episode). The right half of the video shows the exact solution. It is possible to see that accuracy is very good, a part for the overall sign inversion that happened for some eigenmodes. The values of the sign correction $s\_{n}$ are reported in the table below.

| Mode Number | $s\_{n}$ |
|-------------|----------|
| $1$         | $+1$     |
| $2$         | $+1$     |
| $3$         | $-1$     |
| $4$         | $-1$     |
| $5$         | $-1$     |
| $6$         | $+1$     |
| $7$         | $+1$     |
| $8$         | $-1$     |
| $9$         | $-1$     |
| $10$        | $+1$     |

We can see that the values are not the same as those found in the [Rigid Walled Room Revisited - Part 2]({{< ref "/posts/16-rigid-walled-room-revisited-part-2.md" >}}) episode. This has to be expected, as sign inversion is somehow unpredictable.

The video below shows a more quantitative error analysis:

{{< youtube etAf2_IKeUg >}}

We can see that the maximum error in the magnitude of the solution has dropped from $1.4\text{%}$ to $0.017\text{%}$ when switching to second order meshes. In terms of decibels, the maximum magnitude error moved from $\sim0.1$ $\text{dB}$ to $\sim0.001$ $\text{dB}$. The improvement in decibel error is then around a factor of $100$. This is, again, a very remarkable improvement in accuracy. The application has to be kept in mind when deciding whether or not an error of this magnitude is acceptable, but I am not aware of any instrumentation accurate and precise enough to be able to possibly consistently measure a difference of $0.001$ $\text{dB}$. That is, the accuracy of this simulation would be superior to that of a measurement of the exact field performed with state of the art instrumentation (if we could measure an exact room eigenmode, that is).

The eigenmode affected by the greatest errors is still mode number $5$, which has the fewer elements along the direction of maximal variation of the mode. This prompts us to consider better meshing techniques if we want to uniform accuracy among all eigenmodes. Errors appear still to be somewhat larger where the magnitude of the eigenmode is larger, although the spread of errors seems more uniform throughout the domain with respect what was observed in the [Rigid Walled Room Revisited - Part 2]({{< ref "/posts/16-rigid-walled-room-revisited-part-2.md" >}}) episode.

# Conclusion

In this episode we seen how to improve the study results presented in the [Rigid Walled Room Revisited - Part 2]({{< ref "/posts/16-rigid-walled-room-revisited-part-2.md" >}}) episode. We seen that:

1. Elmer can be configured to output the eigenmodes in separate `vtu` files; and
2. The use of higher order p-elements increases the accuracy of the solution to very high levels.

Point 2 ties in nicely with what was observed in the [Mesh Order and Accuracy]({{< ref "/posts/14-mesh-order-and-accuracy.md" >}}) episode, where we seen that mesh order is much more effective in rising accuracy with respect mesh fineness. Of course, higher mesh order implies higher computational costs and possibly harder to achieve convergence, so a balance has to be achieved in practice.

{{< cc >}}

