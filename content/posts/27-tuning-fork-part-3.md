---
title: "Tuning Fork - Part 3"
date: 2021-06-12T15:36:33+01:00
draft: false
featured_image: "/posts_res/27-tuning-fork-part-3/featured.png"
description: "Weak Coupling Between Tuning Fork and Air"
categories: ["Modelling"]
tags: ["Physics", "Elmer", "FEM", "Linear System", "Weak Coupling", "Series: Tuning Fork"]
---

In the [Tuning Fork - Part 1]({{< ref "/posts/25-tuning-fork-part-1" >}}) and [Tuning Fork - Part 2]({{< ref "/posts/26-tuning-fork-part-2" >}}) we set up and solved an Elmer study for the modes of vibration of a tuning fork. We learned that the principal mode of the fork, the one related to the nominal frequency of the fork, is very marginally affected by boundary conditions affecting the handle of the fork. In this episode we will attempt studying the acoustic field emitted by the fork in the surrounding air. In other words, we will start exploring the coupling of vibration and acoustics.

<!--more--> 

# Project Files

All the files used for this project are available at the repositories below:

* [Tuning Fork 2](https://gitlab.com/computational-acoustics/tuning-fork-2).

# A Note on Terminology

In this previous episodes of this series, quite inappropriately, the handle of the tuning fork was referred as _prong_ while its prongs were be referred as _tines_. From this episode we will use the more correct terminology of _handle_ and _prongs_.

# The Problem of Coupling

That of coupling is a fundamental problem in physics and one of central importance in acoustics and it can be treated in few different ways. In its broader sense, we have coupling when we have bodies in a contact of some type (mechanical, thermal, electromagnetic...). This means that the behaviour of the fields in the respective bodies is influenced by that of the others.

For example, think of a tuning fork. We know its modes in a vacuum, but if we place the same tuning fork in a fluid (air, water, ...) the pressure of this fluid will _load_ the structure, affecting how it can vibrate. Will this change the modes? But also, if the fork vibrates, these vibrations will propagate through the fluid. Or, in other words, the _displacement_ vector field within the fork and the _pressure_ scalar field (as well as the _particle velocity_ vector field) through the fluid are mutually connected. We can expect the vibration in the fluid to affect the mechanics of the fork back as well.

What we described above is an example of _strong coupling_, in which there is mutual and constant simultaneous back and forth interaction between the connected systems. Note how this means that, even in the case of linear governing equations, coupled systems might not necessarily behave, when connected, as they do in isolation. A prime example of this is electric circuits, especially passive networks. We can make, for example, two analogue filter with passive components (capacitors, resistors...) and measure their frequency responses. If we now cascade them the frequency response of the final network might very well be very different with respect what we expect from taking the product of the two original frequency responses, which is the expected result in case of no mutual coupling between the two networks. In other words, by cascading two linear systems we made a third linear system with different response.

In the case of electric networks the problem is formulated in terms of input and output impedance. If the output impedance of a network is much smaller then the input impedance of the subsequent network then the networks behave as _single units_: the global response when cascading them is the product of the single responses. That of impedance is a formalism that applies to acoustics as well. We have in fact already encountered acoustic impedance in various different ways.

In the case in which systems can be treated as "independent" units we can describe the global system in terms of _weak coupling_. In weak coupling the field from one of the systems in contact serves to compute the boundary condition at the contact surface with the other systems. This boundary condition serves as a _wake_ for the field supported by the other systems.

Let's consider now our problem. What we want to study is the vibroacoustic coupling between a tuning fork and a volume of air in which it is immersed. As we explained above the displacement field within the fork and the pressure field within the air are intimately connected. However, in this episode we will study the coupling in the weak coupling approximation, i.e. we will have the fork to provide the wake for the acoustic field. This is motivated by a couple of reasons.

1. The acoustic impedance of aluminium is much higher than that of air. From [Onda Corporation](https://www.ondacorp.com/wp-content/uploads/2020/09/Solids.pdf) (see also [abbreviations](https://www.ondacorp.com/wp-content/uploads/2020/09/Abbreviations.pdf)) we see that we expect impedance in the order of $17 \cdot 10^\{6\}$ $\left\[\text{MKS}\right\]$ $\text{Rayl}$ while that of air is $\rho c = 413.315$ $\left\[\text{MKS}\right\]$ $\text{Rayl}$ (at room temperature), with $\rho$ the density of air and $c$ the speed of sound in air. This means that the acoustic waves at the interface between air and fork will be mostly reflected, with little to no transmitted field within the fork to perturb the the displacement field within.
2. We will attempt to model the volume of air as infinite, so there will not be acoustic waves radiating back towards the fork.

Care should be taken with condition 1, as simple reasoning in terms of specific acoustic impedance really applies best to plane wave propagation traversing a boundary between bodies infinite bodies. Our case is clearly different. Still, we expect the back coupling from air to fork to be much less significant than the direct coupling.

The reasons above, in combination, tell us that we can approximate the problem as that of independent systems, and simply let the fork activate the air knowing that the fork is hardly activated by air. Or, in other words, we approximate the coupling as that of weak coupling.

This approximation is also motivated by and additional reason: the best way to treat problems in physics is with increasing steps of complexity, as we already stated in multiple episodes. In this analysis we add one single layer of complexity on top of what we did so far, and we will take it further in other episodes. By building complexity layer by layer we will develop an understanding on the correctness of simplifying assumptions and in what conditions they break.

# Model Setup

Even though in the last few episodes we setup our studies by writing the `sif` file directly, in this episode we will use `ElemrGUI`. `ElmerGUI` is in constant improvement and for problems for which we do not need special features is very often the quickest way to setup a study, so it is good to keep an eye on its features.

We will assume all the same geometry and material parameters for the tuning fork as in the the [Tuning Fork - Part 1]({{< ref "/posts/25-tuning-fork-part-1" >}}) episode, while we will assume the air to be at room temperature.

We aim to model the sound radiated by the fork principal mode when the handle bottom is simply supported, which is $528.90$ $\text{Hz}$.

## Geometry

Our geometry consists simply of the tuning fork from the [Tuning Fork - Part 1]({{< ref "/posts/25-tuning-fork-part-1" >}}) episode surrounded by a body of air. Orientation of the fork is not important, but in this study the bottom of the handle was placed in the centre of the $XY$ plane, as shown below. The figure also shows the body of air. This is a simple sphere centred at half the height of the fork ($82$ $\text{mm}$). The radius of the sphere is $250$ $\text{mm}$.

{{< figure src="/website/posts_res/27-tuning-fork-part-3/geometry.png" title="Figure 1" caption="Body Model Geometry." class="mw10" >}}

## Meshing

### Geometry Preparation

With reference to our geometry in `FreeCAD`, we will need to export both our entities, the fork and the sphere, in a `BREP` file. To do so, select both of them from the _Combo View_ as shown below and then select _File_ > _Export_ to save as a `BREP` file.

{{< figure src="/website/posts_res/27-tuning-fork-part-3/freecad-export.png" title="Figure 2" caption="FreeCAD Entity Export." class="mw6" >}}

Then, as usual, we can open up Salome and import our `BREP` file in the _Geometry_ module. In this episode we will be using Salome 9.6.0.

Select the _Geometry_ module, then _File_ > _Import_ > _BREP_. If the file we import was named as `geometry.brep` then we will have an entity called _geometry.brep\_1_ in the _Object Browser_. Select this entity and then _New Entity_ > _Explode_. This will popup a window. Let's explode our base entity in solids, as shown below.

{{< figure src="/website/posts_res/27-tuning-fork-part-3/explode.png" title="Figure 3" caption="Base Entity Explode Operation." class="mw6" >}}

This will produce two solid entities under _geometry.brep\_1_ in the _Object Browser_, one for the fork and one for air. I find beneficial to right click on them and rename them so that I don't loose track of them. In the following we will assume that the sphere was named as _Air_ and the fork as _Fork_.

Now it comes the **crucial** bit. We need to create a partition. That is, we will create a single solid entity in which part of the volume is allocated solely to the fork and the remaining solely to the air. To do so, select the _Air_ solid first from the _Object Browser_. Then _Operations_ > _Partition_. This will open a new window. Click the small curly arrow next to _Tool Objects_ and select the _Fork_ solid entity from the _Object Browser_, as shown below.

{{< figure src="/website/posts_res/27-tuning-fork-part-3/partition.png" title="Figure 4" caption="Partition Operation." class="mw6" >}}

You can change the _Name_ field to something else. I decided to go for _Domain_, but you can rename the resulting entity afterwards.

Finally, we explode our newly created partition first in solids, as before, and then again in _Faces_ (simply select _Face_ from the _Sub-shapes Type_ combo menu).

The partition object, with all of its sub entities, is the entity we will mesh. The _Object Browser_ is shown below with said entity highlighted. Again, I renamed the solids for the air and fork respectively, so to always know where they are.

{{< figure src="/website/posts_res/27-tuning-fork-part-3/hierarchy.png" title="Figure 5" caption="Partition Entity Hierarchy." class="mw6" >}}

### Meshing

Switch to the _Mesh_ module and select our partition object, called _Domain_ in this example. Then _Mesh_ > _Create Mesh_. This will popup a new window. From _Algorithm_ we select our old trustworthy _NETGEN 1D-2D-3D_, as shown below.

{{< figure src="/website/posts_res/27-tuning-fork-part-3/mesh-1.png" title="Figure 6" caption="Mesh Creation." class="mw6" >}}

Then, we click the gear button next to the _Hypothesis_ combo menu and select _NETGEN 3D Parameters_. This will popup a new window. As a first step lets just set a global size for the entirety of the domain (air and fork) which is compatible with the accuracy of the pressure wave solution. We use our typical rule:

$$s<\frac{\lambda}{10}=\frac{c}{10f}$$

Where $s$ is the maximum mesh size and $f$ is the frequency of the simulation. Our frequency is $528.90$ $\text{Hz}$. Hence our limit size is $64.9$ $\text{mm}$. To give it a good accuracy margin, we select a max size of $15$ $\text{mm}$, a min size of $1$ $\text{mm}$ and a _Moderate_ _Fineness_, as shown below. We keep the mesh first order as we will use $p$-elements within Elmer.

{{< figure src="/website/posts_res/27-tuning-fork-part-3/mesh-2.png" title="Figure 7" caption="Initial Mesh Parameters." class="mw6" >}}

Then, we go to the _Local sizes_ tab and assign a mesh size of $1.5$ $\text{mm}$ (as in the [Tuning Fork - Part 1]({{< ref "/posts/25-tuning-fork-part-1" >}}) episode) to the fork as follows: from the _Object Browser_ select the _Fork_ entity under our partition _Domain_ entity and then click the button _On solid_. Then, edit the value. The result is shown below.

{{< figure src="/website/posts_res/27-tuning-fork-part-3/mesh-3.png" title="Figure 8" caption="Mesh Local Sizes." class="mw6" >}}

In Salome 9.6.0 groups from geometry should be automatically created, so we will see the following hierarchy in the _Object Browser_:

{{< figure src="/website/posts_res/27-tuning-fork-part-3/mesh-4.png" title="Figure 9" caption="Mesh Groups Hierarchy." class="mw6" >}}

If this does not happen, select your mesh object (_Mesh\_1_ in this example), right click on it and select _Create Groups from Geometry_. Select then all the sub-entities of the partition object _Domain_ from the _Object Browser_.

We can then right click our mesh object from the object browser and choose _Compute_. The resulting mesh will look as follows:

{{< figure src="/website/posts_res/27-tuning-fork-part-3/mesh-all.png" title="Figure 10" caption="Mesh (Whole Domain)." class="mw10" >}}

{{< figure src="/website/posts_res/27-tuning-fork-part-3/mesh-zoom.png" title="Figure 11" caption="Mesh (Zoom to Fork)." class="mw10" >}}

Where we configured a clipping plane simply by right click anywhere in the 3D view and selecting _Clipping_ (note that the clipping only affects visualisation).

It is possible to see that we realised a _conformal_ mesh: the mesh from the fork "fades" with continuity into that of the air. This is very crucial and the most important step to setup many multiphysics problems properly.

The groups of geometry in the mesh will be treated as follows by Elmer:

* Groups of volumes will be seen as _bodies_: we will be able to apply governing equations to them.
* Groups of faces will be seen as _boundaries_: we will be able to apply boundary conditions to them.

We can now right click our mesh, choose _Export_ > _UNV File_ to export it as `UNV`, a format Elmer (more accurately `ElmerGrid`) understands.

## Elmer Study

We can now open ElmerGUI by issuing the command `ElmerGUI` in a terminal.

From _File_, select _Open_ and navigate to our `UNV` file exported from Salome. When the file is loaded you will see the domain in the middle of the 3D view, similarly to the figure below.

{{< figure src="/website/posts_res/27-tuning-fork-part-3/screen.png" title="Figure 12" caption="UNV Opened in ElmerGUI." class="mw10" >}}

Then we go in order through the steps of the _Model_ menu.

### Setup

Choose _Model_ > _Setup_. We will search for harmonic solutions for both the displacement field fork and the pressure field of the air. Both solvers will need then a _Frequency_ keyword to be set. We can avoid this by creating a global _Frequency_ keyword to which all harmonic solvers can lookup.

#### Header

First, lets create a variable to hold our frequency for the simulation. We will support the bottom of the fork handle, so we go into the `eigenvalues_bottom_prong.dat` exported by the [Tuning Fork - Part 1]({{< ref "/posts/25-tuning-fork-part-1" >}}) simulation and use the exact value to define a variable `f` in the _Free text_ field of the _Header_ section:

```text 
$ f = 528.9081189009062
```

For more information about the various eigenfrequencies see the [Tuning Fork - Part 2]({{< ref "/posts/26-tuning-fork-part-2" >}}) episode. Our variable `f` will be used to set the frequency.

#### Simulation

In our simulation section, let's put `0.001` in the _Coordinate Scaling_ field, so that Elmer knows the coordinates of our mesh as exported by Salome are in $\text{mm}$. In the _Free text_ field we define the _Frequency_ keyword:

```text 
Frequency = $f
```

This will ensure that the harmonic solvers have a value of frequency to look up to. note that we could have simply done:

```text 
Frequency = 528.9081189009062
```

For this particular problem perhaps the variable `f` is not necessary. However, it is a good habit to define variables (or arrays) in the header so to have them editable in one place, and the rest of the simulation will adapt as required.

#### Summary

All other parameters can be kept as default. Your _Setup_ window should look as follows:

{{< figure src="/website/posts_res/27-tuning-fork-part-3/setup.png" title="Figure 13" caption="Setup Settings." class="mw8" >}}

Click apply to finish the configuration.

### Equation: Elasticity

Select _Model_ > _Equation_ > _Add_. Scroll the tabs until you find the _Linear elasticity_ tab. Activate this equation by clicking the _Active_ tickbox. In the _Name_ field, type _Elasticity_ (for easy reference). We will apply this equation to the fork later. Click now the _Edit Solver Settings_ button. This will open the solver settings window.

In the _Linear System_ tab, select a _MUMPS_ as a _Direct_ method. This method was selected as, after some trial end error, it appeared to perform better for this problem (solves without warning and errors).

{{< figure src="/website/posts_res/27-tuning-fork-part-3/linear.png" title="Figure 14" caption="Elasticity Linear System Settings." class="mw8" >}}

In the _Nonlinear System_ set the _Max. iterations_ field to _1_ since our problem is linear (our fork equation and material are fully linear).

{{< figure src="/website/posts_res/27-tuning-fork-part-3/nonlinear.png" title="Figure 15" caption="Elasticity Nonlinear System Settings." class="mw8" >}}

In _Solver specific options_ click the _Harmonic Analysis_ tickbox and in the _Free text input_ insert the following:

```text 
Element = "p:2"
Variable = -dofs 3 Displacement
```

The first instruction tells Elmer to use second order $p$-elements for the mesh. The second instruction defines the `Displacement` variable name to use for our coupling boundary condition.

{{< figure src="/website/posts_res/27-tuning-fork-part-3/specific.png" title="Figure 16" caption="Elasticity Solver Specific Options." class="mw8" >}}

 Click _Apply_ to finish the configuration.

### Equation: Helmholtz

Select _Model_ > _Equation_ > _Add_. Scroll the tabs until you find the _Helmholtz Equation_ tab. Activate this equation by clicking the _Active_ tickbox. In the _Name_ field, type _Helmholtz_ (for easy reference). We will apply this equation to the fork later. Click now the _Edit Solver Settings_ button. This will open the solver settings window.

In the _Linear System_ tab, select the _BiCGStabl_ _Iterative_ method. Set a high number of iterations (_10000_, but you will need way less then this) and _ILUT_ preconditioning. Click the _Abort if the solution did not converge_ tickbox so that we do not risk to keep bad solutions.

{{< figure src="/website/posts_res/27-tuning-fork-part-3/linear-2.png" title="Figure 17" caption="Helmholtz Linear System Settings." class="mw8" >}}

In the _Nonlinear System_ set the _Max. iterations_ field to _1_ since our problem is linear.

{{< figure src="/website/posts_res/27-tuning-fork-part-3/nonlinear-2.png" title="Figure 18" caption="Helmholtz Nonlinear System Settings." class="mw8" >}}

In the _Solver specific options_ put the following text in the _Free text input_ field

```text 
Element = "p:2"
Displacement Variable EigenMode = Integer 1
```

The first line tells Elmer to apply second order $p$-elements to this equation. The second instead is necessary for the coupling boundary condition. In essence, the harmonic solution from the fork will be stored as an Eigenmode and used for the wake on the boundary we will select for the _interface_ between fork and air. Only the value `1` appears to work for this simulation.

{{< figure src="/website/posts_res/27-tuning-fork-part-3/specific-2.png" title="Figure 19" caption="Helmholtz Solver Specific Options." class="mw8" >}}

Click _Apply_ to finish the configuration.

### Materials

We need to define two materials, air for the air body and Aluminium 6061 for the fork, as done in the [Tuning Fork - Part 1]({{< ref "/posts/25-tuning-fork-part-1" >}}) episode.

#### Materials: Air

Select _Model_ > _Material_ > _Add_. This will open a new material window. Click the _Material library_ button. This will open a new window with a list of materials. Select _Air (room temperature)_ and click _Ok_, as shown below. This will close the materials list window.

{{< figure src="/website/posts_res/27-tuning-fork-part-3/materials.png" title="Figure 20" caption="Materials List." class="mw6" >}}

In the material window type _Air_ in the _Name_ field, for easy reference. We will apply this material to the air body later.

Your _General_ and _Helmholtz Equation_ tabs for this material should look as follows.

{{< figure src="/website/posts_res/27-tuning-fork-part-3/air-general.png" title="Figure 21" caption="Air Material: _General_ Tab." class="mw8" >}}

{{< figure src="/website/posts_res/27-tuning-fork-part-3/air-helmholtz.png" title="Figure 22" caption="Air Material: _Helmholtz Equation_ Tab." class="mw8" >}}

Click _Ok_ to finish the configuration.

#### Materials: Aluminium 6061

Select _Model_ > _Material_ > _Add_. This will open a new material window. Aluminium 6061 is not available in the library, so we will input the parameters listed in the [Tuning Fork - Part 1]({{< ref "/posts/25-tuning-fork-part-1" >}}) episode manually.

In the _General_ tab, type _2712.63_ in the _Density_ field, as shown below.

{{< figure src="/website/posts_res/27-tuning-fork-part-3/alu-general.png" title="Figure 23" caption="Fork Material: _General_ Tab." class="mw8" >}}

In the _Linear elasticity_ tab, type _68.9e9_ in the _Youngs modulus_ field and _0.33_ in the _Poisson ratio_ field, as shown below.

{{< figure src="/website/posts_res/27-tuning-fork-part-3/alu-linearel.png" title="Figure 24" caption="Fork Material: _Linear elasticity_ Tab." class="mw8" >}}

In the _Name_ field, type _Aluminium 6061_ for easy reference. We will apply this material to the air body later. Click _Ok_ to finish the configuration.

### Body Force

We will communicate the harmonic excitation to the fork by making use of a body force. A body force acts throughout the entire volume of the body. In other words, we will excite the fork from "within", making sure to excite each single (continuous) element of it. The modal behaviour of the system will ensure that the resulting vibration pattern is still modal.

Of course, this is not the only possible way to excite the fork, we could apply a boundary condition for this. However, a body force is particularly advantageous here as it allows to keep all boundaries free to couple with air. This in turn allows us to see the field radiated by the fork principal mode with clarity.

With reference to the governing equation, equation 6.1 of the [Models Manual](http://www.nic.funet.fi/index/elmer/doc/ElmerModelsManual.pdf#chapter.6), we will be defining the term $\overrightarrow{f}$, a 3D vector which has the dimensions of force per unit volume, as we would expect for a force acting on each (continuous) volume element.

To define the volume force, select _Model_ > _Body force_ > _Add_. This opens a new window. In the new window, scroll the tabs until you reach the _Linear elasticity_ tab and select it. We will apply a $z$ directed volume force of $10^6$ $\frac{\text{N}}{\text{m}^{3}}$. To do so we simply insert `1e6` into the _Force 3_ field and type _Excitation_ in the _Name_ field for easy reference, as done below. We will apply this body force to the fork later. Click _Ok_ to finish the configuration.

{{< figure src="/website/posts_res/27-tuning-fork-part-3/body-force.png" title="Figure 25" caption="Body Force for Linear Elasticity." class="mw8" >}}

### Boundary Conditions

We will need to apply three boundary conditions to implement our simulation:

1. A simply supported boundary condition for the linear elasticity equation applied to the fork handle base.
2. An interface boundary condition for the Helmholtz equation applied on all the remaining fork boundaries.
3. A "no reflections" outlet boundary condition for the Helmholtz equation applied to the outer end of the domain.

Conditions 1 and 2 are fairly straightforward. Condition 3 is a bit more tricky. We want no reflections so that we can simulate an infinite body of air. If we achieve that, our solution will simply look like observing what happen in an infinite body of air when we focus only a spherical portion of it. Perfect absence of reflections would happen if we were able to match the impedance at the boundary with that of the wavefront. We did exactly this in [The Pulsating Sphere]({{< ref "/posts/8-the-pulsating-sphere" >}}) episode. However, in this case we have no idea how the field will look like, so we cannot compute the specific acoustic impedance $z$ at the boundary.

Luckily, far away from any kind of source all wavefronts become plane, hence the plane wave specific acoustic impedance $\rho c$ applies. Although our domain is actually quite small (the entire domain diameter is only around $1.3$ times bigger then the wavelength) we will use this impedance at the outer boundary, to keep things simple while not bloating the computational requirements with an excessively big domain.

As a note, a typical solution to implement the "no reflections" boundary condition for wave equations is the Perfectly Matched Layer (PML). With PML a new small domain is defined as a layer around the main domain where a modified wave equation, coupled with the main wave equation in the main domain, is defined. This modified equation is such that the wave undergoes exponential damping within the layer. The implementation of PML is possible for acoustic solvers, but not available in Elmer at the moment.

#### Outlet

Select _Model_ > _Boundary condition_ > _Add_. This opens a new window. In the new window, scroll the tabs until you reach the _Helmholtz Equation_ tab and select it. To define a plane wave impedance, simply tick the _Plane wave outlet condition_ tickbox. Also type _Outlet_ in the _Name_ field, for easy reference, as shown below. We will apply this condition to the boundary later. Click _Ok_ to finish the configuration.

{{< figure src="/website/posts_res/27-tuning-fork-part-3/outlet.png" title="Figure 26" caption="Outlet Boundary Condition." class="mw8" >}}

#### Coupling

This condition is a bit different from others, but can be setup from ElmerGUI. Select _Model_ > _Boundary condition_ > _Add_. This opens a new window. In the new window, scroll the tabs until you reach the _General_ tab and select it (it should be the first tab). In the _Free text input_ textbox input the following string:

```text 
Structure Interface = Logical True
```

Also type _Coupling_ in the _Name_ field, for easy reference. The resulting configuration is shown below. We will apply this condition to the relevant boundaries later.

{{< figure src="/website/posts_res/27-tuning-fork-part-3/coupling.png" title="Figure 27" caption="Coupling Boundary Condition." class="mw8" >}}

Thanks to the special keywords we defined in both the linear elasticity and Helmholtz solvers, this keyword will take care to apply displacement from the linear elasticity solver to wake the air in the body governed by the Helmholtz equation, at the boundary where this condition is applied.

#### Support

Select _Model_ > _Boundary condition_ > _Add_. This opens a new window. In the new window, scroll the tabs until you reach the _Linear elasticity_ tab and select it. To define a simple support, simply force $x$, $y$ and $z$ displacements to be $0$ by setting to _0_ the keywords _Displacement 1_, _Displacement 2_ and _Displacement 3_ respectively. Also set the _Name_ field to _Support_, for easy reference. the boundary condition configuration is shown below. We will apply this condition to the boundary later. Click _Ok_ to finish the configuration.

{{< figure src="/website/posts_res/27-tuning-fork-part-3/support.png" title="Figure 28" caption="Support Boundary Condition." class="mw8" >}}

### Applying Equations, Materials, Body Forces and Boundary Conditions

In the sections above we defined equations, materials, body forces and boundary conditions but we did not apply them to any entity. Of course, it is completely fine to apply each of them to the required entity at definition, but we can do that afterwards graphically in case the name of the entities are not immediately clear.

Select _Model_ > _Set body properties_. Now we can double click on bodies and apply equations, materials and body forces to them.

By double clicking the outer boundary we select the body of air, which then turns red. We can then assign the equation and material to it from the drop down menus, as shown below. Click _Update_ to finish the configuration.

{{< figure src="/website/posts_res/27-tuning-fork-part-3/air.png" title="Figure 29" caption="Air Body Properties." class="mw10" >}}

To reveal the fork we can zoom into the geometry with the mouse wheel until we clip through the outer shell and see the fork within. Unfortunately we cannot select the fork body by double clicking on any of its boundaries, perhaps due to how the mesh groups are setup. So, to assign body properties we simply go back to the definitions.

If we navigate to _Model_ > _Equation_ we can see that the equations we have defined before are available to edit. We select the _Elasticity_ option, as we gave that _Name_ to the linear elasticity equation during definition. This will reopen the window where we define all equation and solver parameters for the linear elasticity equation. Now that we applied an equation to the air, the one body that is left available is the fork, and it will appear as the only clickable tickbox in the window, which we proceed to select, as shown below (note that we do not need to navigate back to the _Linear elasticity_ tab). Click _Ok_ to finish the configuration.

{{< figure src="/website/posts_res/27-tuning-fork-part-3/eq-app.png" title="Figure 30" caption="Applying Linear Elasticity Equation to the Fork." class="mw8" >}}

Follow the same exact procedure with _Model_ > _Material_ > _Aluminium 6061_ and _Model_ > _Body force_ > _Excitation_ to apply them to the correct body.

Now we are left with boundary conditions. Select _Model_ > _Set boundary properties_. Now we can double click any boundary and assign a boundary condition to it. The selected boundary will become red. For example, in the figure below the _Support_ boundary condition is being assigned to the desired boundary. Click _Update_ to finish the configuration.

{{< figure src="/website/posts_res/27-tuning-fork-part-3/support-b.png" title="Figure 31" caption="Applying the Support Boundary Condition." class="mw10" >}}

You can repeat this for every boundary, but it gets quite tedious. I suggest you simply apply the _Outlet_ condition to the outer spherical shell, then select _Model_ > _Boundary condition_ > _Coupling_ and select all the remaining boundaries from there, as shown below. Click _Ok_ to finish the configuration.

{{< figure src="/website/posts_res/27-tuning-fork-part-3/coupling-b.png" title="Figure 32" caption="Applying the Coupling Boundary Condition." class="mw8" >}}

### Save and Run

Select _File_ > _Save project as_ and then select a directory. Your project files will be saved in the selected directory. To load the project back, you will need to select _File_ > _Load project_ and select the same directory.

The best way to run the project is by clicking the _Generate, save and run_ button at the right of the toolbar, shown below. Note that some of the operations we performed above are also available in the toolbar.

{{< figure src="/website/posts_res/27-tuning-fork-part-3/run.png" title="Figure 33" caption="_Generate, save and run_ Button." class="mw10" >}}

# Conclusion

In this episode we setup our first coupled problem. A number of assumptions and simplifications have been made:

* The vibroacoustic coupling between the fork and air can be considered a weak coupling.
* The specific acoustic impedance of the outer spherical shell, as seen from the wave emitted by the fork, can be considered to be that of a plane wave.
* All the materials and equations are linear.

The fork has been excited by an harmonic volume force of defined direction and magnitude, applied to each (continuous) element of the fork volume.

In the next episode we will review the results of this simulation.

{{< cc >}}

