---
title: "Rigid Walled Room Revisited - Part 1"
date: 2020-09-05T10:24:41+01:00
draft: false
featured_image: "/posts_res/15-rigid-walled-room-revisited-part-1/featured.png"
description: "Computational Eigenmodes of a Rectangular Rigid Walled Room - Simulation Setup"
categories: ["Modelling"]
tags: ["Physics", "Elmer", "FEM", "Linear System", "Single Physics", "Steady State", "Eigensystem", "Series: Rigid Walled Room"]
---

We covered the rigid walled rectangular room previously in the [Rigid Walled Room]({{< ref "/posts/10-rigid-walled-room.md" >}}) episode. In that episode we solved for the steady state field in a rectangular rigid walled room as driven by a source placed somewhere in the room. This allowed us to see how the steady state field is sustained by a _modal superposition_, the purer the lower the driving frequency (assuming that this driving frequency matched an eigenfrequency of the room). The study was setup the way it was in order to work around one limitation of the Elmer Helmholtz solver: the lack of an Eigensolver feature. Had an Helmholtz Eigensolver been available we would have been able to compare with the analytical solution presented in the [Acoustic Modes of a Rectangular Room]({{< ref "/posts/5-acoustic-modes-of-a-rectangular-room.md" >}}) episode directly, in a much more straightforward and meaningful way. It turns out that Elmer does have the capability of solving for the normal modes of cavities. This capability is implemented in the Wave Equation solver. We will explore this solver in this and further episodes, and use this solver to build a new benchmark system for the Home Studio episodes, as we mentioned in the conclusion of the [Mesh Order and Accuracy]({{< ref "/posts/14-mesh-order-and-accuracy.md" >}}) episode.

# Shouldn't this Episode Been Published Before?

We studied the [Acoustic Modes of a Rectangular Room]({{< ref "/posts/5-acoustic-modes-of-a-rectangular-room.md" >}}) analytically so to have a benchmark analytical solution to use as a guide to setup FEM studies, as suggest by the workflow outlined in [The FEM Pipeline]({{< ref "/posts/4-the-fem-pipeline.md" >}}) episode. So, it would have made more sense to start off with this episode, as it is exactly the numerical counterpart to the analytical study we did. So why we started off with the steady state source driven field instead? There are few good reasons.

1. I wasn't completely aware that Elmer offered a solver able to compute the Eigenvalues and Eigenfunctions of an acoustic system until recently (OK, maybe this is not a good reason...).
2. The solver that we will use in this episode, the Wave Equation Solver, doesn't have a GUI module. Also, there are few additional rough edges that make this study a little more advanced, so it makes sense to present it later in the series.

So, without further ado, let's dig in!

# Project Files

All the files used for this project are available at the repositories below:

* [Rigid Walled Room 3](https://gitlab.com/computational-acoustics/rigid-walled-room-3/-/tree/master/).

# The Equation

As we know, the first step is to get comfy with what the solver is doing. So let's pull out the [Elmer Models Manual](http://www.nic.funet.fi/index/elmer/doc/ElmerModelsManual.pdf#chapter.14) and have a good read.

The Wave Equation solver is designed to solve this PDE:

$$-\frac{1}{c} \frac{\partial^{2}p}{\partial t^{2}} + \nabla p + \eta \frac{1}{c^{2}} \nabla \left( \frac{\partial p}{\partial t} \right) - \alpha \frac{1}{c^{2}} \frac{\partial p}{\partial t} = f$$

With $p$ the pressure disturbance field, $t$ the time, $c$ is the speed of sound (a material parameter), $f$ a source function and $\eta$ and $\alpha$ material parameters. $p$ and $f$ are functions of the spatial coordinates and time defined over our _domain_ (and time interval of interest) to which we are able to apply boundary conditions. If we solve for transients, we can also apply _initial_ conditions. Note that the equation is linear, so we will not need nonlinear iterations for this study (see the [Elmer Model and Solver Parameters]({{< ref "/posts/9-elmer-model-and-solver-parameters.md" >}}) for a more in depth discussion).

This equation might look really very different from the Helmholtz equation, but this is a good example of [fumo negli occhi](http://www-dimat.unipv.it/gilardi/). Let's take a deeper look.

The parameters $\eta$ and $\alpha$ model two different types of acoustic damping sustained by the medium itself. Since our analytical solution (to which we aim to compare) doesn't include any damping we quickly set them to $0$ for this study. Similarly, our analytical study does not make use of any source. The normal modes (eigenfunctions) do not need it, as they are a consequence of the geometry and boundary conditions only. So, we set also $f$ to be $0$ everywhere in space and time.

We are then left with this equation:

$$-\frac{1}{c} \frac{\partial^{2}p}{\partial t^{2}} + \nabla p = 0$$

just by moving the first term to the second member:

$$\nabla p = \frac{1}{c} \frac{\partial^{2}p}{\partial t^{2}}$$

which you probably recognise as the wave equation introduced in the [What is Acoustic Modelling]({{< ref "/posts/2-what-is-acoustic-modelling.md" >}}) episode. It turns out that the Elmer Wave Equation solver can solve for a more general version of it.

Now, as you know from the [Acoustic Modes of a Rectangular Room]({{< ref "/posts/5-acoustic-modes-of-a-rectangular-room.md" >}}) episode, this equation is the starting point for searching for eigenvalues (and eigenfunctions) of the system. In fact, the Helmholtz equation just stems from this equation. Solving the eigenproblem associated with the wave equation is, pretty literally, solving the Helmholtz equation in the exact same fashion as in the [Acoustic Modes of a Rectangular Room]({{< ref "/posts/5-acoustic-modes-of-a-rectangular-room.md" >}}) episode.

If you keep on reading the manual you will see that the possibility to solver for the eigenproblem is not mentioned, while it is for the [Linear Elasticity solver](http://www.nic.funet.fi/index/elmer/doc/ElmerModelsManual.pdf#chapter.14). Still, the Wave Equation solver allows to do so pretty much in the very same fashion as for the Linear Elasticity solver. It turns out that this is a more general feature of Elmer supported by many other solvers. For more information, see the [Solver Manual](http://www.nic.funet.fi/pub/sci/physics/elmer/doc/ElmerSolverManual.pdf#chapter.7) and [this thread](http://www.elmerfem.org/forum/viewtopic.php?f=3&t=7153) I opened on the Elmer forums (the assistance I received in there was instrumental to get the study running). Quoting Peter Råback (one of the main Elmer developers) from the thread:

> Now the Models Manual does not mention this possibility because it is a library feature mainly. As long as we take care that the time derivative multiplier matrix is registered even though the case is not transient, any time-dependent equation may become eigenvalue or harmonic system in this way. I would guess the "Eigen Analysis" will be found on the SolverManual side.

[Link](http://www.Elmer.org/forum/viewtopic.php?f=3&t=7153#p23177).

As a final note, the eigenvalues that the Elmer eigen solver calculates, which we can denote with $\lambda$, are squared angular frequencies:

$$\lambda\_{n} = \omega\_{n}^{2} = \left(2 \pi \nu\_{n} \right)^{2}$$

Where the index $n$ denotes the eigenvalue (from $1$ to $10$) and $\nu$ is the eigenfrequency (or resonance frequency) associated with the given eigenvalue. So, the eignfrequencies are found like this:

$$\nu\_{n}=\frac{\sqrt[+]{\lambda\_{n}}}{2 \pi}$$

# Study Parameters 

It is useful to collect the study parameters here for reference. They are the same as for the [Acoustic Modes of a Rectangular Room]({{< ref "/posts/5-acoustic-modes-of-a-rectangular-room.md" >}}) and the [Rigid Walled Room]({{< ref "/posts/10-rigid-walled-room.md" >}}) episodes.

## Room Geometry

{{< load-table-dfl-style >}}

| Name   | Symbol | Value          |
|--------|--------|----------------|
| Length | $L_x$  | $5$ $\text{m}$ |
| Width  | $L_y$  | $4$ $\text{m}$ |
| Height | $L_z$  | $3$ $\text{m}$ |

## Medium Properties

|Parameter Name                 | Symbol   | Value                            |
|-------------------------------|----------|----------------------------------|
| Medium Sound Phase Speed      | $c$      | $343$ $\frac{\text{m}}{\text{s}}$ |
| Medium Sound Damping          | $\eta$   | $0$ $\frac{\text{m}^2}{\text{s}}$ |
| Medium Sound Reaction Damping | $\alpha$ | $0$ $\text{Hz}$                  |

# Patching Elmer

If you read the already mentioned [forum post](http://www.elmerfem.org/forum/viewtopic.php?f=3&t=7153) you will see that a bug in Elmer prevented the Wave Equation solver to work properly. Depending on which Elmer version you have on your system you might need to "patch" Elmer to have this working. Turns out that, thanks to the Fortran compiler wrapper provided by Elmer itself, this is quite easy. Follow the procedure below only if you get errors as those reported in the forum thread.

First of all, install `gfortran`.

```bash 
sudo apt install gfortran
```
Then, clone the Elmer git repository `devel` branch:

```bash
git clone https://github.com/ElmerCSC/elmerfem.git
cd elmerfem/
git checkout devel
```

Then, go where the modules are:

```bash
cd fem/src/modules/
```

and compile the wave solver:

```bash
elmerf90 -o WaveSolver.so WaveSolver.F90 
```

Now we only need to put the new `WaveSolver.so` file in our Elmer system directory:

```bash
cp /usr/share/elmersolver/lib/WaveSolver.so ~/WaveSolver.so
cp WaveSolver.so /usr/share/elmersolver/lib/WaveSolver.so
```
The first `cp` command above just makes a backup copy of the original `WaveSolver.so` file in your home, just in case. The second `cp` command puts the new one in place.

# The Model

We will now go through the steps to build our Elmer study to search for the eigenfrequencies and eigenfunctions of a rectangular room. As we mentioned in the introduction, there isn't a GUI module for the Wave Equation solver, so we will create our _sif_ file manually. This is actually a more flexible and perhaps overall better way to handle simulations with Elmer, as the GUI has only limited functionality while the preprocessors (such as Salome) give us all the GUI support we need to setup the study.

## Geometry

The geometry step is very simple. We just create a rectangle with FreeCAD having sizes $4$ $\text{m}$, $5$ $\text{m}$ and $3$ $\text{m}$ respectively. You can follow the [Rigid Walled Room]({{< ref "/posts/10-rigid-walled-room.md" >}}) episode for guidance. After we are done, we just export the geometry as _BREP_, as usual.

By the way, you might find more advantageous for big objects like a room to configure FreeCAD to use the _MKS_ system. To do so:

* Select _Edit_ from the top menu and then _Preferences_.
* In the newly opened _Preferences_ windows, go to the _Units_ tab.
* From the _User system:_ combo select _MKS (m/kg/s/degree)_.
* Click _Apply_.
* Click _Ok_.

{{< figure src="/website/posts_res/15-rigid-walled-room-revisited-part-1/freecad-mks.png" title="Figure 1" caption="_MKS_ Settings for FreeCAD." class="mw7" >}}

## Meshing

Meshing largely works the usual way, you can refer to the previous episodes for guidance on how to import _BREP_ files into Salome and mesh them. For this study, we will use the following mesh parameters:

| Mesh Algorithm  | Mesh Min. Size   | Mesh Max. Size    | Element Order | Fineness |
|-----------------|------------------|-------------------|---------------|----------|
| NETGEN 1D-2D-3D | $10$ $\text{mm}$ | $170$ $\text{mm}$ | First         | Fine     |

{{< figure src="/website/posts_res/15-rigid-walled-room-revisited-part-1/mesh-parameters.png" title="Figure 2" caption="Mesh Parameters (Salome)." class="mw6" >}}

The maximum mesh size was chosen in order to allow good accuracy up to $200$ $\text{Hz}$ based on the tenth of wavelength rule, assuming a speed of sound $c$ of $343$ $\frac{\text{m}}{\text{s}}$. The value of $200$ $\text{Hz}$ in turn is chosen as we will search for the first $10$ eigenfrequencies. From the [Acoustic Modes of a Rectangular Room]({{< ref "/posts/5-acoustic-modes-of-a-rectangular-room.md" >}}) episode we know that the tenth eigenfrequency (the highest) is expected to be $\sim86$ $\text{Hz}$. Hence choosing $200$ $\text{Hz}$ as a target frequency for the mesh size should allow a good "safety margin".

Normally we would export the mesh as _UNV_ but since we will make the study manually, without GUI, it is more advantageous to use a third party script to export the mesh from Salome directly in Elmer format. To do so we will use the [salomeToElmer](https://github.com/jvencels/salomeToElmer) script. This script was successfully used for this episode running on Ubuntu 18.04 and Salome 9.5.0.

To use the script, simply clone the repo (note that the `salomeToElmer` repo is included as a submodule of the [project repository](https://gitlab.com/computational-acoustics/rigid-walled-room-3)):

```bash
git clone https://github.com/jvencels/salomeToElmer.git
```

Then, select the mesh to be exported from the Salome _Object Browser_ as shown below:

{{< figure src="/website/posts_res/15-rigid-walled-room-revisited-part-1/mesh-selection.png" title="Figure 3" caption="Mesh Selection (Salome)." class="mw5" >}}

You can see in the figure above that the various groups have been renamed. You can do so simply by right clicking the groups and choosing _Rename_ from the menu. This is not necessary, but can help you with assigning boundary conditions, as we will see later.

Then, click _File_ from the top menu and choose _Load Script..._. This will open a file selection dialogue. Just navigate to the cloned `salomeToElmer` directory and choose the `salomeToElmer.py` file. Then click _Open_. You should see something like this in the _Python Console_ window at the bottom of the Salome GUI:

```text
>>> exec(open("/home/administrator/salomeToElmer/salomeToElmer.py", "rb").read())
Exporting mesh to /home/administrator/SALOME-9.5.0-UB18.04-SRC/Mesh_1

Exporting 3D mesh..

Done exporting!

Total time: 2 s

>>> 
```

Note that the message above shows the location where the mesh is exported. Inside the said location you will find these files:

* `mesh.boundary`
* `mesh.elements` 
* `mesh.header` 
* `mesh.names`
* `mesh.nodes`

We will use these to build the study in the next sections.

## Elmer Study

To setup an Elmer study manually we proceed as follows.

### Create a Folder to Hold the Study Files

This step is very simple, just create a folder for your Elmer study. In [my project](https://gitlab.com/computational-acoustics/rigid-walled-room-3) I chosen the name `elmerfem` for the folder. I will then refer to this folder as the `elmerfem` folder for the rest of the article. Of course, you can use any name, but you will have to adjust the rest accordingly.

### Copy the Mesh Files in the Folder

Just copy all the files exported by `salomeToElmer` into the project folder we just created. After this step the content of the `elmerfem` directory looks like this:

* `mesh.boundary`
* `mesh.elements` 
* `mesh.header` 
* `mesh.names`
* `mesh.nodes`

### Create the Solver Input File

The solver input file contains all the settings and definitions for the simulation. We create it section after section the very same way we do in the GUI, but textually. So, create an empty file named `case.sif` (you can actually name it as you want but if you choose another name you will have to adapt some parts further down the line).

Let's now open the file with a text editor and write all the sections. We will not go into the detail of all the Solver Input Files options, but only what is important for this study. What we don't mention you can treat as a "safe default". For more information about the Solver Input File you can refer to online documentation such as [this presentation](http://www.nic.funet.fi/index/elmer/slides/ElmerCourseWarsaw2014October/ElmerSolverInput_2014Oct.pdf).

The whole Solver Input File can be viewed [here](https://gitlab.com/computational-acoustics/rigid-walled-room-3/-/blob/master/elmerfem/case.sif).

#### Header

We use this section to tell Elmer that the mesh files are in the current directory, and we want results in the current directory as well. This is how this section will look:

```text 
Header
  CHECK KEYWORDS Warn
  Mesh DB "." "."
  Include Path ""
  Results Directory ""
End
```

#### Simulation

This section contains the main parameters of the simulation.

```text 
Simulation
  Max Output Level = 5
  Coordinate System = Cartesian
  Coordinate Mapping(3) = 1 2 3
  Simulation Type = Steady state
  Steady State Max Iterations = 10
  Output Intervals = 1
  Timestepping Method = BDF
  BDF Order = 1
  Coordinate Scaling = 0.001
  Solver Input File = case.sif
  Post File = case.vtu
End
```

Our simulation is a Steady State simulation, as that is the nature of the eigenmodes. Note that we defined, as usual, the coordinate scaling. We also indicate the name of the Solver Input File (`case.sif` in this article) and we define the output file for the simulation (Post File) as `case.vtu`. You can edit this output file name as you wish, but keep the `vtu` file format to ensure you can load it into ParaView. Note that if you change the output file name then the script `validate.jl` included in the project repository will not work anymore.

#### Constants

This is used to define physical constants.

```text 
Constants
  Gravity(4) = 0 -1 0 9.82
  Stefan Boltzmann = 5.67e-08
  Permittivity of Vacuum = 8.8542e-12
  Boltzmann Constant = 1.3807e-23
  Unit Charge = 1.602e-19
End
```

#### Solver 1 

We define the Wave Equation solver parameters here. Note that the `!` denotes comments.

```text 
Solver 1

  ! Equation
  Equation = "Wave Equation"
  Procedure = "WaveSolver" "WaveSolver"
  Variable = "Pressure"
  Variable DOFs = 1
  
  ! Solver
  
  ! Steady State
  Steady State Convergence Tolerance = 1e-09
  
  ! Nonlinear
  ! Nonlinear System Convergence Tolerance = 1.0e-7
  ! Nonlinear System Max Iterations = 1
  ! Nonlinear System Newton After Iterations = 3
  ! Nonlinear System Newton After Tolerance = 1.0e-3
  ! Nonlinear System Relaxation Factor = 1
  
  ! Linear
  ! Linear System Scaling = Logical True
  ! Linear System Symmetric = Logical True
  Linear System Solver = string "Iterative"
  Linear System Direct Method = Umfpack
  Linear System Iterative Method = BiCGStabl
  Linear System Max Iterations = integer 10000
  Linear System Convergence Tolerance = real 1e-8
  BiCGstabl polynomial degree = 4
  Linear System Preconditioning = ILU2
  Linear System ILUT Tolerance = 1.0e-3
  Linear System Abort Not Converged = True
  Linear System Residual Output = 50
  Linear System Precondition Recompute = 1
  
  ! Special
  Eigen Analysis = True
  Eigen System Values = 10
  Eigen System Select = smallest magnitude
  Eigen System Convergence Tolerance = Real 1.0e-9
  Eigen System Max Iterations = 10000
  ! Eigen System Shift = Real 1000
  Eigen System Normalize To Unity = Logical True
  
End
```

To ease reading, the keywords in this section were separated.

Note how there is a number next to `Solver`. That number is the solver ID. Multiple solvers can be defined and we identify them through the ID.

##### Equation Keywords

These keywords define the main attributes of the equation and solver subroutine. Note that the keywords `Procedure` and `Variable DOFs` can only assume the values reported above in order for the Wave Equation solver to work. You can assign any value to the `Equation` keyword, and this is true also for the `Variable` keyword, which defines the name we want to use to call $p$, essentially. However, note that if you change the `Variable` keyword value this will need to be reflected in the boundary conditions or any other Solver Input File section that references the solver variable. By default we would have:

```text 
Variable = "Excess Pressure"
```

since $p$ is a pressure disturbance. However, I decided to rename it to `Pressure` for brevity.

##### Solver Keywords

These are our usual convergence, Linear and Nonlinear system parameters. Note how the Nonlinear System keywords are all commented out, due to the equation being linear. An interactive Linear system technique is used. Normally these parameter might need a little trial and error to tune. For more information, you can refer to the [Elmer Model and Solver Parameters]({{< ref "/posts/9-elmer-model-and-solver-parameters.md" >}}) and [Dealing with Convergence Issues]({{< ref "/posts/13-dealing-with-convergence-issues.md" >}}) episodes.

##### Special Keywords

These keywords are used to define all the details of our Eigen Analysis. We first activate the Eigen Analysis, and then instruct Elmer to search for the first $10$ eigenmodes. The following keyword tells Elmer to search for the eigenvalues in order of magnitude, from lowest to bigger. This means we will find $10$ sorted eigenvalues. We then assign a convergence tolerance and a max number of iterations to the Eigen System solver. The `Eigen System Shift` is commented out. You can make use of it to search for eigenvalues starting from a lower threshold of your choosing rather than $0$. Finally, `Eigen System Normalize To Unity` is used to normalise the solution to $1$ peak value, so that we can readily compare with the analytical solution $S$ from the [Acoustic Modes of a Rectangular Room]({{< ref "/posts/5-acoustic-modes-of-a-rectangular-room.md" >}}) episode.

For more information about these keywords, refer to the [Elmer Models Manual](http://www.nic.funet.fi/index/elmer/doc/ElmerModelsManual.pdf#chapter.14) and the [Elmer Solver Manual](http://www.nic.funet.fi/pub/sci/physics/elmer/doc/ElmerSolverManual.pdf#chapter.7).

#### Solver 2

Elmer has also special solvers that are not real solvers, but more of utilities. We define here a second solver, with ID 2, to save the eigenvalues found by Elmer to a file. This comes in handy as it will prevent us to have to parse or otherwise use the solver logs to get the eigenvalues.

```text 
Solver 2
  Equation = "SaveScalars"
  Filename = eigenvalues.dat

  Procedure = "SaveData" "SaveScalars"
  Save EigenValues = True

  ! Compare the norm of the 10th eigenvalue
  Show Norm Index = 10 
  
End
```

You can change the filename to anything else, but note that if you do the script `validate.jl` included in the repo will not work anymore.

#### Equation 1

We now put together an equation, with ID 1, to describe our domain. This is easy.

```text 
Equation 1
  Name = "Wave Equation"
  Active Solvers(1) = 1
End
```

We assign `Wave Equation` to it as a name, and the active solvers for this equation are just one, the `Solver 1`. The `Active Solvers` keyword is used to specify a list, so between the parenthesis we put the list length (which is 1) and after the equal sign all the IDs of the solvers we want to use, in this case 1 only (the `Solver 2` will just run automatically).

#### Material 1

We now define the material parameters. We can define many materials, so this property also has an ID number. The Wave Equation solver only need three material parameters:

```text 
Material 1
  Name = "Air (room temperature)"
  Sound Speed = Real 343.0
  Sound Damping = Real 0.0
  Sound Reaction Damping = Real 0.0
End
```

We can put any value for `Name`. Here I chosen a descriptive one. As we stated above, the only nonzero material parameter in this study is $c$ and we set it to $343$ $\frac{\text{n}}{\text{s}}$ (the value for air at room temperature).

#### Body 1

Now we have all the pieces needed to describe a body, which are equations and materials (and, when applicable, initial conditions). So, let's define our body.

```text 
Body 1
  Target Bodies(1) = 1
  Name = "Body 1"
  Equation = 1
  Material = 1
End
```

Here is where the file `mesh.names` comes in handy. Let's open it and take a look:

```text 
! ----- names for bodies -----
$ Volume = 1
! ----- names for boundaries -----
$ Wall_1 = 2
$ Wall_2 = 3
$ Floor = 4
$ Ceiling = 5
$ Wall_3 = 6
$ Wall_4 = 7
$ empty = 8
```

This file reports on the left the names we assigned to our mesh groups with Salome, and to the right the ID numbers we can use to access them with Elmer. Here we have only one body (volume group) with ID 1, so we assign it to the `Target Bodies` list (remember that the length of the list is defined by the number in parenthesis).

We are free to choose any name for the `Name` keyword. Finally, we assign the IDs of the Equation and Material that describe the body, which we defined in the previous Solver Input File sections.

#### Boundary Condition 1

Finally, we define the boundary condition. Of course, as we can have many for many different boundaries, also this property has an ID. As we know from the [Acoustic Modes of a Rectangular Room]({{< ref "/posts/5-acoustic-modes-of-a-rectangular-room.md" >}}) episode the correct boundary condition is $0$ velocity at the boundaries. So we do just that:

```text 
Boundary Condition 1
  Target Boundaries(6) = 2 3 4 5 6 7
  Pressure Velocity = Real 0
End
```
Note that if you assigned a different value to the solver variable in the Solver 1 section then you would need to change the `Pressure Velocity` keyword accordingly. As you can see all the boundaries to which the condition is applied are simply listed. If you need to apply different conditions to different boundaries the file `mesh.names` will come in handy.

### Create the ELMERSOLVER_STARTINFO File 

As the tile say, create a file named `ELMERSOLVER_STARTINFO` in the `elmerfem` directory, open it with a text editor and put this lines inside:

```text 
case.sif
1
```

This file will make the `ElmerSolver` command to start the solver with our Solver Input File.

# Running the Study 

Now everything is ready, so we can run the study. Open the terminal and navigate to the `elmerfem` directory. The contents will be:

* `ELMERSOLVER_STARTINFO`
* `case.sif`
* `mesh.boundary`
* `mesh.elements` 
* `mesh.header` 
* `mesh.names`
* `mesh.nodes`

You can now issue the `ElmerSolver` command:

```bash
ElmerSolver
```

This will start the solver. The terminal should report logs similar to these:

```text 
ELMER SOLVER (v 8.4) STARTED AT: 2020/09/05 17:10:33
ParCommInit:  Initialize #PEs:            1
MAIN: 
MAIN: =============================================================
MAIN: ElmerSolver finite element software, Welcome!
MAIN: This program is free software licensed under (L)GPL
MAIN: Copyright 1st April 1995 - , CSC - IT Center for Science Ltd.
MAIN: Webpage http://www.csc.fi/elmer, Email elmeradm@csc.fi
MAIN: Version: 8.4 (Rev: unknown, Compiled: 2020-08-29)
MAIN:  Running one task without MPI parallelization.
MAIN:  Running with just one thread per task.
MAIN:  HYPRE library linked in.
MAIN:  MUMPS library linked in.
MAIN: =============================================================
LoadInputFile: Reading only "Run Control" section
MAIN: 
MAIN: 
MAIN: -------------------------------------
MAIN: Reading Model: case.sif
LoadInputFile: Scanning input file: case.sif
LoadInputFile: Scanning only size info
LoadInputFile: First time visiting
LoadInputFile: Reading base load of sif file
LoadInputFile: Loading input file: case.sif
LoadInputFile: Reading base load of sif file
Model Input:  Unlisted keyword: [pressure velocity] in section: [boundary condition 1]
LoadInputFile: Number of BCs: 1
LoadInputFile: Number of Body Forces: 0
LoadInputFile: Number of Initial Conditions: 0
LoadInputFile: Number of Materials: 1
LoadInputFile: Number of Equations: 1
LoadInputFile: Number of Solvers: 2
LoadInputFile: Number of Bodies: 1
Loading user function library: [WaveSolver]...[WaveSolver_Init0]
Loading user function library: [SaveData]...[SaveScalars_Init0]
LoadMesh: Base mesh name: ./.
LoadMesh: Scaling coordinates: 1.000E-03 1.000E-03 1.000E-03
LoadMesh: Elapsed REAL time:     0.1050 (s)
MAIN: -------------------------------------
AddVtuOutputSolverHack: Adding ResultOutputSolver to write VTU output in file: case
Loading user function library: [WaveSolver]...[WaveSolver_Init]
Loading user function library: [WaveSolver]...[WaveSolver_bulk]
Loading user function library: [WaveSolver]...[WaveSolver]
OptimizeBandwidth: ---------------------------------------------------------
OptimizeBandwidth: Computing matrix structure for: wave equation...done.
OptimizeBandwidth: Half bandwidth without optimization: 15949
OptimizeBandwidth: 
OptimizeBandwidth: Bandwidth Optimization ...done.
OptimizeBandwidth: Half bandwidth after optimization: 1088
OptimizeBandwidth: ---------------------------------------------------------
Loading user function library: [SaveData]...[SaveScalars_Init]
Loading user function library: [SaveData]...[SaveScalars_bulk]
Loading user function library: [SaveData]...[SaveScalars]
Loading user function library: [ResultOutputSolve]...[ResultOutputSolver_Init]
Loading user function library: [ResultOutputSolve]...[ResultOutputSolver_bulk]
Loading user function library: [ResultOutputSolve]...[ResultOutputSolver]
ElmerSolver: Number of timesteps to be saved: 10
MAIN: 
MAIN: -------------------------------------
MAIN:  Steady state iteration:            1
MAIN: -------------------------------------
MAIN: 
SingleSolver: Attempting to call solver
SingleSolver: Solver Equation string is: wave equation
WaveSolver: Solving the divergence pressure wave
WaveSolver: We have a harmonic or eigenmode system
DefUtils::DefaultDirichletBCs: Setting Dirichlet boundary conditions
DefUtils::DefaultDirichletBCs: Dirichlet boundary conditions set
CRS_IncompleteLU: ILU(2) (Real), Starting Factorization:
CRS_IncompleLU: Recursive round: 1
CRS_IncompleteLU: ILU(2) (Real), NOF nonzeros:   1134084
CRS_IncompleteLU: ILU(2) (Real), filling (%) :       453
CRS_IncompleteLU: ILU(2) (Real), Factorization ready at (s):     0.21
      18 0.1457E-14 0.1112E-08
      20 0.9786E-12 0.4808E-08
       4 0.1573E-11 0.7758E-08
       4 0.9262E-12 0.4590E-08
       4 0.1981E-12 0.9828E-09
       4 0.3207E-13 0.1624E-09
       4 0.7249E-13 0.3639E-09
       4 0.2092E-12 0.1054E-08
       4 0.2606E-12 0.1329E-08
       4 0.8988E-13 0.4529E-09
       4 0.1780E-11 0.9139E-08
       4 0.2217E-12 0.1134E-08
       4 0.1517E-11 0.7763E-08
       4 0.1050E-12 0.5380E-09
       4 0.1890E-11 0.9718E-08
       4 0.2219E-12 0.1131E-08
       4 0.5035E-13 0.2569E-09
       4 0.1578E-11 0.8171E-08
       4 0.6613E-12 0.3364E-08
       4 0.5063E-12 0.2638E-08
       4 0.1255E-11 0.6443E-08
       4 0.3627E-12 0.1874E-08
       4 0.1239E-11 0.6465E-08
       4 0.1235E-11 0.6424E-08
       4 0.5657E-12 0.2922E-08
       4 0.1236E-11 0.6474E-08
       4 0.1107E-11 0.5726E-08
       4 0.1730E-11 0.9006E-08
       4 0.1127E-11 0.5876E-08
       4 0.1452E-11 0.7619E-08
       4 0.6118E-12 0.3191E-08
       4 0.3283E-12 0.1723E-08
       4 0.1336E-11 0.6711E-08
       4 0.1022E-11 0.5207E-08
       4 0.8708E-12 0.5062E-08
       4 0.1454E-11 0.7802E-08
       4 0.5369E-12 0.2772E-08
       4 0.1231E-11 0.6506E-08
       4 0.3854E-12 0.2047E-08
       4 0.1402E-11 0.7240E-08
       4 0.8260E-12 0.4388E-08
       4 0.1569E-11 0.8225E-08
       4 0.1472E-11 0.7581E-08
       4 0.7688E-12 0.4064E-08
       4 0.9191E-12 0.4821E-08
EigenSolve: 
EigenSolve: Eigen system solution complete:
EigenSolve: 
EigenSolve: Convergence criterion is:    1.000E-09
EigenSolve: Number of converged Ritz values is: 10
EigenSolve: Number of update iterations taken: 2
EigenSolve: 
EigenSolve: Computed 10 Eigen Values
EigenSolve: --------------------------------
EigenSolve: 1:   -3.662635E-10   0.000000E+00
EigenSolve: 2:    4.648078E+04   0.000000E+00
EigenSolve: 3:    7.265502E+04   0.000000E+00
EigenSolve: 4:    1.192589E+05   0.000000E+00
EigenSolve: 5:    1.293063E+05   0.000000E+00
EigenSolve: 6:    1.760096E+05   0.000000E+00
EigenSolve: 7:    1.863333E+05   0.000000E+00
EigenSolve: 8:    2.023110E+05   0.000000E+00
EigenSolve: 9:    2.491320E+05   0.000000E+00
EigenSolve: 10:    2.594809E+05   0.000000E+00
EigenSolve: --------------------------------
Loading user function library: [WaveSolver]...[WaveSolver_post]
ComputeChange: SS (ITER=1) (NRM,RELC): (  0.0000000      0.0000000     ) :: wave equation
SingleSolver: Attempting to call solver
SingleSolver: Solver Equation string is: savescalars
SaveScalars: -----------------------------------------
SaveScalars: Saving scalar values of various kinds
SaveScalars: Found 10 Eigenvalues
SaveScalars: Saving names of values to file: ./eigenvalues.dat.names
SaveScalars: Saving values to file: ./eigenvalues.dat
ComputeChange: SS (ITER=1) (NRM,RELC): (  259480.90      0.0000000     ) :: savescalars
Loading user function library: [SaveData]...[SaveScalars_post]
ComputeChange: SS (ITER=1) (NRM,RELC): (  259480.90      2.0000000     ) :: savescalars
SingleSolver: Attempting to call solver
SingleSolver: Solver Equation string is: internalvtuoutputsolver
ResultOutputSolver: -------------------------------------
ResultOutputSolver: Saving with prefix: case
ResultOutputSolver: Creating list for saving - if not present
CreateListForSaving: Field Variables for Saving
ResultOutputSolver: Saving in unstructured VTK XML (.vtu) format
VtuOutputSolver: Saving results in VTK XML format with prefix: case
VtuOutputSolver: Saving number of partitions: 1
ResultOutputSolver: -------------------------------------
Loading user function library: [ResultOutputSolve]...[ResultOutputSolver_post]
ElmerSolver: *** Elmer Solver: ALL DONE ***
ElmerSolver: The end
SOLVER TOTAL TIME(CPU,REAL):        16.92        9.46
ELMER SOLVER FINISHED AT: 2020/09/05 17:10:43
```

# Conclusion 

This episode is pretty long, so it is best to draw the line here for the time being. We learned that there is another solver in Elmer, the Wave Equation solver, that can be used to calculate the normal modes of acoustic systems. This solver provides both the eigenvalues and the eigenfunctions. However, this solver requires a bit of experience to be used as it does not have a GUI module. We seen then how to setup non-GUI studies, also by making use of helpful scripts.

In the next episode we will compare the numerical FEM results from this study to the exact ones, in order to assess the accuracy.

{{< cc >}}

