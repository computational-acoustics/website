---
title: "Tuning Fork - Part 4"
date: 2021-07-31T16:54:32+01:00
draft: false
featured_image: "/posts_res/28-tuning-fork-part-4/featured.png"
description: "Weak Coupling Between Tuning Fork and Air - Results"
categories: ["Modelling"]
tags: ["Physics", "Elmer", "FEM", "Linear System", "Weak Coupling", "Series: Tuning Fork"]
---

In the [Tuning Fork - Part 3]({{< ref "/posts/27-tuning-fork-part-3" >}}) episode we seen how to setup a weak vibroacoustic coupling problem. We applied an harmonic body force to our tuning fork, and used its displacement field to wake the acoustic field within a coupled volume of air. In this episode we will review the results.

<!--more--> 

# Project Files

All the files used for this project are available at the repositories below:

* [Tuning Fork 2](https://gitlab.com/computational-acoustics/tuning-fork-2).

# Solution Overview

The `vtu` file output from Elmer will contain the following fields:

* `pressure wave 1`: the real part of the steady state pressure (scalar) field.
* `pressure wave 2`: the imaginary part of the steady state pressure (scalar) field.
* `displacement HarmonicMode1`: the displacement (vector) field.

The pressure fields will be defined in the coupled air body, while the displacement field is defined within the fork body.

We can use the fields listed above to animate the steady state solution. For the pressure field, we can use the equations in the [Interpreting Helmholtz Solver Solutions]({{< ref "/posts/18-interpreting-helmholtz-solver-solutions#the-helmholtz-solution-field" >}}) episode for the time dependent physical field. For the displacement field we will simply multiply it with a cosine term, as done in the [Tuning Fork - Part 2]({{< ref "/posts/26-tuning-fork-part-2" >}}).

Many more things might be said on how to produce animations of the resulting fields. This will form the core of subsequent episodes. For the time being, you are referred to [the code](https://gitlab.com/computational-acoustics/tuning-fork-2/-/blob/6960b8c0a9e20ca999c38e46a591350de055592f/make_animation.jl) generating the animation frames.

An animation of the resulting fields is shown below, for multiple views. The animation shows the displacement of the fork both with a colormap and scaled (by a factor of $10$, to ease visualisation) fork deformation. The pressure field is instead visualised as surfaces of constant pressure, each colored in relation to its pressure value. The colormap is shown in the bottom right view.

{{< youtube KWdw9B1dnEU >}}

The solution appears to make physical sense. We can see that the displacement field has the same shape of that of the principal mode observed in the [Tuning Fork - Part 2]({{< ref "/posts/26-tuning-fork-part-2" >}}) episode for the simply supported handle bottom case, the same boundary condition applied to this study. This is expected from the excitation frequency matching that of the principal mode for said boundary condition ($528.91$ $\text{Hz}$).

The maximum value of pressure projected by the fork is quite high, $26.34$ $\text{Pa}$, associated to a [Sound Pressure Level (SPL)](https://en.wikipedia.org/wiki/Sound_pressure#Sound_pressure_level) of $122.4$ $\text{dB} \space \text{re} \space 20\mu \text{Pa}$. Remember how we applied quite a large [body force]({{< ref "/posts/27-tuning-fork-part-3#body-force" >}}) to the fork, an amount of $10^{6}$ $\frac{\text{N}}{\text{m}^3}$. Since all equations are linear the SPL reached for a certain body force magnitude is not extremely interesting: for any other body force magnitude we simply expect the resulting fields to simply scale linearly. Perhaps, the most interesting takeaway from this is that the tuning fork is a very inefficient radiator, as one can expect from having had experience with one. In fact, typically the most reliable way to hear the tone is through bone conduction, by biting the handle or placing on the skull near the ear.

On the other hand, the shape of the pressure field is more interesting. We can note that high pressure is only found in proximity of the prongs, and degrades fast going away from the fork. Also, as especially evident from the top view, there are directions along which little to no pressure is radiated. This is expected behaviour, as the nature of the prongs motion forces an alternation of positive and negative pressure zones around the prongs. Or, in other words, the pressure wakes at the prongs have phase shifts of $180^{\circ}$ with respect each other, causing the waves they radiate to mostly cancel out. This is a known fact for tuning forks, the main cause behind their inefficiency, and a fact our model appears to correctly capture. It is to be noted, though, that these considerations are qualitative and not quantitative (for the time being).

For clarity, the pressure field animation is also presented on its own, without the fork, on a single view below. In this larger animation is perhaps clearer how the pressure between the prongs is always "sandwiched" by pressures of opposites sign at the other sides of the prongs, producing the cancellation we talked about.

{{< youtube z-hvPnBR5HM >}}

# Additional Acoustic Fields Details

The animations above are quite fun to look at, but perhaps some plots can help us understanding the radiated field a little bit better.

{{< figure src="/website/posts_res/28-tuning-fork-part-4/level-iso.png" title="Figure 1" caption="Isosurfaces of Pressure Level (Relative to Peak Emitted Pressure)" class="mw10" >}}

The figure above shows us, in four different views, various different surfaces of constant pressure level relative to the peak pressure emitted by the fork. We can see where the radiated pressure is maximal, in "bubbles" around the prongs, and how fast the pressure decays. We can also see very minimal radiation coming from the handle, as expected from the handle being only very marginally involved in the principal mode. However, we can also see that some of the angles of the surfaces appear sharp and rough. This is telling us that perhaps it would have been wise to use a smaller mesh size.

We can also visualise the pressure level relative to the peak pressure in slices of the domain, or along particular directions. This is done in the plot below.

{{< figure src="/website/posts_res/28-tuning-fork-part-4/level-slices.png" title="Figure 1" caption="Slices of Pressure Level (Relative to Peak Emitted Pressure)" class="mw10" >}}

The figure above shows slices of the domain, note that the horizontal slice in the bottom left plot is taken at half the height of the prongs. These slices tell us even clearer (although only on selected planes) where the fork is unable to radiate pressure: in the dark areas there is very low pressure with respect the peak, up to $-85$ $\text{dB}$, a very remarkable attenuation.

Finally, the curves in the bottom right plot report the pressure level as a function of distance along lines parallel to the Cartesian axes (red for $+x$, yellow for $+y$ and green for $+z$). The lines all start from the point in between the prongs, at half the prong height, and reach, along the desired direction, the end of the domain. Note that the $y$ line (red) hence passes through one of the prongs. In all cases, the level rises as we get closer to the boundary of the prong, but quickly falls as soon as we walk away from said boundary. However, the plots also show 3 different behaviours towards the limit of the domain. In the far field (many wavelengths away from the source) we expect the pressure to decay proportionally to $\frac{1}{r}$, hence all curves should match. Clearly, our domain is not big enough to show the far field, as the diameter is a mere $1.3$ times bigger than the wavelength, but we are also left wondering whether the boundary condition we applied, a simple plane wave specific acoustic impedance, really ensure no reflections are happening: reflections from the boundary can also produce a behaviour different from the $\frac{1}{r}$ attenuation.

# Conclusion

In this episode we took a quick look at the solution of the weak viboracoustic coupling between a tuning fork and the surrounding air. We found a solution that, from a qualitative standpoint, makes a lot of sense and conforms to expectations. The solution also allows us to predict important source properties, such as the directions of maximal and minimal radiation, or the radiated field properties as a function of distance (note that at such a short distance from the source we cannot quite yet talk about _directivity_, as that is more of a far field concept).

However, we have not attempted quantitative validation of the solution, which we might cover in subsequent episodes. Also, we have noticed few possible improvements area for our study, mainly:

* Mesh size: a smaller size might be beneficial to capture the details of the acoustic field.
* Acoustic boundary conditions: we might need to explore better ways to ensure a no reflections boundary condition and the domain outer boundary.

We also produced animations and other post-processing without explaining how in details. In the following episodes we will take a break from studies and show how to produce animations with ParaView and Julia.

{{< cc >}}

