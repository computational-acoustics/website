---
title: "Ear Canal - Part 2"
date: 2025-02-22T13:57:27Z
draft: true
featured_image: "/posts_res/39-ear-canal-part-2/featured.png"
description: "An Elmer Model for the Ear Canal"
categories: ["Modelling"]
tags: ["Physics", "Elmer", "FEM", "Linear System", "Single Physics", "Ear Canal"]
---

In the [Ear Canal - Part 1]({{< ref "/posts/38-ear-canal-part-1" >}}) episode we built a first ear canal model. We used a freely available cast of a hear canal and manipulated its geometry. We obtained some initial results, but it was clear that the accuracy left much to be desired. In this episode we step back: we solve an eigenproblem on a better geometry and compare the results with a benchmark problem.

<!--more-->

# The Problems of the Previous Model

The previous model had multiple issues, including geometry, governing equation and post-processing techniques. From a geometry point of view, it used an ear canal cast from a particular person. While this is not wrong, the specific of this person anatomy might make comparison with data from literature harder, as that typically involves averages over multiple people. This episode focus on a better geometry.

# A Better Geometry

It is not straightforward to say what a better geometry is. Ear canals, like most anatomical parts, are subject to variation. A good geometry definition would be a reasonable average of the geometry over a large number of diverse humans. However, how to measure the ear canal anatomy, and then reasonably average the results, is a complex problem in on its own.

Thankfully we do not have to do that step. The [ITU-T P.57](https://www.itu.int/rec/T-REC-P.57-202106-I/en) recommendations contain specifications for various types of ear simulators, including one that is anatomically shaped: the Type 4.3. It is the specifics of this simulator that we will use to define our geometry.

Of course, the ITU methodology (which you can read in the standard) is not immune to critique. Brian Johansen, Per Rasmussen, and Morten H. Pedersen have made a veiled one in an [article they released for audioxpress](https://audioxpress.com/article/unveiling-the-impact-of-ear-canal-geometry-for-in-ear-headphone-testing):

> In addition to the attempt to create a “one size fits all” ear simulator that, in reality, fits no one but simply represents an “average ear” geometry, there is a rarely explored dimension at play.

However, using an ITU standardised geometry has the following pro: our results can be compared with other people doing similar work. For example:

* [The Digital Twin of a New and Standardized Fullband Ear Simulator](https://pub.dega-akustik.de/DAGA_2022/data/articles/000465.pdf) by Lars Birger Nielsen1 and  Mads Herring Jensen
* [Type 4.3 Ear Simulator](https://www.comsol.com/model/type-43-ear-simulator-106741) from the COMSOL Application Gallery.

It is therefore a better starting point.

## Implementing ITU-T P.57

### Difficulties

The implementation of ITU-T P.57 was much harder than I anticipated. If you read ITU-T P.57 (06/2021) you will see that Table 6 specifies an ear canal centre line. Along this centre line various cross sections of the ear canal are presented in planes orthogonal to the center line. The unit vectors defining these planes are presented in Table 7, while the cross sections shapes are presented in Figure 12.

At first sight is all seems simple: sample the cross sections outlines and put the points in space. Fit some close loop curve through them and then loft through them.

This operation can be in fact done with [Gmsh](https://gmsh.info/) by:

* Defining [`Points`](https://gmsh.info/doc/texinfo/gmsh.html#Points) along the outline of each cross section.
* Define a closed [`BSpline`](https://gmsh.info/doc/texinfo/gmsh.html#index-BSpline-_0028-expression-_0029-_003d-_007b-expression_002dlist-_007d_003b) through them, and add each one as a [`Curve loop`](https://gmsh.info/doc/texinfo/gmsh.html#Curves).
* Create a [`Thrusections`](https://gmsh.info/doc/texinfo/gmsh.html#t19) between the loops.

However, despite my best efforts, this very often results in a geometry where only an outer surface is present, without a solid. This happens, I think, due to the outer surface curling excessively in certain spots. And this is, in turn, created by the strange step by which the cross sections are provided, together with some sharp turns of the ear canal center line.

My attempts at creating a good ear canal geometry are perhaps too lengthy to discuss in here. If you are interested, you can read the code at [`acoupy_ears`](https://gitlab.com/acoupy/acoupy_ears). This is the python package that I written to implement the geometry and it will be better documented in the future. As of now, I could only produce:

* A flat terminated ear canal model ending at 28 mm (Ear Canal End).
* A flat terminated ear canal model ending at the last EEP (Ear canal Entrance Point) projection point at 32.5 mm (EEP End).

If you want to install `acoupy_ears` into any python environment you can use:

```commandline
pip install git+https://gitlab.com/acoupy/acoupy_ears.git
```

After install you will have the command `build` avalable. Use `build --help` to see the help. The sub-commands allow you to save a BREP file of the geometry on disk. You can process this file with Salome as per usual.

### The Resulting Geometry

The ear canal geometry up to its end at 28 mm is shown below. Some of the unnatural bends of the surface are visible. Thankfully, a mesh size that works up to 20 kHz is still about 1 mm. This means that a mashing algorithm will still be successful in meshing this geometry, since the nodes end up falling on opposite sides of the unnatural bends.

{{< figure id="fig-1" src="/website/posts_res/39-ear-canal-part-2/canal-geo.png" title="Figure 1" caption="Ear Canal Geometry (28 mm)" class="mw10" >}}
 
The same holds for the ear canal geometry up to the last EEP projection point at 32.5 mm, which is shown below.

{{< figure id="fig-2" src="/website/posts_res/39-ear-canal-part-2/canal-geo-eep.png" title="Figure 2" caption="Ear Canal Geometry (32.5 mm)" class="mw10" >}}

Both geometries can be loaded into Salome and exploded in a solid. This solid can then be further exploded in 3 faces: the two flat terminations and the "tube" surface. The solid can therefore be meshed as usual with convenient mesh groups for boundary conditions. If this doesn't make any sense you can refer to a previus tutorial, for example [Home Studio - Part 1]({{< ref "/posts/12-home-studio-part-1.md#meshing" >}}), to see how you can load a BREP file into Salome, process its geometry and mesh it.

In this episode both ear canal models have been meshed with NETGEN to a maximum and minimum size of 1 mm. For details you can refer to the [Salome study file](https://gitlab.com/computational-acoustics/ear-canal-2/-/blob/main/preprocessing/ear-canal.hdf?ref_type=heads).

## The Benchmark Problem

I decided to introduce a simple benchmark to evaluate our ear canal models accuracy. As a first approximation, an ear canal is a cylinder. However, various sources differ on the size of this cylinder. This confusion is perhaps due to the complexity in measuring the length of an ear canal, but also in sources referring to dimension in the anatomical sense or in the sense of an equivalent model of ear canal. The former case is of straightforward interpretation. In the latter case instead we might be faced with parameters that have been tweaked to maximise the accuracy of some acoustic property (say, impedance) to a measured value.

For example, [Psychoacoustics Facts and Models](https://link.springer.com/book/10.1007/978-3-540-68888-4) by  Hugo Fastl and Eberhard Zwicker states (Third Edition, page 23):

> It \[the ear canal\] acts like an open pipe with a length of about 2 cm corresponding to a quarter of the wavelength of frequencies near 4 kHz.

In [Fundamentals of Acoustics](https://www.wiley.com/en-us/Fundamentals+of+Acoustics%2C+4th+Edition-p-9780471847892) by Lawrence E. Kinsler, Austin R. Frey, Alan B. Coppens and James V. Sanders instead we read (Fourth Edition, page 312):

> The auditory canal is an approximately straight tube, about 0.8 cm in diameter and 2.8 cm long, closed at its inner end by the _tympanic membrane_ (eardrum). The lowest resonance of this tube is broadly peaked around 3 kHz and affords appreciable gains from about 2 kHz to 6 kHz.

[Wikipedia](https://en.wikipedia.org/wiki/Ear_canal), citing Structural and functional anatomy of the outer and middle ear by Faddis, B. T. (which I couldn't access) states:

> The adult human ear canal extends from the auricle to the eardrum and is about 2.5 centimetres (1 in) in length and 0.7 centimetres (0.3 in) in diameter. 

Clearly, there are many different values floating around, and fundamental resonance ranges vary across significantly. The fundamental frequency is not only affected by the geometry, but also by the boundary condition. There is a difference between considering, for example, the ear canal geometry as fully enclosed by a rigid boundary versus terminated at one side by the tympanic membrane impedance and open on the other.

In this article we will do the simplest possible model. We will follow Kinsler and Frey and use 0.8 cm for diameter and 28 cm for height. This because the 28 cm length reported by them ties nicely with length of one of our ITU-T P.57 models. And we will solve with a rigid boundary condition on every boundary. That is, we consider the normal modes of the air contained in the ear canal when this is plugged by a rigid flat cap, assuming that the inside of the ear canal, including the ear drum, is a rigid boundary too.

We can compute the normal modes of the cylinder analytically in this case. Therefore, we will be able to assess whethe Elmer gives us good answers and, if so, we will switch to the ITU-T P.57 geometries with confidence.

The benchmark mesh, also meshed to 1 mm size, is shown below in comparison with the ear canal meshes from ITU-T P.57.

{{< figure id="fig-3" src="/website/posts_res/39-ear-canal-part-2/meshes.png" title="Figure 3" caption="Meshes Comparison" class="mw10" >}}
