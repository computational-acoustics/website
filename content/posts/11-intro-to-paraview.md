---
title:  "Intro to ParaView"
date:   2020-04-11 20:00:00 +0100
draft: false
featured_image: "https://www.paraview.org/wp-content/uploads/2022/09/ParaView-Logo.svg"
description: "A Basic Introduction to ParaView"
categories: ["Software"]
tags: ["Introductory", "Software", "ParaView"]
---

In the previous episodes we often made use of the [ParaView](https://www.paraview.org/) postprocessor to visualise our solution field from the [Elmer](http://www.elmerfem.org/blog/) solver. ParaView can do all sorts of cool visualisations and animations, as well as providing the way of doing quantitative analysis. It is by far the best option to visualise and postprocess results from Elmer. It also allows to export data in various formats, such as CSV, that allow us to do any additional kind of postprocessing or verification, by either using [Julia](https://duckduckgo.com/?t=ffab&q=julia+lan&ia=web), [Python](https://www.python.org/) or any other language, or even spreadsheet software if you fancy that (for whatever reason...). ParaView functionalities are so many, and so advanced, that it is impossible to cover them all in a blog post. For that, you should instead refer to [this page](https://www.paraview.org/resources/). In this page I will merely collect a few useful tips and tricks that I found useful when working on my studies. This page will be updated throughout the series.

# Basic ParaView Usage

Normally, Elmer will provide us with _*.vtu_ files. These files can be imported into ParaView. Upon boot, ParaView will look something like this:

{{< figure src="/website/posts_res/11-intro-to-paraview/opening_screen.png" title="Figure 1" caption="ParaView Opening Screen" class="mw10" >}}

In order to load  a _vtu_ file, we can act as follows:

* From the top menu, click _File_.
    * Select _Open_.
    * Navigate to your file, and select it.
    * Click on the _OK_ button.
* On the left, in the _Properties_ browser, you will see that the _Apply_ button is highlighted. If you click you will load all the data of the _vtu_ file into ParaView.
    
For example, below I loaded the Elmer file from the [Rigid Walled Room]({{< ref "/posts/10-rigid-walled-room.md" >}}) episode.

{{< figure src="/website/posts_res/11-intro-to-paraview/loaded_data.png" title="Figure 2" caption="ParaView Loaded with Data" class="mw10" >}}

We can see that ParaView opened a default view on our data, looking at it from above, and that our domain does not seem very interestingly coloured. With your mouse, you can:

* Rotate your point of view by holding the left button and panning around with the mouse.
* Zoom in and out by using the mouse wheel or by holding the right mouse button and panning around.
* Pan your view by holding the mouse middle button and panning around.

## Data Sets

Your _vtu_ file probably contains more than one single dataset. For example, the file I loaded above contains two arrays of data, _pressure wave 1_ and _pressure wave 2_, together with _GeometryIds_ which normally we do not directly use. As you remember from the previous episodes, _pressure wave 1_ and _pressure wave 2_ are the real and imaginary part, respectively, of the complex steady state pressure field output by Elmer Helmholtz solver.

A simple way to colour your domain by any of these datasets is by selecting it in the menu in the toolbar as follows:

{{< figure src="/website/posts_res/11-intro-to-paraview/coloring_by_pressure_wave_1.png" title="Figure 3.1" caption="Colouring by pressure wave 1" class="mw10" >}}

{{< figure src="/website/posts_res/11-intro-to-paraview/coloring_by_pressure_wave_1_result.png" title="Figure 3.2" caption="Result" class="mw10" >}}

You will probably remember from the [Rigid Walled Room]({{< ref "/posts/10-rigid-walled-room.md" >}}) episode that when there are many files called in a similar fashion to _case0001.vtu_, _case0002.vtu_, _case0003.vtu_ ... ParaView will group them all together when selecting which file to open, and you will be able to load them all at once. In this case, whenever ParaView recognises a series, it interprets it as time series. On the top of the window you can see some _play/stop/rewind_ style of controls. These allow you to cycle through all the case files. Not always the files are grouped by time. For example, in the [Rigid Walled Room]({{< ref "/posts/10-rigid-walled-room.md" >}}) episode they were just different steady state solutions at different frequencies, but ParaView will still cycle through them, treating them as frames of an animation.

On the left of the menu we used to select the field by which to colour our domain, you can see various buttons to set the scale and the colour map. It is possible to rescale according to the current frame data, a custom range, all data, or the visible data.

You should feel free to explore all controls.

## Filters

ParaView can manipulate the fields in many different ways to allow us to see things more clearly. This is achieved by filters. A selection of common filters is available as buttons right above the _Pipeline Browser_. In order to use them, a dataset has to be selected from the _Pipeline Browser_ to act as the input of the filter. Certain filters can take as input multiple datasets.

For example, let's try to apply the _Contour_ filter to our dataset:

* Select the dataset from the _Pipeline Browser_.
* Click the _Contour_ button right above the _Pipeline Browser_.

After you do so, you will see a view like the one below.

{{< figure src="/website/posts_res/11-intro-to-paraview/setting_the_contour_filter.png" title="Figure 4.1" caption="Setting the Contour Filter" class="mw10" >}}

Now we need to configure the filter, and this is done by editing the properties below the _Pipeline Browser_.

* In _Contour By_, select the field you want to contour by (in this case _pressure wave 1_.
* Under _Isosurfaces_, in _Value Range_, you can enter any values or configure a range by acting on the buttons on on the side.
* Once you are done, you can click on _Apply_ on the top of the _Properties_ section.

Below, my results for $10$ equally spaced _Isosurfaces_ values.

{{< figure src="/website/posts_res/11-intro-to-paraview/setting_the_contour_filter_result.png" title="Figure 4.1" caption="Result" class="mw10" >}}

This procedure is typical of any filter:
* Select the input dataset.
* Select the filter, either from the toolbar or by the _Filters_ menu.
* Configure the filter.
* Apply.

Note that the filter will create an output dataset. In the _Pipeline Browser_, you can click on the eyeball to enable view many different datasets at the same time. In this case, you could make use of the _Opacity_ control, as shown below.

{{< figure src="/website/posts_res/11-intro-to-paraview/multiple_datasets.png" title="Figure 5" caption="Multiple Datasets in View" class="mw10" >}}

You can also edit the background and add _Data Axes Grid_, and many more things, as splitting the render view with the buttons on the top left of the render view to see datasets side by side, or by crating many more _Layouts_.

This guide cannot be exhaustive, so now that you get the basics of ParaView, let me list some tricks I commonly use. For more information, you should refer to the the [ParaView Resources page](https://www.paraview.org/resources/).

# Tricks

## Linked 3D Views

* Click the _Split Horizontal_ or _Split Vertical_ button on the top left of the _RenderView_.

{{< figure src="/website/posts_res/11-intro-to-paraview/linked_3d_views_1.png" title="Figure 6.1" caption="Linked 3D Views 1" class="mw10" >}}

* Then, click the _Render View_ button. Right click anywhere on the view you just opened and select _Link Camera..._.

{{< figure src="/website/posts_res/11-intro-to-paraview/linked_3d_views_2.png" title="Figure 6.2" caption="Linked 3D Views 2" class="mw10" >}}

* Now just click on the other 3D view (the one on the left in the current example). This will link the cameras of the two views.

Now you can click on any other view. The _Pipeline Browser_ will update to the data being displayed in the selected view. So, you can have two linked views showing different things. For example, below we look at the raw _pressure wave 1_ and its _Isosurfaces_:

{{< figure src="/website/posts_res/11-intro-to-paraview/linked_3d_views_3.png" title="Figure 6.3" caption="Linked 3D Views 3" class="mw10" >}}

## Calculator Filter 

Select your data and add the calculator filter. This filter allows to compute quantities from the fields in the dataset in a simple way. For example, this expression produces the $\text{dB}$ SPL value from the field:

```text
20 * log10(sqrt(pressure wave 1^2 + pressure wave 2^2) / (sqrt(2) * 20e-6))
```

Where the $\sqrt{2}$ factor at the denominator is used for harmonic solutions to convert to root mean square.

{{< figure src="/website/posts_res/11-intro-to-paraview/calculator_filter.png" title="Figure 7" caption="Calculator Filter" class="mw10" >}}

## Python Calculator Filter 

The Python calculator filter allows you to do more complex things with respect the calculator filter. Select your data and add the python calculator filter. Then, put your expression in the _Properties_ section.

The inputs are of the calculator are served as arrays of classes with different attributes. These attributes are dictionaries at which we can access the field.

For example, this expression normalises the real part of the pressure field between $1$ and $-1$:

```python
inputs[0].PointData['pressure wave 1'] / max(abs(inputs[0].PointData['pressure wave 1']))
```

Our filter had one input, so we get into `inputs[0]`. Then, we are interested into the `PointData`, as that is the kind of data our field is stored as. Finally, we select the field of interest, `pressure wave 1`. For more information, refer to [this page](https://www.paraview.org/Wiki/Python_calculator_and_programmable_filter).

{{< figure src="/website/posts_res/11-intro-to-paraview/python_calculator_filter.png" title="Figure 8" caption="Python Calculator Filter" class="mw10" >}}

Another useful expression is this, which allows to correctly compute the phase of the complex field:

```python
arctan2(inputs[0].PointData['pressure wave 2'], inputs[0].PointData['pressure wave 1'])
```

## Python Annotation

Creating annotations is pretty easy (for example, use the _Text_ filter), but what if we want a different annotation for every time frame? This could be a bit tricky, actually.

My favourite way is to create many CSV files with the stuff I want to use for the annotation. For example, look at [this function](https://gitlab.com/computational-acoustics/featools.jl/-/blob/master/src/libFEATools.jl#L13). The code is from the [FEATools.jl](https://gitlab.com/computational-acoustics/featools.jl) package. The code will create $10$ different CSV files, each one containing a single value of frequency, and suffixed with `_1`, `_2`, ... `_10`, each containing just one element of the input array. A set of files like this can contain all the frequencies of of a scanning simulation like the one we did in the [Rigid Walled Room]({{< ref "/posts/10-rigid-walled-room.md" >}}) episode, for example. The following paragraphs show how to create a python annotation with these frequencies.

When we crate a set of CSV files we can go ahead on ParaView to open them up. Select _File_, than _Open_, and navigate to the directory where you have saved the CSV files. As ParaView did with our Elmer vtu _vtu_ files, it will show them all collapsed as below. Select them and click _OK_.

{{< figure src="/website/posts_res/11-intro-to-paraview/python_annotation_1.png" title="Figure 9.1" caption="Importing CSV Files for Python Annotation 1" class="mw10" >}}

Once you click _OK_ the _Open Data With..._ window will appear. Select _CSV Reader_ and click _OK_. Then, in the _Properties_ section on the left un-tick _Have Headers_, as we do not have any. Now you can click _Apply_ at the top of the _Properties_ section.

{{< figure src="/website/posts_res/11-intro-to-paraview/python_annotation_2.png" title="Figure 9.2" caption="Importing CSV Files for Python Annotation 2" class="mw10" >}}

This will open a new _SpreadSheetView_ which you can dismiss by clicking the _x_ button on its top right. Notice, however, how the _Attribute_ listbox in the _SpreadSheetView_ says _Row Data_. According to what kind of CSV you have you might need to cycle through different _Attributes_ until you find your data, but generally every spreadsheet you make yourself with the `writeParaViewTimeArray` function will just have _Row Data_.

Now, from the _Pipeline Browser_, select the newly imported table. Then click on _Filters_, _Alphabetical_ and _Python Annotation_ (you can also use _Search_).

With the Python annotation we can compose a string by using attributes from the input fields. From _Array Association_, select the the correct attribute (in our case _Row Data_). Then, we can just index into it when we composes our _Expression_. For example:

```python
"{0:.2f} Hz".format(input.RowData[0][0])
```

from the `input` variable, we select the `RowData` attribute. Since we have only one input, we select the first with `[0]`. Finally, since we have only one element in the row, we select the first with another `[0]`. This will produce the result below.

{{< figure src="/website/posts_res/11-intro-to-paraview/python_annotation_3.png" title="Figure 10" caption="Python Annotation" class="mw10" >}}

Now, the annotation will change for each frame according to the values in our original CSV files. This is how the annotations in the animations for the [Rigid Walled Room]({{< ref "/posts/10-rigid-walled-room.md" >}}) episode were made.

## Save Data to CSV

If after your calculation you want to save the data to a CSV file(s) I suggest you proceed as follows:

* Select the dataset you want to export to CSV from the _Pipeline Browser_.
* Select _File_, and then _Save Data_.
* Navigate to where you want to save files and input a file name. Then click _OK_.
* Configure now the _CSVWriter_, as its window will popup.

You might want to do different things. The screenshot below shows how to configure the writer to write all timesteps and all _Point Data_ arrays. Using _30_ as _Precision_ is overkill, but ensures all the digits are there. This will create a CSV where each row is for a mesh node point. The _x_, _y_, and _z_ coordinates of the point will be reported, and then all the values of the field at the node, in different columns. When ready, just click _OK_. This data can be used for validation or additional post-processing.

{{< figure src="/website/posts_res/11-intro-to-paraview/csvwriter_configuration.png" title="Figure 11" caption="CSVWriter Configuration" class="mw7" >}}

## Exporting Pictures, Videos and State

My favourite way to export a picture is by using _File_ > _Save Screenshot_. All controls are self explanatory. If you have multiple views, and you want all of them exported, remember to tick _Save All Views_ in the _Save Screenshot Options_ window.

To export the animation, go to _File_ > _Save Animation_. My favourite format is _avi_. If you have multiple views, and you want all of them exported, remember to tick _Save All Views_ in the _Save Animation Options_ window. You can set the _Frame Rate_ to your liking.

To convert the _avi_ files to _gif_, I like to use `ffmpeg`:

```bash
ffmpeg -i avi_anim.avi -r rate gif_anim.gif
```
Where you will have to replace `avi_anim.avi` to your actual _avi_ file name, `rate` to the desired frame rate and `gif_anim.gif` to the desired output file name. Note that `ffmpeg` is also able to do many clever things, like change the final size, or do some optimisation. For more information, refer to its [website](https://ffmpeg.org/).

Finally, you can save the ParaView state by going to _File_ > _Save State_. This will save a _pvsm_ file containing all of your work. If you want to load it into another ParaView session, go to _File_ > _Load State_. If the location of your dataset files is changed, you will be given option to find them again when loading.

## Exporting the Camera Position

Maybe you have one study of one system, and you postprocess it with ParaView. But then, you do another study of the same system, and want to postprocess it in a similar way, and make an animation from the same point of view. You can save the camera settings from one ParaView session and import them into another Paraview session.

To do so, click the _Adjust Camera_ button as shown below.

{{< figure src="/website/posts_res/11-intro-to-paraview/adjust_camera_1.png" title="Figure 12" caption="Adjust Camera" class="mw10" >}}

The options to save and load the camera settings will be available in the window that will pop up, under _Camera Parameters_.

{{< figure src="/website/posts_res/11-intro-to-paraview/adjust_camera_2.png" title="Figure 13" caption="Adjust Camera Window" class="mw7" >}}

## Hiding Parts of The Domain

Sometimes we work with simulations over different bodies. For example, in the [Tuning Fork - Part 3]({{< ref "/posts/27-tuning-fork-part-3" >}}) episode we simulated a tuning fork coupled with air. Since the simulation was set with the tuning fork body _inside_ a spherical air body this is what we see when we load the simulation data.

{{< figure src="/website/posts_res/11-intro-to-paraview/coupled-ex.png" title="Figure 14" caption="Coupled Systems Data" class="mw7" >}}

We can only see the external surface of the sphere, and not the fork inside. It turns out we can extract any part of the domain and concentrate on that. We will use this tuning fork problem as an example. 

First select the mesh of the subdomain of interest. In this example we want to select the tuning fork mesh. Zoom in until you clip through the sphere and can see the fork. This is shown below.

{{< figure src="/website/posts_res/11-intro-to-paraview/clip.png" title="Figure 15" caption="Zoom into the Subdomain" class="mw7" >}}

Above the 3D view you can see a number of controls. Activate the _Select Cells On_ and _Add selection_ as shown below.

{{< figure src="/website/posts_res/11-intro-to-paraview/sel.png" title="Figure 16" caption="Add Selection" class="mw6" >}}

Select over your subdomain, in this case the tuning fork. You will be selecting only the cells you can see, so turn the view and repeat until you selected all the cells on the fork. Do not worry about cells being selected on the sphere: we will remove them later. At the end of the process your selection will look like this:

{{< figure src="/website/posts_res/11-intro-to-paraview/sel-done.png" title="Figure 17" caption="Subdomain Selection" class="mw7" >}}

Now zoom out until you can see the sphere from outside. Activate the _Select Cells On_ and _Subtract selection_ as shown below.

{{< figure src="/website/posts_res/11-intro-to-paraview/selsub.png" title="Figure 18" caption="Subtract Selection" class="mw6" >}}

Now select all the cells on the spherical surface. This will remove them from the current selection. Again, you will do this only to the cells you can see, so rotate the view and repeat to remove all the cells from the sphere. At the end of the process you will have removed all the cells on the sphere, while all the cells selected on the fork will still be selected. You can confirm this by zooming back into the domain. 

At this point use the _Extract Selection_ filter. This will crate a new node in the pipeline which contains only the selected faces. You can process this new node with any filters of your liking.

The procedure above will select only the faces on the surface of the subdomain. In many cases this is enough. If you need more advanced selection you can use the other options on the top of the 3D view.

# Conclusion

This clearly was not an exhaustive guide on how to use ParaView, but it contains most of the things I find myself doing most of the time. I will update it with new tricks here and there as I write more articles on this project. This article can then serve as a quick reference for common ParaView hacks and links to the documentation.

In the next episodes we will be going back to modelling.

{{< featuredpic source="paraview.org" url="https://www.paraview.org" topbr="True" >}}

{{< cc >}}

