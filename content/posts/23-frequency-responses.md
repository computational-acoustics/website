---
title: "Frequency Responses"
date: 2021-01-16T10:59:31Z
draft: false
featured_image: "/posts_res/23-frequency-responses/featured.svg"
description: "More Realistic Impedances"
categories: ["Modelling"]
tags: ["Physics", "Linear System", "Frequency Response"]
---

In the [Home Studio - Part 3]({{< ref "/posts/21-home-studio-part-3.md" >}}) and [Home Studio - Part 4]({{< ref "/posts/22-home-studio-part-4.md" >}}) episodes we referred to the results of our frequency sweep as _frequency responses_. In those episodes we calculated the steady state pressure disturbance in a room at different frequencies and sampled the field at a couple of locations so to be able to plot the steady state magnitude at those locations as a function of frequency. It is intuitive to refer to this result as a frequency response, in the sense that the field magnitude as a function of frequency gives us an understanding of how strong the steady state disturbance is at any given frequency in our study. However, what is typically meant by by frequency response is in reality quite a different concept. This episode is intended to provide a cursory introduction to this concept, and explain why what we calculated in the previous episodes is different, even though we called it frequency response.

# Systems

First of all we need to think of any acoustic system as, well, a system. A system is any physical entity that we can study, and in this case accepts inputs and outputs.

For example, imagine our realistic room. We can think of the spherical source within as an input. If we think of the probe points as ideal microphones (which is what they are) then they are outputs.

It is evident then that an acoustic system in principles can enable multiple transmissions of acoustic waves: we can have multiple sources of different kinds and multiple receivers of different kinds. If we use one single source and a single receiver we can treat the _transmission line_ between the source and the receiver as a Single Input - Single Output (SISO) system. Otherwise, we could have multiple sources and one receiver to realise a Multiple Inputs - Single Output (MISO) system. Finally, in the case of Multiple Inputs (sources) and Multiple Outputs (receivers) we would call our system a MIMO system.

In any case the important bit to remember is that whatever the sources and receivers we use the transmission between them will not only be governed by the source and receiver internal dynamics, which dictate their capability of emit or receive acoustic waves as a function of frequency and wave incidence angle, but also on the properties of the acoustic system itself which dictates what kind of vibration the air within the sources and receivers can sustain.

This is evident by thinking about the [Home Studio - Part 3]({{< ref "/posts/21-home-studio-part-3.md" >}}) and [Home Studio - Part 4]({{< ref "/posts/22-home-studio-part-4.md" >}}) episodes. In these episodes the source was a spherical omnidirectional uniform velocity source while the receivers were ideal point-like omnidirectional receivers. Even in that case the room acted as complex _filter_ exhibiting multiple resonances. That behaviour was not dictated by the source and receiver properties but rather by the modal nature of the room itself. Changing the positions of source and receiver in the room will yield to a different resonance pattern despite the source and receiver being equally good at emitting and receiving along any direction. This because of the modal nature of the room, that makes certain modes easy to excite and other hard to excite and, conversely, easy to capture or hard to capture, depending on where source and receiver are placed (think again to the nodal surfaces and how a microphone placed in a nodal surface would never pick any sound for the frequency related to that nodal surface despite how loud the field will be in other locations).

We should then think about what kind of system an enclosure of air, like a room, is. Of course, similar considerations can apply to systems that are not enclosed, but we should refer to enclosed systems as that is the room that is the subject of the Home Studio episodes.

## Is the Room a Linear System?

A linear system is a kind of system for which scaling the input by a factor will produce an output scaled by the same factor. Also, if we provide as input the sum (superposition) of many signals the output will be the sum of the outputs the system would produce if exposed to each input in isolation.

Air in an enclosure can sustain both linear and nonlinear propagation governed respectively by linear and nonlinear wave equations. In the previous episodes we only considered linear wave equations. Linear wave equations govern very well propagation in large bodies of air, such as rooms, or even larger. In systems of this kind nonlinear behaviour will start appearing at very high pressures, typically in the order of $134$ $\text{dBSPL}$ in free field (see [Thomas Leissing, page 1](http://publications.lib.chalmers.se/records/fulltext/61697.pdf) and [Michael L Oelze](https://jontalle.web.engr.illinois.edu/uploads/473.F18/Lectures/Chapter_5d.pdf)). As these levels are very high and not often reached in realistic room acoustics settings treating the air enclosed in a room as a linear system is accurate. Things will be different in small enclosed spaces and high levels, the smaller the space the lower the level at which nonlinear behaviour will manifest itself. Different is the case for realistic sources (loudspeakers) and realistic receivers (microphones) which, especially in the case of loudspeakers, can instead exhibit significant nonlinear behaviour.

As a conclusion, we can safely treat the room itself as a linear acoustic system as long as we do not drive it so hard to force it to sustain pressure wave too high for a typical musical acoustics application.

## Is the Room Time Invariant?

A time invariant system is a system whose properties, and hence response, are constant in time. Realistic rooms might very well not be, actually. Think about heating: as the air changes temperature then its acoustic properties change. This even for the simplest linear wave equation, as temperature affects the phase speed of sound $c$, which is the main parameter upon which the linear wave equation depends. Or, think about air conditioning which, on top of gradients of temperature (so, not uniform temperature), introduces _flow_. In many cases real rooms really are not time invariant. However, that of time variance in systems is a complex topic and we should defer it. In the meanwhile we will model our room as a body of air in equilibrium at a certain temperature. This is exactly what we did in the [Home Studio - Part 3]({{< ref "/posts/21-home-studio-part-3.md" >}}) and [Home Studio - Part 4]({{< ref "/posts/22-home-studio-part-4.md" >}}) episodes. This helps in keeping the formalism we will develop in this episode consistent with the previous episodes.

## SISO? MISO? MIMO?

For simplicity, we should consider a single input (source) single output (receiver) SISO arrangement.

## Is the Room Stable?

That of stability is another important properties of systems. We define stability according to the Bounded Input - Bounded Output (BIBO) concept, that is, any signal we provide as input which has finite amplitude will result in an output with finite amplitude as well. This is clearly the case for the room itself, which is a passive system. Even if the walls are perfectly rigid the disturbance within will peak at a maximum finite value thanks to conservation of energy. Examples of unstable systems typically involve an active _feedback loop_, i.e. something that picks the output and feeds it back to the input, using energy externally provided to the system. For example, putting a microphone very close to a source will most likely produce a ringing, the external energy being consumed in this example being that provided to the microphone and loudspeaker amplifiers. In real systems nonlinear behaviour or unaccounted dissipation will intervene in limiting the amplitude to a certain maximum, but according to the linear equations that govern the feedback systems (typically as an approximation) the amplitude will diverge with time. That is an example of system that, if treated as a linear system, is unstable. Our room is not like that.

## Is the Room Causal?

Yes, as every physical system, applying a signal to the room _causes_ it to respond, and to have any response we need to apply the signal first.

## In Short

In this episode we will assume that the room is a causal Linear Time Invariant (LTI) stable (in the BIBO sense) SISO system by having a single emitter and a single receiver at fixed locations within its volume. These assumptions allow simple treatment and are consistent with the previous episodes. Along the same lines, we will not concern ourselves with the response of the emitter and receiver, that is, we are only interested in the response of the air interposed between them, as dictated by the conditions of said air, including the boundary conditions at the border of the room. We called this body of air _transmission line_.

# A Simplified Model


{{< drawio fname="/posts_res/23-frequency-responses/system-diagram.html" >}}

The diagram above represent the system we will be considering. We will concern ourselves with the description of the transmission line. It is important to clarify what we mean by transmission line again. Sound does not propagate through a line like, say, a well focused laser beam. Instead the whole of the air in the room will be put into vibration by the source. However, the law governing transmission of sound from the source to the receiver is dependent upon:

* The conditions of the air.
* The air boundary conditions at the room walls.
* The position of the source.
* The orientation of the source.
* The position of the receiver.
* The orientation of the receiver.

All of these parameters, when fixed, identify _one_ transmission law, which we can call transmission line. Note that the orientations of source and receiver are only important when they are not omnidirectional.

If $p\_{i}\left(t\right)$ is the pressure disturbance radiated by the source and $p\_{o}\left(t\right)$ is the pressure disturbance sensed by the receiver we can draw the system as follows:

{{< drawio fname="/posts_res/23-frequency-responses/simple-system.html" >}}

Where we represent the the radiated pressure disturbance $p\_{i}\left(t\right)$ and the sensed pressure disturbance $p\_{o}\left(t\right)$ as ordinary real functions of time $t$, expressed as a real value. We consider them defined over the whole real numbers set, $p\_{i}\left(t\right)$ being $0$ for times at which the source is off. These functions act as input and output of the transmission line (hence the subscripts $i$ and $o$). Since the transmission line is linear and time invariant it is _fully described_ by another ordinary function of time, $h\left(t\right)$, called the _impulse response_. The impulse response is defined over the whole real numbers set, but for causal systems is $0$ for all negative numbers. Typically it has large values for early times and progressively small values as time goes on.

The theory of LTI systems is deep and intricate, and discussing it fully is beyond the scope of this post. Below we will discuss just the most notable aspect, with special attention to what relates to the FEM field we produced in the previous episodes.

## The Impulse Response

As we said, the impulse response fully characterises a SISO LTI system. But how is so? Well, given any input $p\_{i}\left(t\right)$ the impulse response $h\left(t\right)$ provides a way to compute the $p\_{o}\left(t\right)$ through the operation of convolution:

$$ p\_{o}\left(t\right) = p\_{i}\left(t\right) \star h\left(t\right) $$

where, for the definition of convolution:

$$ p\_{i}\left(t\right) \star h\left(t\right) = \int\_\{-\infty}^{+\infty} p\_{i}\left(t-\tau\right)h\left(\tau\right)d\tau$$

but, being the system causal:

$$ p\_{o}\left(t\right) = \int\_\{0}^{+\infty} p\_{i}\left(t-\tau\right)h\left(\tau\right)d\tau$$

where we used the fact that the integrand will be $0$ for all values of the integration variable $\tau$ for which the impulse response $h$ is $0$ (i.e. all negative values, being $h$ causal). Being the operation of integration linear, it is understood how the definition above makes the system linear.

As you can see, convolution is a complex operation in which the output, at each instant of time $t$, is computed through a whole integral. The reasons why LTI systems work this way are profound and routed in functional analysis. For the time being, we should remark on one important aspect evident from the equation above.

As the integration variable $\tau$ slides from $0$ to infinity the input signal $p\_{i}$ will be evaluated at times $t-\tau$ which, given that $\tau$ is positive, are all _earlier_ times. This means that, to compute the output $p\_{o}$ at time $t$ we will for sure use the value of $p\_{i}$ at time $t$, but also all values of $p\_{i}$ previous to that. Which means that the output at any time is computed by using the input at _all previous times_. That is, the system has _memory_: the output will differ depending on what the input did in the past.

For example, if $p\_{i}$ is $0$ (the source is off) and then at time $t\_{1}$ we turn it on, then at time $t\_{1}$ we will have a certain output $p\_{o}$ dictated by $p\_{i}$ having been null so far. Let some time pass and at $t\_{2}$, after $p\_{i}$ has been running for some time, the output will be fairly different from that at $t\_{1}$. If the input signal is steady (for example a sine wave with fixed amplitude and frequency) then after a while also the output will be steady, but at the first instants after $t\_{1}$ we will observe a _transient_ in the output dictated by the input regime changing from $0$ to something else.

Conversely, as $h$ might take a long time, eventually forever, to decay to $0$ the system will produce some output even after the input is shut off at some later time $t\_{3}$. This output is sustained by the memory the system has of the signal at previous times, as well as its decaying impulse response. In essence, the output takes some time to decay to $0$ even if the input is abruptly stopped.

But what is this memory? Broadly speaking, it is mainly dynamics. Physical systems react to excitation, and this reaction need some time to be built. Think about moving an object. If the applied force is the input, and the resulting velocity is the output, we will need some time to reach a desired velocity, after which we can shut off the input and the velocity signal will slowly decay thanks to friction (if any in our system). This reaction time depends on the excitation itself as a physical system might not be able to react the same way to all types of excitation. In fact, this dependency of the system output on what the system has been doing in the past might ring some bells. In the [What is Acoustic Modelling]({{< ref "/posts/2-what-is-acoustic-modelling.md" >}}) we discussed PDEs and initial conditions. This is pretty much a similar line of reasoning. In fact, there is a direct relationship between PDEs and impulse responses (for all linear systems, that is). The memory simply captures the fact that a system response can only depend on its state and that of the forces acting on it at the time we start our modelling. Different initial conditions produce different outcomes. Modelling our system at $t\_{1}$ is equivalent to model it with a null initial condition, at $t\_{2}$ with a steady state initial condition, and so on and so forth.

## The Frequency Response

Finally, after having talked at great length about what kind of system an acoustic system can be, and having decided we can model an acoustic system (in appropriate circumstances) as a causal SISO LTI system, and having seen that such a system is fully described by its impulse response, we are finally able to define the proper frequency response.

Turns out that the frequency response doesn't give any more information about a system, it merely translate the impulse response in a conjugate domain, the frequency domain:

$$ H\left(j\omega\right) = \mathcal{F}\left\[h\left(t\right)\right\] $$

where $\mathcal{F}$ is the Fourier transform, operated on the impulse response $h$, to yield the complex function $H$ dependent upon the angular frequency $\omega$. Note how the notation above makes use of $j\omega$ for the argument of $H$, where $j$ is the imaginary unit. This not only helps remembering that $H$ is complex, but has roots in the _Transfer Function_, a more generalised system representation. In this episode we will only focus on the frequency response.

The frequency response is fully equivalent to the impulse response, but sort of "decomposes" the information contained in it frequency by frequency. Its magnitude is known as _magnitude response_:

$$ M\left(\omega\right) = |H\left(j\omega\right)| $$

while its phase is known as the phase response:

$$ \Phi\left(\omega\right) = \angle H\left(j\omega\right) $$

Hence:

$$ H\left(j\omega\right) =  M\left(\omega\right) \exp \left( j\Phi\left(\omega\right) \right) $$

The magnitude response is easy to interpret: for an harmonic input at a certain frequency, it tells us how much the system attenuates its input in order to produce the corresponding harmonic output.

The phase response is similarly straightforward: for an harmonic input at a certain frequency, it tells us how much the system changes the phase of its input in order to produce the corresponding harmonic output.

Note here that the frequency response, being the result of the Fourier transform of the _whole_ impulse response, has sort of lost the capability of making the transient behaviour of the system explicit. Instead, it makes the steady state behaviour explicit by showing us what happens to harmonic waves, i.e. eternal sine waves.

Harmonic waves do not exist, but they can be used to describe any signal as a superposition of infinite harmonic waves. The frequency response allows us then to see what will happen to each of those components as they pass through the linear system. Note then that the linear system does distort an input, but linearly, i.e. by changing, as a function of frequency, both amplitude and phase. This dependency on frequency makes linear systems _filters_.


# The Relationship Between System Theory Frequency Response and Helmholtz Solver Results

As we said in the introduction, in the previous episodes we referred to the results of our frequency sweep as frequency responses too. In those episodes we used the Helmholtz solver, and the fact that the Helmholtz solver produces complex quantities might right some bells with respect the frequency response formalism described above. In the [Interpreting Helmholtz Solver Solutions]({{< ref "/posts/18-interpreting-helmholtz-solver-solutions.md" >}}) episode we described what the physical meaning of Helmholtz solutions is. By comparing the two it should be evident how, when we described the results of the previous episodes, we somehow used the term "frequency response" improperly.

The magnitude of the Helmholtz solution is in reality very similar to the magnitude response. Both represent the steady state amplitude of a signal.

Things get different when we get to look at the phases. Through the Helmholtz solution we get to understand how the steady state phase field looks like in term of relative phase gaps between points in the domain. This doesn't tell us directly the phase change an harmonic signal component would undergo when propagating from source to receiver, which is instead what the phase response tells us.

So, when we sample the field at a probe point we can get an understanding of the magnitude response of the transmission line from source to probe, but not an understanding of the phase response. Hence, the complex Helmholtz field values sampled at a point do not represent directly a frequency response. 

# Conclusion

In this episode we understood how acoustic systems can be represented in the framework of system theory. Various representations are possible, the simplest of which is that of causal stable SISO LTI systems. In this case systems have an unique well defined impulse response, and hence a well defined unique frequency response. The impulse response fully describes the system and the frequency response illustrates how the system deals with individual steady state harmonic signals and, through linearity, with complex signals as well in a way that is equivalent to the impulse response.

With respect the previous episodes, we seen that the Helmholtz solution can help us directly evaluate the magnitude response. However, characterisation of the phase response is not direct, and hence the frequency response proper is not directly available in the solution field. Even though in the [Home Studio - Part 3]({{< ref "/posts/21-home-studio-part-3.md" >}}) and [Home Studio - Part 4]({{< ref "/posts/22-home-studio-part-4.md" >}}) episodes we referred to the results as frequency responses, they are more appropriately magnitude responses.

Of course, FEM methods (and also many other methods) can be used to simulate impulse responses too and this might form the core of future episodes.

# Further Reading

In order to understand this topic further any good book on Systems Theory and Systems Control should help. Below are some recommendations.

[The Scientist and Engineer's Guide to Digital Signal Processing](https://www.dspguide.com/) is a very good free resource that covers many of the topics discussed above (and many more), albeit (of course) with a focus on discrete time systems.

On a similar note, all free books and material by [Julius Orion Smith III](https://ccrma.stanford.edu/~jos/) are very good, although they tend to focus on discrete time systems as well.

A good introduction on Systems Theory and Control is included in the book [Active Control of Sound](https://www.elsevier.com/books/active-control-of-sound/nelson/978-0-12-515425-3), which has the benefit of focusing this discourse on acoustics.

Finally, there is plenty of good free resources online, including study materials from University courses. However, I cannot quite suggest any as I mainly studied those that I was provided with at my University, which as far as I know are not freely available online. 

{{< cc >}}

