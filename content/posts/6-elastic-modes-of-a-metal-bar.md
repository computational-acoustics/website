---
title: "Elastic Modes of a Metal Bar"
date: 2019-06-09 20:42:56 +0100
draft: false
featured_image: "/posts_res/6-elastic-modes-of-a-metal-bar/featured.png"
description: "Introduction to Elmer with a Simple Linear Elasticity Problem"
categories: ["Modelling"]
tags: ["Introductory", "Physics", "Elmer", "FEM", "Linear System", "Single Physics", "Steady State", "Modal"]
---

In the [last episode]({{< ref "/posts/5-acoustic-modes-of-a-rectangular-room.md" >}}) we examined the analytical solution of the acoustic modes of a rectangular room and we are now ready to take steps into moving in the world of FEM modelling. Elmer is a powerful package, but it is not extremely user friendly. So, it is best to have a gentle introduction to it first before dwelling into the intricacies of FEM modelling of acoustic fields. One of the simplest problems to solve with Elmer is that of the elastic vibration modes of solids. This problem will allow us to get started with Elmer in the most gentle way possible, by only using the GUI.

# The Relevance of Elastic Modes

Not only these are problems that one can setup _relatively_ easily, but they are also problems of central importance in acoustics. Exactly as [we observed for acoustic modes]({{< ref "/posts/5-acoustic-modes-of-a-rectangular-room.md" >}}), a weighted sum of modes is what defines the steady state vibration of elastic bodies, and the vibration of elastic bodies is one of the most important causes of sound radiation in fluids. As an example, see this beautiful simulation made with Elmer by [Ben Qui](https://www.youtube.com/channel/UCMcP1V6o9MfDyJSp2te6xkA)  on the acoustic field radiated by the elastic modes of a tuning fork.

{{< youtube i5bJL4CbAxk >}}

We will not try a simulation like that one (yet). We will study only the modes of a very simple bar of metal, without coupling it with air.

_By the way, [dymaxionkim](https://github.com/dymaxionkim/ElmerFEM_Examples) made similar Elmer studies, go check them out!_

_Note that, whilst this project recommends the use of Ubuntu as a base for a workstation (see [this episode]({{< ref "/posts/1-setting-up-an-ubuntu-modelling-environment.md" >}})) my operating system of choice is Arch Linux. Nothing will change in the way the use of the software is portrayed in the videos._

{{< load-table-dfl-style >}}

# Accompanying Videos

To make things easier to follow, I decided to record a bunch of videos in a tutorial style. I have put them into [one playlist on YouTube](https://www.youtube.com/playlist?list=PLYd6FLtUglLl6dFkWUGxnIRXVhCDVCKfA).

# The Workflow

In [The FEM Pipeline]({{< ref "/posts/4-the-fem-pipeline.md" >}}) episode we seen what the basic pipeline for FEM modelling looks like. In this episode we will see what it means to implement one. We will follow these steps:

* Geometry Setup.
* Meshing.
* Solver Setup.
* Solution.
* Visualization and Post-processing.

## Geometry Setup

The first step is always to define the geometry. There are many ways to do this, but we will use FreeCAD. The reason being that very often, when we model real systems, we will start from CAD files. So, it is best to get used to CAD programs.

In this example, we will create just a simple bar with square cross section, with a $10$ $\text{mm}$ by $10$ $\text{mm}$ base and a height of $100$ $\text{mm}$.

| Bar Width        | Bar Thickness    | Bar Length        |
|------------------|------------------|-------------------|
| $10$ $\text{mm}$ | $10$ $\text{mm}$ | $100$ $\text{mm}$ |

Watch the video below to see how to setup the geometry.

{{< youtube 1qHNmgLYgYs >}}

## Meshing

In the [FEM in a Nutshell]({{< ref "/posts/3-fem-in-a-nutshell.md" >}}) episode we discussed the basics of FEM and observed that meshing is the most crucial aspect of it. In this first episode we will start with a simple coarse, but second order, mesh. Although we will not have an exact theoretical model to compare the solution with (as we would do with our development pipeline) we will still review the results critically. As we made clear in [The FEM Pipeline]({{< ref "/posts/4-the-fem-pipeline.md" >}}) episode: **never blindly trust the solver**.

Watch how to use Salome to mesh our geometry in the video below.

{{< youtube lrPZdpB9FIg >}}

## Solver Setup and Solution

Elmer is the only bit of software that I did not get to work on Arch Linux. So, I will be using the virtual machine available [here](https://github.com/ElmerCSC/elmerfem/wiki/Packages).

Follow the video to learn how the solution process works.

{{< youtube Cfd6SMsLsOU >}}

## Visualization and Post-processing

Now that the problem is solved, we can look at the solution itself.

{{< youtube bM2rl0BnzTE >}}

By having a look around with ParaView it feels like the last mode, at highest frequency, has a bit of a strangely looking solution, with some weird stuff going on inside. Perhaps, to solve with good accuracy at such a high frequency, we need a finer mesh. In fact, at that mode, the metal bar is twisting around its own axis. So, to calculate the rotation of the bar as best as possible, we should have a high density of elements going from the axis to the borders. This is where FEM comes in handy: we can have different degrees of mesh density along different directions. We will see how to do that at a later episode.

# Can we Trust it?

This physical problem, that of the modes of a metal bar, is already quite not trivial. Other solutions available somewhere are still approximated. However, we used a bar, and not some other very complicated object such as a car door, for example, not only because that makes for a very simple introductory problem, but also to be able to compare with other available solutions, to build a sense of confidence in it. The goal is to find the correct way to mesh the geometry and set the solver so that we can trust the solution, and then we will use what we learn on more complex problems.

So, let's dig out other approximate solutions. One is reported [here](http://hyperphysics.phy-astr.gsu.edu/hbase/Music/barres.html). The solution in there holds for thin bars, while ours is a pretty thick one. Also, the boundary condition is not the same, as the clamped condition is also applied to the derivative of the field. But let's have a look anyway.

Let's calculate the first $3$ resonance frequencies of the bar according to the thin bar formula.

From the Elmer material library, we know the Young's modulus and density of the bar:

$$Y=7\cdot10^{10}\space \textrm{Pa}$$

$$\rho=2700\space \frac{\textrm{kg}}{\textrm{m}^{3}}$$

While length and thickness are known from our geometry:

$$L=0.1\space \textrm{m}$$

$$a=0.01\space \textrm{m}$$

So, in the thin bar approximation the fundamental frequency is:

$$f\_{1}\approx824.86\space \textrm{Hz}$$


Which is very very close to what our Elmer model predicts, with two close first modes at $824.50$ $\text{Hz}$ and $824.57$ $\text{Hz}$ respectively. As we noted in the video, the two modes are most likely degenerate, i.e. actually related to the same modal frequency, which we can think of being $824.53$ $\text{Hz}$ (the mean value). So, all good for the first modal frequency!

For the second, the thin bar theory predicts:

$$f\_{2}\approx5171.89\space \textrm{Hz}$$

While we got a prediction more akin to $4941.93$ $\text{Hz}$, by reasoning about degeneracy along the same lines as above. Whilst there is clearly a big difference, it is not as dramatic as it might look like, amounting to $78.92$ [cents](https://en.wikipedia.org/wiki/Cent_(music)). So, the difference is pretty much $80 \text{\%}$ of a semitone, not huge but significant: it would be definitely audible by a human, and for sure measurable.

Finally, let's look at the third frequency:

$$f\_{3}\approx14476.36\space \textrm{Hz}$$

That's now hugely different with respect what Elmer gave us, which is pretty much $7291$ $\text{Hz}$. So, what is going on?

Well, the thin bar modal theory that we are using as a benchmark not only works well with thin bars, but also if displacement is only happening in one direction: the benchmark theory works for transverse vibration. In fact, the benchmark solution seems consistent with the theory detailed, for example, [here](https://www.mapleprimes.com/DocumentFiles/206657_question/Transverse_vibration_of_beams.pdf). Now, if you recall the FEM solution, at $7291$ $\text{Hz}$ our bar was twisting around its own axis. So, clearly we are not talking about a transverse mode. In fact, if we look at our [reference page again](http://hyperphysics.phy-astr.gsu.edu/hbase/Music/barres.html) we see that while the first two modes in the picture kinda look like our first four from Elmer, the third really doesn't look like our fifth one from Elmer.

So, it looks like two things are going on here:

1. The most widely known analytical solution for the problem, the one which we are using as a benchmark, is derived in more special conditions with respect our FEM model, so direct comparison is hard.
2. At the same time, our mesh size might be introducing errors at the higher modes, as we seen in the video, and we should try to reduce these errors.

We ought to expected lower accuracy the higher the frequency, as the bar will be taking more complex vibration patterns, which hence would need a higher mesh density to be solved for accurately (just think of how many little mesh volumes we have per twist of the bar).

Still, the first mode frequency is quite accurate. The lowest mode is where we can expect all approximate models to agree, so it looks like we are going in the right direction.

In the next episode we will use all of this information to refine the model.

# Conclusion

Even when the problem looks simple, we should not just trust the solver. It is a good idea to solve a simple case first, and compare with other solutions available in literature.

Sometimes analytical solutions might be hard to find, even for simple models, but it is always possible to do some comparison with other approximate models, to critically review the results from our solver.

Once we are satisfied about the simple solution, we can apply the same meshing paradigm and solver setting to other problems, with much higher confidence in the results we will get.

In the next episode we will look into using the information above to improve our model.

{{< cc >}}

