---
title: "Home Studio - Part 4"
date: 2020-12-26T12:20:03Z
draft: false
featured_image: "/posts_res/22-home-studio-part-4/featured.png"
description: "More Realistic Impedances"
categories: ["Modelling"]
tags: ["Physics", "Elmer", "FEM", "Linear System", "Single Physics", "Steady State", "Frequency Response", "Series: Home Studio"]
---

In the [Home Studio - Part 3]({{< ref "/posts/12-home-studio-part-1.md" >}}) episode we simulated the steady state pressure field for a realistically shaped room governed by the linear wave equation. The boundary condition for the room was that of ideally rigid walls. We seen that the effect of such a boundary condition is an extremely uneven frequency response. This due to how sharp the resonances of the room are. In this episode we will introduce slightly more realistic boundary conditions and see what the effect on the solution is.

# Project Files

All the files used for this project are available at the repositories below:

* [Home Studio 6](https://gitlab.com/computational-acoustics/home-studio-6).

# Specific Acoustic Impedance

We encountered the specific acoustic impedance in many previous episodes. Without going into too much detail (there would be enough material for more than one episode) acoustic impedance represents the _opposition_ a surface offers to pressure wave propagation. This for any surface. For example, in [The Pulsating Sphere]({{< ref "/posts/8-the-pulsating-sphere.md" >}}) episode we seen how any surface, even if within an infinite body of a uniform medium in equilibrium, offers an impedance dependent upon the distance $r$ from the source centre. This due to the properties of the pressure wave emitted by such a source. However, this is not the only way we can find impedance. Impedance manifests itself at the surface separating two different mediums too, for example air and water, or air and concrete. In this case the different wave propagation properties in the two media are the reasons for the impedance. A change in impedance, either a continuous one as that of the specific acoustic impedance associated with spherical wave propagation, or that more abrupt at the separation between two media, give rise to two phenomena:

* Reflection; and
* Transmission.

In the previous episodes we specified ideally rigid boundaries. A rigid boundary has infinite impedance. You will recall that the specific acoustic impedance $z$ is defined as follows:

$$ z = \frac{p}{u} $$

Where $p$ is the pressure field at the surface of interest and $u$ is the particle flow velocity through that surface. This value is typically complex, and defines the:

* Portion of energy reflected;
* Portion of energy transmitted;
* Phase of reflected wave; and
* Phase of transmitted wave.

You will also recall that to specify a rigid boundary we told Elmer to impose the velocity $u$ to be $0$ $\frac{\text{m}}{\text{s}}$ at all boundaries with these boundary condition keywords:

```text
  Wave Flux 1 = 0
  Wave Flux 2 = 0
```

Where the flux $g$ is related to the velocity $u$ through this relationship (see the [Elmer Models Manual](ephy-resource:///org/gnome/epiphany/pdfjs/web/#chapter.12)):

$$ g = j \omega \rho u $$

with $j$ the imaginary unit, $\omega$ the angular frequency od the simulation and $\rho$ the density of the medium. $g$ is a complex quantity, and `1` and `2` in the keyword definition refer to real and imaginary part respectively. In this episode instead we will specify a finite value of impedance for every boundary instead of the flux.

# Specifying Impedance

Specifying impedance in Elmer boundary conditions is actually very easy. Just use the following keywords:

```text 
  Wave Impedance 1 = 
  Wave Impedance 2 = 
```

where `1` and `2` refer to the real and imaginary part of the impedance respectively. However, one must be careful with the value used with these keywords. If the **specific** acoustic impedance $z$ is known, in units of $\text{MKS}$ $\text{Rayl}$ then the quantity to use with the `Wave Impedance` keywords is:

$$Z = \frac{z}{\rho}$$

For a discussion of why, see [The Pulsating Sphere]({{< ref "/posts/8-the-pulsating-sphere.md" >}}) episode.

# The Model and Impedance Values

In general, impedance is a function of frequency. In this episode however we will do a simple model with frequency independent impedance values. This because, as we already know, it is best to add complication to a model in simple incremental steps.

The model is set up exactly as in the [Home Studio - Part 3]({{< ref "/posts/12-home-studio-part-1.md" >}}) episode, to which you can refer for guidance. The only different part in the `sif` file is the the boundary conditions specification. The `sif` file is available [here](https://gitlab.com/computational-acoustics/home-studio-6/-/blob/master/elmerfem/case.sif).

The table below summarises the chosen materials for the boundaries and their specific acoustic impedance values.

{{< load-table-dfl-style >}}
| Boundary | Material     | Impedance $\left\[\text{MKS Rayl}\right\]$ |
|----------|--------------|--------------------------------------------|
| Walls    | Concrete     | $8.00 \cdot 10^{6}$                        |
| Ceiling  | Concrete     | $8.00 \cdot 10^{6}$                        |
| Floor    | Pine         | $1.57 \cdot 10^{6}$                        |
| Door     | Oak          | $2.90 \cdot 10^{6}$                        |
| Window   | Silica Glass | $13.00 \cdot 10^{6}$                       |

The values are published by the [Onda Corporation](https://www.ondacorp.com/wp-content/uploads/2020/09/Solids.pdf) (see also the [abbreviations](https://www.ondacorp.com/wp-content/uploads/2020/09/Abbreviations.pdf)). The longitudinal value of impedance is the correct one to use since we are not interested in transversal wave propagation at the boundary. Note that all values are real. This means that the boundaries are modelled as offering only _resistance_ to the pressure wave: they work similarly to an electrical resistor.

That of specifying a real constant value for impedance is typically a simplification, and a good starting point for acoustic analysis and simulation. Frequency dependent complex impedance is an advanced measurement and simulation topic in acoustics. We can treat this analysis as a first approximation for the behaviour of these materials.

In order to setup our simulation, as we typically do, it is convenient to specify all of our study parameters in the `sif` `Simulation` section:

```text 
Simulation
  Max Output Level = 5
  Coordinate System = Cartesian
  Coordinate Mapping(3) = 1 2 3
  Simulation Type = Scanning
  Steady State Max Iterations = 1
  Output Intervals = 1
  Timestepping Method = BDF
  BDF Order = 1
  Timestep intervals = 134
  Coordinate Scaling = 0.001
  Solver Input File = case.sif
  Post File = case.vtu
$ f = 1.0 2.0 3.0 4.0 5.0 6.0 7.0 8.0 9.0 10.0 11.0 12.0 13.0 14.0 15.0 16.0 17.0 18.0 19.0 20.0 21.0 22.0 23.0 24.0 25.0 26.0 27.0 28.0 29.0 30.0 31.0 32.0 33.0 34.0 35.0 36.0 37.0 38.0 38.87460581286569 39.0 40.0 41.0 42.0 43.0 44.0 45.0 46.0 47.0 48.0 49.0 50.0 51.0 52.0 53.0 54.0 55.0 56.0 57.0 58.0 59.0 59.944517111825824 60.0 61.0 62.0 63.0 64.0 65.0 66.0 67.0 68.0 69.0 70.0 71.0 71.12338044621859 72.0 73.0 74.0 75.0 76.0 76.2226096881928 77.0 78.0 78.74873387812008 79.0 80.0 81.0 82.0 83.0 84.0 85.0 85.5638895654582 86.0 87.0 88.0 89.0 90.0 91.0 92.0 93.0 94.0 95.0 96.0 96.97157816943052 97.0 98.0 99.0 99.26649696726423 100.0 101.0 102.0 103.0 104.0 104.25320323657283 105.0 106.0 107.0 108.0 109.0 110.0 111.0 112.0 113.0 114.0 115.0 116.0 117.0 118.0 119.0 120.0 121.0 122.0 123.0 124.0 125.0
$ p = 1.205
$ U = 10
End
```

This also sets the medium density $\rho$ as `p` (we are simulating air at room temperature). This will allow easy definition of all impedance values through `MATC` expressions:

```text 
Boundary Condition 1
  Target Boundaries(1) = 6 
  Name = "Radiator"
  Wave Flux 2 = Variable time; Real MATC "2 * pi * f(tx - 1) * p * U"
End

Boundary Condition 2
  Target Boundaries(5) = 2 4 5 9 10 
  Name = "Concrete Walls"
  Wave Impedance 1 = Real MATC "8e6 / p"
  Wave Impedance 2 = 0
End

Boundary Condition 3
  Target Boundaries(1) = 8 
  Name = "Concrete Ceiling"
  Wave Impedance 1 = Real MATC "8e6 / p"
  Wave Impedance 2 = 0
End

Boundary Condition 4
  Target Boundaries(1) = 7 
  Name = "Pine Floor"
  Wave Impedance 1 = Real MATC "1.57e6 / p"
  Wave Impedance 2 = 0
End

Boundary Condition 5
  Target Boundaries(1) = 3 
  Name = "Oak Door"
  Wave Impedance 1 = Real MATC "2.9e6 / p"
  Wave Impedance 2 = 0
End

Boundary Condition 6
  Target Boundaries(1) = 11 
  Name = "Glass Window"
  Wave Impedance 1 = Real MATC "13e6 / p"
  Wave Impedance 2 = 0
End
```

Where the `Target Boundaries` are specified by referring to the `mesh.names` file exported by [salomeToElmer](https://github.com/mrkearden/salomeToElmer) (see the previous episodes for more information).

# Results

{{< youtube JvEGHfzGS84 >}}

The video above shows the steady state solution of the Helmholtz solver at all the simulation frequencies, shown in the top left corner. The video shows the SPL field on the left and the phase field on the right. The scale is that of the video shown in the [Home Studio - Part 3]({{< ref "/posts/12-home-studio-part-1.md" >}}) episode, for easier comparison.

It is possible to see that the SPL field looks very similar to that of the [Home Studio - Part 3]({{< ref "/posts/12-home-studio-part-1.md" >}}) episode. However, it reaches peak SPL values which are significantly lower.

The most conspicuous difference with respect the rigid boundary case is observed for the phase field. While for rigid boundaries the field presents sharp transitions between positive and negative phase, for realistic impedance values, albeit very high, the field has smooth transitions between positive and negative values.

In the [Home Studio - Part 3]({{< ref "/posts/12-home-studio-part-1.md" >}}) episode we plotted the SPL at two probe points in the room. We can do the same for the very same probe points for this study as well. The results are shown below.

{{< plotly fname="/posts_res/22-home-studio-part-4/responses.html" height_px=512 >}}

The [Home Studio - Part 3]({{< ref "/posts/12-home-studio-part-1.md" >}}) episode results are shown as solid lines (_rigid_), while the solutions from this episode are shown as dashed lines. The dotted vertical lines are drawn at the resonance frequencies found in the [Home Studio - Part 2]({{< ref "/posts/19-home-studio-part-2.md" >}}) episode, for reference. It is possible to see that the realistic impedance values act by strongly reducing the magnitude of the resonance peaks. However, the behaviour between the resonances is largely unchanged, and overall the system is still strongly modal.

# Conclusion

In this episode we run the same model of the [Home Studio - Part 3]({{< ref "/posts/12-home-studio-part-1.md" >}}) episode with slightly raised complexity and realism. We introduced more realistic impedance values for the boundaries, but we still kept them real and independent on frequency. This amounts to modelling the boundary as resistive elements for acoustic propagation. We seen that the consequence of this are:

* Reduction of the magnitude of the resonant peaks; and
* Smoother phase field.

This shows how strong the effect of boundary impedance on the resulting field is. However, whilst it is found that large values of impedance do not produce resonances as strong as those of ideally rigid boundaries, the behaviour for boundary impedance values in the order of that of commonly used building materials is still large enough to produce strongly resonant systems. This shows why, for listening spaces, room treatment is normally very important.

{{< cc >}}

