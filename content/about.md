---
title: "About"
featured_image: "/site_img/featured_image.png"
omit_header_text: true
description: "Experimenting and Disseminating"
type: page
menu: main
---

# The Project Author and History

This project was started by me, [Stefano Tronci](https://stefano-tronci.gitlab.io/website/), in 2018. Originally on a (now defunct) WordPress blog, this project reached its final location in 2020.

# Why this Project?

Whilst there is plenty of scientific open source software with incredible capabilities, the learning curve is often very steep. I decided to start this project with the aim of disseminating what I learned, and help others learning how to do acoustic simulations with open source software.

Commercial software capabilities are also terrific. However, commercial software is normally not affordable enough for ordinary people to purchase and use. At the same time, I cannot think of the use of commercial software in scientific settings as _scientifically appropriate_, as the scientific paradigms mandate that results should be _repeatable_: hence the algorithms and the code _must_ be public for any work made with them to be considered effectively scientific. With this project, I hope I will be able to promote the use of open source scientific software (which really is already commonplace) even further, if even just by a small margin.

Finally, open source software is normally far more educative. The steep learning curve often comes with the reward of more intimate knowledge of the inner workings of the numerical methods at the core of solvers and simulation packages, and a more solid grasp of how to use them to achieve the required accuracy and predictive power. This is not often the case for polished and shiny commercial packages which, by hiding complexity to favour ease of use, offer the risk of turning scientists in glorified software users. If I managed to learn anything useful in the domain of computational acoustics through working with open source software I hope I will be able to disseminate it.

# What License?

The material presented in this website is offered under the [Creative Commons Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/) license, while the website code is distributed under the [MIT](https://opensource.org/licenses/MIT) license. The various [associated projects](https://gitlab.com/computational-acoustics) code are available with their own licenses.

