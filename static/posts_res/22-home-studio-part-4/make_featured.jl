#!/usr/bin/env julia

using JLD
using Plots
pyplot()

data_response = load("response.jld")
data_reference = load("reference.jld")

plot(
    data_reference["frequencies"],
    20 * log10.(abs.(data_reference["response1"])),
    legend=false,
    linewidth=5,
    linecolor=:red,
    gridlinewidth=3,
    foreground_color=:black,
    background_color=:transparent,
    framestyle = :box,
    xaxis=false,
    yaxis=false,
    size=(1920, 1080)
)

plot!(
    data_response["frequencies"],
    20 * log10.(abs.(data_response["response1"])),
    legend=false,
    linewidth=5,
    linecolor=:blue,
    linestyle=:dash,
    gridlinewidth=3,
    foreground_color=:black,
    background_color=:transparent,
    framestyle = :box,
    xaxis=false,
    yaxis=false,
    size=(1920, 1080)
)

png("featured.png")

