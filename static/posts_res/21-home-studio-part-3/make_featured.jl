#!/usr/bin/env julia

using JLD
using Plots
pyplot()

data = load("response.jld")

plot(
    data["frequencies"],
    20 * log10.(abs.(data["response1"])),
    legend=false,
    linecolor=:red,
    linewidth=5,
    gridlinewidth=3,
    foreground_color=:black,
    background_color=:transparent,
    framestyle = :box,
    xaxis=false,
    yaxis=false,
    size=(1920, 1080)
)

png("featured.png")

