import pathlib
import h5py
import numpy as np
from scipy import interpolate
import plotly.graph_objects as go
from pywebplots import config
from pywebplots import plot
from pywebplots import file
import typing
import enum


def _make_plot_data(mesh: np.ndarray, field: np.ndarray, n_points: int = 1024) -> typing.Tuple[np.ndarray, np.ndarray]:

    radii = np.sqrt(np.sum(mesh**2, axis=1))
    idx = np.argsort(radii)

    x_full = radii[idx]
    y_full = field[idx]

    interpolator = interpolate.interp1d(x=x_full, y=y_full)
    x = np.linspace(np.min(x_full), np.max(x_full), n_points)
    y = interpolator(x)

    return x, y


class PlotType(enum.Enum):
    RESULTS = 0
    ERROR = 1


def _add_result_lines(
        fig: go.Figure,
        mesh: np.ndarray,
        solution: np.ndarray,
        exact: np.ndarray,
        plot_type: PlotType,
        name: str,
        color: str,
        n_points: int = 1024
) -> None:
    if plot_type == PlotType.RESULTS:
        mag = 20 * np.log10(np.abs(solution) / (np.sqrt(2) * 20e-6))
        phs = np.angle(solution)
    elif plot_type == PlotType.ERROR:
        mag = 20 * np.log10(np.abs(solution / exact))
        phs = np.angle(solution / exact)
    else:
        raise ValueError

    x, y = _make_plot_data(mesh=mesh, field=mag, n_points=n_points)
    plot.add_line_subplot(
        fig=fig,
        row=1,
        col=1,
        x=x,
        y=y,
        name=name,
        line_color=color,
        mode='lines',
        showlegend=True,
    )

    x, y = _make_plot_data(mesh=mesh, field=phs, n_points=n_points)
    plot.add_line_subplot(
        fig=fig,
        row=2,
        col=1,
        x=x,
        y=y,
        name=name,
        line_color=color,
        mode='lines',
        showlegend=False,
    )


def make_plots(plot_type: PlotType, study_path: pathlib.Path) -> None:
    basic_config = config.BasicConfig()
    basic_config.from_toml(
        conf_path=pathlib.Path(__file__).parent.parent.parent.parent.joinpath('config.toml')
    )

    fig = plot.subplots(
        basic_config=basic_config,
        rows=2,
        cols=1,
        shared_xaxes=True
    )

    fig.update_xaxes(title_text='Distance From Source Centre [m]', row=1, col=1)
    fig.update_xaxes(title_text='Distance From Source Centre [m]', row=2, col=1)

    if plot_type == PlotType.RESULTS:
        fig.update_yaxes(title_text='SPL [dBSPL]', row=1, col=1)
        fig.update_yaxes(title_text='Phase [rad]', row=2, col=1)
    elif plot_type == PlotType.ERROR:
        fig.update_yaxes(title_text='Error [dB]', row=1, col=1)
        fig.update_yaxes(title_text='Error [rad]', row=2, col=1)
    else:
        raise ValueError

    with h5py.File(study_path, 'r') as f:

        _add_result_lines(
            fig=fig,
            mesh=np.array(f['elmer/mesh']),
            solution=np.array(f['elmer/solution']),
            exact=np.array(f['elmer/exact']),
            plot_type=plot_type,
            name='Elmer',
            color='rgb(255, 0, 0)'
        )

        _add_result_lines(
            fig=fig,
            mesh=np.array(f['fenics/mesh']),
            solution=np.array(f['fenics/solution']),
            exact=np.array(f['fenics/exact']),
            plot_type=plot_type,
            name='FEniCS',
            color='rgb(0, 0, 255)'
        )

        if plot_type == PlotType.RESULTS:
            _add_result_lines(
                fig=fig,
                mesh=np.array(f['fenics/mesh']),
                solution=np.array(f['fenics/exact']),
                exact=np.array(f['fenics/exact']),
                plot_type=plot_type,
                name='Exact',
                color='rgb(0, 255, 0)'
            )

    if plot_type == PlotType.RESULTS:
        dst = pathlib.Path(__file__).parent.joinpath('{:s}_results.html'.format(study_path.stem))
    elif plot_type == PlotType.ERROR:
        dst = pathlib.Path(__file__).parent.joinpath('{:s}_error.html'.format(study_path.stem))
    else:
        raise ValueError

    file.save_plot_with_script(
        fig=fig,
        dst=dst
    )


if __name__ == '__main__':
    make_plots(plot_type=PlotType.RESULTS, study_path=pathlib.Path(__file__).parent.joinpath('5mm_study.hdf5'))
    make_plots(plot_type=PlotType.ERROR, study_path=pathlib.Path(__file__).parent.joinpath('5mm_study.hdf5'))
