import pathlib
import h5py
import numpy as np
from pywebplots import config
from pywebplots import plot
from pywebplots import file
import typing
from KDEpy import FFTKDE


def make_plot(
        study: pathlib.Path,
) -> None:

    basic_config = config.BasicConfig()
    basic_config.from_toml(
        conf_path=pathlib.Path(__file__).parent.parent.parent.parent.joinpath('config.toml')
    )

    fig = plot.subplots(
        basic_config=basic_config,
        rows=1,
        cols=2,
    )

    fig.update_xaxes(title_text='Error [dB]', row=1, col=1)
    fig.update_xaxes(title_text='Error [rad]', row=1, col=2)
    fig.update_yaxes(title_text='PDF', row=1, col=1)
    fig.update_yaxes(title_text='PDF', row=1, col=2)

    mag_pdf_fenics = FFTKDE(kernel='gaussian', bw=0.00001)
    phs_pdf_fenics = FFTKDE(kernel='gaussian', bw=np.pi/648000)
    mag_pdf_elmer = FFTKDE(kernel='gaussian', bw=0.00001)
    phs_pdf_elmer = FFTKDE(kernel='gaussian', bw=np.pi/648000)

    with h5py.File(study, 'r') as f:
        err_fenics = np.array(f['fenics/solution']) / np.array(f['fenics/exact'])
        mag_pdf_fenics.fit(20.0 * np.log10(np.abs(err_fenics)))
        phs_pdf_fenics.fit(np.angle(err_fenics))

        err_elmer = np.array(f['elmer/solution']) / np.array(f['elmer/exact'])
        mag_pdf_elmer.fit(20.0 * np.log10(np.abs(err_elmer)))
        phs_pdf_elmer.fit(np.angle(err_elmer))

    x, y = mag_pdf_fenics.evaluate(4096)
    plot.add_line_subplot(
        fig=fig,
        row=1,
        col=1,
        x=x,
        y=y,
        name='FEniCS',
        line_color='#1f77b4',
        mode='lines',
        showlegend=True,
    )

    x, y = mag_pdf_elmer.evaluate(4096)
    plot.add_line_subplot(
        fig=fig,
        row=1,
        col=1,
        x=x,
        y=y,
        name='Elmer',
        line_color='#ff7f0e',
        mode='lines',
        showlegend=True,
    )

    x, y = phs_pdf_fenics.evaluate(4096)
    plot.add_line_subplot(
        fig=fig,
        row=1,
        col=2,
        x=x,
        y=y,
        name='FEniCS',
        line_color='#1f77b4',
        mode='lines',
        showlegend=False,
    )

    x, y = phs_pdf_elmer.evaluate(4096)
    plot.add_line_subplot(
        fig=fig,
        row=1,
        col=2,
        x=x,
        y=y,
        name='Elmer',
        line_color='#ff7f0e',
        mode='lines',
        showlegend=False,
    )

    dst = pathlib.Path(__file__).parent.joinpath('stats.html')

    file.save_plot_with_script(
        fig=fig,
        dst=dst
    )


if __name__ == '__main__':
    make_plot(
        study=pathlib.Path(__file__).parent.joinpath('5mm_study.hdf5')
    )
