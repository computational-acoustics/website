#!/usr/bin/env julia

using JLD
using WebPlots

function plot_eigen_error_cents(data::Dict)

    bc = BasicConfig("../../../config.toml")

    fig = canvas(bc)

    abscissa = collect(1:length(data["eigen_error_cents"]))

    add_bar!(
        fig,
        x=abscissa,
        y=data["eigen_error_cents"]
        )

    fig.update_layout(
        xaxis=Dict(
            "tickmode" => "array",
            "tickvals" => abscissa,
            "ticktext" => string.(abscissa)
            )
    )

    fig.update_xaxes(title_text="Mode Number")
    fig.update_yaxes(title_text="Error [cents]")

    save_plot_with_script(fig, "eigen-error-cents.html")

end

function make_plots()

    data = load("validation_data.jld")

    plot_eigen_error_cents(data)

end

make_plots()

