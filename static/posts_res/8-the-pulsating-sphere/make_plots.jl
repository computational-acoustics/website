#!/usr/bin/env julia

using JLD
using WebPlots

function make_fig(data::Dict, decimation::Integer, file_name::String)

    radius = data["r"]

    ratio = data["femSolution"] ./ data["exactSolution"]
    ratio_dB = 20 .* log10.(abs.(ratio))
    ratio_rad = angle.(ratio)

    seld_idx = ((1:length(radius)) .- 1) .% decimation .== 0

    fig = subplots(
        BasicConfig("../../../config.toml"),
        rows=2,
        cols=1,
        shared_xaxes=true
        )

    add_line_subplot!(
        fig,
        1,
        1,
        x=radius[seld_idx],
        y=ratio_dB[seld_idx],
        name="dB Error",
        mode="lines"
        )

    add_line_subplot!(
        fig,
        2,
        1,
        x=radius[seld_idx],
        y=ratio_rad[seld_idx],
        name="Phase Error",
        mode="lines"
        )

    fig.update_xaxes(title_text="Distance from Center [m]", row=2, col=1)
    fig.update_yaxes(title_text="[dB re Exact]", row=1, col=1)
    fig.update_yaxes(title_text="[rad re Exact]", row=2, col=1)

    save_plot_with_script(fig, file_name)

end

make_fig(load("validation_data_coarse_mesh.jld"), 100, "coarse_mesh_plot.html")
make_fig(load("validation_data_fine_mesh.jld"), 100, "fine_mesh_plot.html")

