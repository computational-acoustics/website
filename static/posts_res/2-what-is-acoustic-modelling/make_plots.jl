#!/usr/bin/env julia

using WebPlots

c = 1
p = 4

x(t) = c * t^2

t_ax = collect(0:0.001:5)

bc = BasicConfig("../../../config.toml")

fig = canvas(bc)

fig.update_layout(
    xaxis_title="Time [s]",
    yaxis_title="Position [m]"
    )

add_line!(fig, x=t_ax, y=x.(t_ax), name="x(t)", mode="lines")

save_plot_with_script(fig, "x_plot.html")

s(t) = 2 * c * t

x_tan(t) = s(p) * t + x(p) - s(p) * p

add_line!(fig, x=t_ax, y=x_tan.(t_ax), name="Tangent in t = 4 s", mode="lines")

save_plot_with_script(fig, "x_tan_plot.html")

