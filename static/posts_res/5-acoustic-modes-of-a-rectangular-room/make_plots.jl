#!/usr/bin/env julia

using AcousticModels
using WebPlots

Lx = 5.0
Ly = 4.0
Lz = 3.0

A, B, C = index_grid(100, 100, 100)
F = mode_frequency.(A, B, C, Lx, Ly, Lz)[:]
l, c, u = exact_third_octave_bands()
counts = [sum((F .>= l[n]) .& (F .< u[n])) for n in 1:length(c)]
sel_idx = u .<= 4e3

bc = BasicConfig("../../../config.toml")

fig_hist = canvas(bc)

add_bar!(
    fig_hist,
    x=c[sel_idx],
    y=counts[sel_idx],
    width=(u - l)[sel_idx]
    )

fig_hist.update_xaxes(type="log", title_text="Frequency [Hz]")
fig_hist.update_yaxes(type="log", title_text="Number of Modes")

save_plot_with_script(fig_hist, "mode_hist_plot.html")

lh = 1.80

nx = 1
ny = 0
nz = 0

rx = 0.1
ry = 0.1
rz = 0.1

x, y, z = room_axes(rx, ry, rz, Lx, Ly, Lz)
X, Y, Z = mesh_grid(x, y, z)
shape = mode.(X, Y, Z, nx, ny, nz, Lx, Ly, Lz)
freq = mode_frequency(nx, ny, nz, Lx, Ly, Lz)
l_idx = argmin(abs.(z .- lh))
h = z[l_idx]

fig_mode_100 = canvas(bc)

fig_mode_100.update_layout(
    title="$freq Hz Mode, z = $h m",
    xaxis_title="x [m]",
    yaxis_title="y [m]"
    )

add_heatmap!(
    fig_mode_100,
    x=X[:],
    y=Y[:],
    z=shape[:, :, l_idx][:],
    colorscale="Spectral",
    zmin=-1,
    zmax=1,
    zsmooth = "best"
    )

fig_mode_100.update_layout(
    yaxis = Dict(
        "scaleanchor" => "x",
        "scaleratio" => 1
        )
    )

save_plot_with_script(fig_mode_100, "mode_plot_100.html")

nx = 2
ny = 3
nz = 2

rx = 0.1
ry = 0.1
rz = 0.25

x, y, z = room_axes(rx, ry, rz, Lx, Ly, Lz)
X, Y, Z = mesh_grid(x, y, z)
shape = mode.(X, Y, Z, nx, ny, nz, Lx, Ly, Lz)
freq = mode_frequency(nx, ny, nz, Lx, Ly, Lz)

fig_iso_232 = canvas(bc)

fig_iso_232.update_layout(
    title="$(round(mode_frequency(nx, ny, nz, Lx, Ly, Lz), digits=1)) Hz Mode",
    scene = Dict(
        "xaxis_title" => "x [m]",
        "yaxis_title" => "y [m]",
        "zaxis_title" => "z [m]"
        )
    )

add_isosurface!(
    fig_iso_232,
    x=X[:],
    y=Y[:],
    z=Z[:],
    value=mode.(X[:], Y[:], Z[:], 2, 3, 2, Lx, Ly, Lz),
    colorscale="Spectral",
    isomin=-1,
    isomax=1,
    surface_count=10,
    colorbar_nticks=10,
    caps=Dict("x_show" => false, "y_show" => false, "z_show" => false)
)

save_plot_with_script(fig_iso_232, "mode_3D_plot_232.html")

