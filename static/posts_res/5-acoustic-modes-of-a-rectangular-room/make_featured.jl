#!/usr/bin/env julia

using AcousticModels
using Plots
pyplot()

rx = 0.01
ry = 0.01
rz = 0.01

lh = 1.80

Lx = 5.0
Ly = 4.0
Lz = 3.0

x, y, z = room_axes(rx, ry, rz, Lx, Ly, Lz)

cb(xv, yv) = mode.(xv,yv, lh, 4, 5, 1, Lx, Ly, Lz)

plot(
    x, 
    y, 
    cb, 
    c=:Spectral_11,
    st=:contourf,
    legend=false,
    background_color=:transparent,
    foreground_color=:black,
    xaxis=false,
    yaxis=false,
    size = (1920, 1080)
)

png("featured.png")

