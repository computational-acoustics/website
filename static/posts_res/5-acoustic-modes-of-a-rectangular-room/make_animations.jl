#!/usr/bin/env julia

using AcousticModels
using Plots
pyplot()

lh = 1.80

Lx = 5.0
Ly = 4.0
Lz = 3.0

nx = 1
ny = 0
nz = 0

rx = 0.01
ry = 0.01
rz = 0.1

x, y, z = room_axes(rx, ry, rz, Lx, Ly, Lz)
X, Y, Z = mesh_grid(x, y, z)
shape = mode.(X, Y, Z, nx, ny, nz, Lx, Ly, Lz)
freq = mode_frequency(nx, ny, nz, Lx, Ly, Lz)
l_idx = argmin(abs.(z .- lh))
h = z[l_idx]

eigenV  = mode.(X, Y, Z, nx, ny, nz, Lx, Ly, Lz)
eigenF  = mode_frequency(nx, ny, nz, Lx, Ly, Lz)

li = argmin(abs.(z .- lh))

eigenT = 1.0 / eigenF
Tsteps = 64
nT = 1
aL = nT * eigenT
Fs = Tsteps * eigenF
nF = floor(Integer, aL * Fs)
fps = 12
f_name = "mode_plot_100.gif"

anim = @animate for i = 1:nF
    heatmap(
        x,
        y,
        eigenV[:, :, li] .* cospi(2 * eigenF * (i - 1) / Fs),
        c=:Spectral_11,
        clim=(-1, 1),
        legend=true,
        framestyle=:box,
        background_color=:transparent,
        foreground_color=:black,
        aspect_ratio=:equal,
        xaxis=false,
        yaxis=false,
        dpi=1000,
        size=(2 * 971, 2 * 454)
    )
end

gif(anim, f_name, fps=fps)

