#!/usr/bin/env julia

using JLD
using WebPlots
using PyCall

function make_subplot(
    fig::PyObject, 
    data::Dict, 
    row::Int, 
    col::Int, 
    cback::Function,
    name::String
    )

    r = sqrt.(
        data["points"][:, 1].^2 .+ 
        data["points"][:, 2].^2 .+ 
        data["points"][:, 3].^2
        )
    
    i = sortperm(r)
    
    add_line_subplot!(
        fig,
        row,
        col,
        x=r[i],
        y=cback.(data["c_field"]),
        name=name,
        mode="lines"
        )

end

data_29mm_2 = load("pulsating_sphere_1/check_t0001.jld")
data_05mm_2 = load("pulsating_sphere_2/check_t0001.jld")
data_29mm_1 = load("pulsating_sphere_3/check_t0001.jld")
data_05mm_1 = load("pulsating_sphere_4/check_t0001.jld")

fig_level = subplots(
    BasicConfig("../../../config.toml"),
    rows=2,
    cols=2,
    shared_xaxes=true
    )

to_level(x) = 20 * log10(abs(x))

make_subplot(fig_level, data_29mm_1, 1, 1, to_level, "29mm - Order 1")
make_subplot(fig_level, data_29mm_2, 1, 2, to_level, "29mm - Order 2")
make_subplot(fig_level, data_05mm_1, 2, 1, to_level, "5mm - Order 1")
make_subplot(fig_level, data_05mm_2, 2, 2, to_level, "5mm -  Order 2")

fig_level.update_xaxes(title_text="Distance from Center [m]", row=2, col=1)
fig_level.update_xaxes(title_text="Distance from Center [m]", row=2, col=2)
fig_level.update_yaxes(title_text="C Magnitude [dB]", row=1, col=1)
fig_level.update_yaxes(title_text="C Magnitude [dB]", row=2, col=1)

max_level = maximum(
    to_level.(
        [
            data_29mm_2["c_field"]; 
            data_05mm_2["c_field"];
            data_29mm_1["c_field"];
            data_05mm_1["c_field"]
        ]
        )
    )
    
min_level = minimum(
    to_level.(
        [
            data_29mm_2["c_field"]; 
            data_05mm_2["c_field"];
            data_29mm_1["c_field"];
            data_05mm_1["c_field"]
        ]
        )
    )
    
fig_level.update_yaxes(range=[min_level; max_level], row=1, col=1)
fig_level.update_yaxes(range=[min_level; max_level], row=1, col=2)
fig_level.update_yaxes(range=[min_level; max_level], row=2, col=1)
fig_level.update_yaxes(range=[min_level; max_level], row=2, col=2)

save_plot_with_script(fig_level, "plot_level.html")

fig_angle = subplots(
    BasicConfig("../../../config.toml"),
    rows=2,
    cols=2,
    shared_xaxes=true,
    shared_yaxes=true
    )

make_subplot(fig_angle, data_29mm_1, 1, 1, angle, "29mm - Order 1")
make_subplot(fig_angle, data_29mm_2, 1, 2, angle, "29mm - Order 2")
make_subplot(fig_angle, data_05mm_1, 2, 1, angle, "5mm - Order 1")
make_subplot(fig_angle, data_05mm_2, 2, 2, angle, "5mm -  Order 2")

fig_angle.update_xaxes(title_text="Distance from Center [m]", row=2, col=1)
fig_angle.update_xaxes(title_text="Distance from Center [m]", row=2, col=2)
fig_angle.update_yaxes(title_text="C Phase [rad]", row=1, col=1)
fig_angle.update_yaxes(title_text="C Phase [rad]", row=2, col=1)

max_angle = maximum(
    angle.(
        [
            data_29mm_2["c_field"]; 
            data_05mm_2["c_field"];
            data_29mm_1["c_field"];
            data_05mm_1["c_field"]
        ]
        )
    )
    
min_angle = minimum(
    angle.(
        [
            data_29mm_2["c_field"]; 
            data_05mm_2["c_field"];
            data_29mm_1["c_field"];
            data_05mm_1["c_field"]
        ]
        )
    )
    
fig_angle.update_yaxes(range=[min_angle; max_angle], row=1, col=1)
fig_angle.update_yaxes(range=[min_angle; max_angle], row=1, col=2)
fig_angle.update_yaxes(range=[min_angle; max_angle], row=2, col=1)
fig_angle.update_yaxes(range=[min_angle; max_angle], row=2, col=2)

save_plot_with_script(fig_angle, "plot_angle.html")

