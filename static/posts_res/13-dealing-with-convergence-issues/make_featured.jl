#!/usr/bin/env julia

using DelimitedFiles
using Plots
pyplot()

data = readdlm("convergence_400_Hz.csv", ',')

plot(
    data[:, 1],
    data[:, 2],
    xscale=:log10,
    yscale=:log10,
    legend=false,
    linecolor=:red,
    linewidth=5,
    gridlinewidth=3,
    foreground_color=:black,
    background_color=:transparent,
    framestyle = :box,
    xaxis=false,
    yaxis=false,
    size=(1920, 1080)
)

png("featured.png")
