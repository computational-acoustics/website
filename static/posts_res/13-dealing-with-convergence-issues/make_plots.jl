#!/usr/bin/env julia

using JLD
using DelimitedFiles
using WebPlots


function make_convergence_plot()

    tol = 1e-10

    fig = canvas(BasicConfig("../../../config.toml"))

    fig.update_layout(
        xaxis_title="Iterations",
        yaxis_title="Convergence Norm"
        )

    fig.update_xaxes(type="log")
    fig.update_yaxes(type="log")

    low_freq = load("convergence_data_16_Hz_100_Hz.jld")["cinfo"]
    high_freq = load("convergence_data_125_Hz_400_Hz.jld")["cinfo"]

    min_it = Inf
    max_it = -Inf

    for f in sort(float.(keys(low_freq)))

        iter = low_freq[f][:, 1]
        data = low_freq[f][:, 2]

        min_it = min_it < minimum(iter) ? min_it : minimum(iter)
        max_it = max_it > maximum(iter) ? max_it : maximum(iter)

        add_line!(
            fig,
            x=iter,
            y=data,
            mode="lines",
            name=string(round(f; digits=2)) * " Hz"
            )
    end

    for f in sort(float.(keys(high_freq)))

        iter = high_freq[f][:, 1]
        data = high_freq[f][:, 2]

        min_it = min_it < minimum(iter) ? min_it : minimum(iter)
        max_it = max_it > maximum(iter) ? max_it : maximum(iter)

        add_line!(
            fig,
            x=iter,
            y=data,
            mode="lines",
            name=string(round(f; digits=2)) * " Hz"
            )
    end

    tol_iter = [min_it; max_it]
    tol_line = tol * ones(size(tol_iter))

    add_line!(
        fig,
        x=tol_iter,
        y=tol_line,
        mode="lines",
        name="Tol.",
        line = Dict("color" => "firebrick", "dash" => "dot")
        )

    save_plot_with_script(fig, "convergence_plot.html")

end

function monodimensional_mode(
    n::Int,
    x::AbstractArray,
    L::Real,
    c::Real,
    A::Real = 1,
    phi_over_pi::Real = 0
    )

    f = 0.5 * c * n / L
    l = 2 * L / n

    S = A .* cospi.((n .* x ./ L) .+ phi_over_pi)

    return f, l, S

end

function make_modal_plot()

    L = 4.35
    c = 343
    dx = 0.01
    x = 0:dx:L

    n_values = [1;10]
    colours = ["mutedblue"; "safetyorange"]

    fig = canvas(BasicConfig("../../../config.toml"))

    fig.update_layout(
        xaxis_title="x",
        yaxis_title="S(x)"
        )

    for n in n_values

        f, l, S = monodimensional_mode(n, x, L, c)

        add_line!(
            fig,
            x=x,
            y=S,
            mode="lines",
            name="n = " * string(n) * ": " * string(round(f; digits=2)) * " Hz"
            )
    end

    save_plot_with_script(fig, "modal_example.html")

end

function make_modal_errors_plot()

    L = 4.35
    c = 343
    dx = 0.01
    x = 0:dx:L

    n_values = [1;10]
    colours = ["#636EFA"; "#EF553B"]

    A_i = 1 + 1e-2 * randn()
    x_shift = 3e-2 + 1e-2 * randn()

    fig = canvas(BasicConfig("../../../config.toml"))

    fig.update_layout(
        xaxis_title="x",
        yaxis_title="S(x)"
        )

    for n in 1:length(n_values)

        f, l, S = monodimensional_mode(n_values[n], x, L, c)

        phi_over_pi = -2 * x_shift / l

        _, _, S_err = monodimensional_mode(n_values[n], x, L, c, A_i, phi_over_pi)

        add_line!(
            fig,
            x=x,
            y=S,
            mode="lines",
            name="n = " * string(n_values[n]) * ": " * string(round(f; digits=2)) * " Hz",
            line = Dict("color" => colours[n])
            )

        add_line!(
            fig,
            x=x,
            y=S_err,
            mode="lines",
            name="n = " * string(n_values[n]) * ": " * string(round(f; digits=2)) * " Hz (With Error)",
            line = Dict("color" => colours[n], "dash" => "dot")
            )
    end

    save_plot_with_script(fig, "modal_error.html")

end

function make_convergence_plot()

    L = 4.35
    c = 343
    dx = 0.01
    x = 0:dx:L

    n_values = 1:50
    norms = zeros(size(n_values))

    A_i = 1 + 1e-2 * randn()
    x_shift = 3e-2 + 1e-2 * randn()

    for n in 1:length(n_values)

        f, l, S = monodimensional_mode(n_values[n], x, L, c)

        phi_over_pi = -2 * x_shift / l

        _, _, S_err = monodimensional_mode(n_values[n], x, L, c, A_i, phi_over_pi)

        norms[n] = sqrt(sum(abs2.(S - S_err)) * dx)
    end

    fig = canvas(BasicConfig("../../../config.toml"))

    fig.update_layout(
        xaxis_title="ξ",
        yaxis_title="E"
        )

    fig.update_xaxes(type="log")
    fig.update_yaxes(type="log")

    xi = 0.5 * n_values

    add_line!(
        fig,
        x=xi,
        y=norms,
        mode="lines",
        name="E",
        )

    save_plot_with_script(fig, "norms.html")

end

function make_final_convergence_plot()

    tol = 1e-10

    data = readdlm("convergence_400_Hz.csv", ',')
    high_freq = load("convergence_data_125_Hz_400_Hz.jld")["cinfo"]

    fig = canvas(BasicConfig("../../../config.toml"))

    fig.update_layout(
        xaxis_title="Iterations",
        yaxis_title="Convergence Norm"
        )

    fig.update_xaxes(type="log")
    fig.update_yaxes(type="log")

    add_line!(
        fig,
        x=high_freq[400][:, 1],
        y=high_freq[400][:, 2],
        mode="lines",
        name="Original Study"
        )

    add_line!(
        fig,
        x=data[:, 1],
        y=data[:, 2],
        mode="lines",
        name="Refactored Study"
        )

    tol_iter = [minimum(data[:, 1]); maximum(data[:, 1])]
    tol_line = tol * ones(size(tol_iter))

    add_line!(
        fig,
        x=tol_iter,
        y=tol_line,
        mode="lines",
        name="Tol.",
        line = Dict("color" => "firebrick", "dash" => "dot")
        )

    save_plot_with_script(fig, "final.html")

end

make_convergence_plot()
make_modal_plot()
make_modal_errors_plot()
make_convergence_plot()

