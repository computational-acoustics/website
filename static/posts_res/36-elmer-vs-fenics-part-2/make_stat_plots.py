import pathlib
import h5py
import numpy as np
from pywebplots import config
from pywebplots import plot
from pywebplots import file
import typing
from KDEpy import FFTKDE


def make_plot(
        study_list: typing.List[pathlib.Path],
        legend_list: typing.List[str],
        color_list: typing.List[str]
) -> None:

    assert len(study_list) == len(legend_list)

    basic_config = config.BasicConfig()
    basic_config.from_toml(
        conf_path=pathlib.Path(__file__).parent.parent.parent.parent.joinpath('config.toml')
    )

    fig = plot.subplots(
        basic_config=basic_config,
        rows=1,
        cols=2,
    )

    fig.update_xaxes(title_text='Error [dB]', row=1, col=1)
    fig.update_xaxes(title_text='Error [rad]', row=1, col=2)
    fig.update_yaxes(title_text='PDF', row=1, col=1)
    fig.update_yaxes(title_text='PDF', row=1, col=2)

    mag_pdf = FFTKDE(kernel='gaussian', bw=0.00001)
    phs_pdf = FFTKDE(kernel='gaussian', bw=np.pi/648000)

    for n in range(len(study_list)):
        with h5py.File(study_list[n], 'r') as f:
            err = np.array(f['fenics/solution']) / np.array(f['fenics/exact'])
            mag_pdf.fit(20.0 * np.log10(np.abs(err)))
            phs_pdf.fit(np.angle(err))

        x, y = mag_pdf.evaluate(4096)
        plot.add_line_subplot(
            fig=fig,
            row=1,
            col=1,
            x=x,
            y=y,
            name=legend_list[n],
            line_color=color_list[n],
            mode='lines',
            showlegend=True,
        )

        x, y = phs_pdf.evaluate(4096)
        plot.add_line_subplot(
            fig=fig,
            row=1,
            col=2,
            x=x,
            y=y,
            name=legend_list[n],
            line_color=color_list[n],
            mode='lines',
            showlegend=False,
        )

    dst = pathlib.Path(__file__).parent.joinpath('stats.html')

    file.save_plot_with_script(
        fig=fig,
        dst=dst
    )


if __name__ == '__main__':
    make_plot(
        study_list=[
            pathlib.Path(__file__).parent.joinpath('10mm_study.hdf5'),
            pathlib.Path(__file__).parent.joinpath('5mm_study.hdf5')
        ],
        legend_list=[
            '10 mm',
            '5 mm',
        ],
        color_list=[
            '#1f77b4',
            '#ff7f0e'
        ]
    )
